import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/public_pages/model/punch_time.dart';

void main() {
  test('test PunchTime.fromJson when not forced', () {
    var json = {"controlPoint": 1, "punchTime": 10};
    var actual = PunchTime.fromJson(json);
    expect(actual.checkpoint, 1);
    expect(actual.timestampInMillisecond, 10);
    expect(actual.forced, false);
  });

  test('test PunchTime.fromJson when not forced', () {
    var json = {"controlPoint": 2, "punchTime": 414638, "forced": true};
    var actual = PunchTime.fromJson(json);
    expect(actual.checkpoint, 2);
    expect(actual.timestampInMillisecond, 414638);
    expect(actual.forced, true);
  });
}
