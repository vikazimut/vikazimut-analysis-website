import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/public_pages/live_tracking/color_picker.dart';

void main() {
  test('Test color picker', () {
    ColorPicker colorPicker = ColorPicker();
    int size = colorPicker.size_();
    var color1 = colorPicker.pickColor(0);
    for (int i = 1; i < size; i++) {
      colorPicker.pickColor(i);
    }
    var color2 = colorPicker.pickColor(size);
    expect(color2, color1);
  });
}
