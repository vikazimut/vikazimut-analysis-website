import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/public_pages/live_tracking/orienteer_item.dart';
import 'package:vikazimut_website/public_pages/live_tracking/server_gateway.dart';

void main() {
  test('Test extractOrienteerListFromJson', () {
    var jsonContents = [
      {
        "latitudes": "49.279202",
        "longitudes": "-0.425130",
        "id": 1,
        "nickname": "Toto",
        "startTimestamp": 1,
      },
    ];

    List<OrienteerItem> actual = ServerGateway.extractOrienteerListFromJson_(jsonContents);
    expect(actual.length, 1);
    expect(actual[0].id, 1);
    expect(actual[0].nickname, "Toto");
  });
}
