import 'dart:math' as math;

import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/public_pages/model/geodesic_point.dart';
import 'package:vikazimut_website/public_pages/routes/3d_view/gps_track_painter.dart';
import 'package:vikazimut_website/public_pages/routes/3d_view/image_size.dart';

void main() {
  test('Test rotation_ in case of angle 0', () {
    GeodesicPoint point = GeodesicPoint(49.1, 0.2);
    var actual = GpsTrackPainter.rotation_(point, math.cos(0), math.sin(0), 0, 0);
    expect(actual.longitude, 0.2);
    expect(actual.latitude, 49.1);
  });

  test('Test rotation_ in case of angle 45 and centered', () {
    const angle = 45 * math.pi / 180;
    GeodesicPoint point = GeodesicPoint(49.1, 0.2);
    var actual = GpsTrackPainter.rotation_(point, math.cos(angle), math.sin(angle), 0.2, 49.1);
    expect(actual.longitude, 0.2);
    expect(actual.latitude, 49.1);
  });

  test('Test rotation_ in case of angle 45', () {
    const angle = 45 * math.pi / 180;
    GeodesicPoint point = GeodesicPoint(49, 0.2, 0, 0);
    var actual = GpsTrackPainter.rotation_(point, math.cos(angle), math.sin(angle), 0, 0);
    expect(actual.latitude, closeTo(34.7, 0.1));
    expect(actual.longitude, closeTo(-34.5, 0.1));
  });

  test('Test projection_ 1', () {
    GeodesicPoint point = GeodesicPoint(100, 50);
    var actual = GpsTrackPainter.projection_(point, const ImageSize(10, 20), [50, 100], [100, 200]);
    expect(actual.x, 5);
    expect(actual.y, 10);
  });

  test('Test projection_ 2', () {
    GeodesicPoint point = GeodesicPoint(50, 25);
    var actual = GpsTrackPainter.projection_(point, const ImageSize(100, 200), [50, 100], [100, 200]);
    expect(actual.x, 25);
    expect(actual.y, 150);
  });
}
