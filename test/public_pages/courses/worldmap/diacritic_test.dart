import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/utils/diacritic.dart';

void main() {
  test('Test diacritic with empty string', () {
    var actual = removeDiacritics("");
    expect(actual, "");
  });

  test('Test diacritic without any special characters', () {
    var actual = removeDiacritics("toto titi");
    expect(actual, "toto titi");
  });

  test('Test diacritic with special characters', () {
    var actual = removeDiacritics("êèàôé @");
    expect(actual, "eeaoe @");
  });
}
