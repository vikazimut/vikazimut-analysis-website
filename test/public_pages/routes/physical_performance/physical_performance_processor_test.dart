import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/public_pages/model/geodesic_point.dart';
import 'package:vikazimut_website/public_pages/routes/physical_performance/physical_performance_processor.dart';
import 'package:vikazimut_website/public_pages/routes/physical_performance/effort_bin.dart';

void main() {
  test('Test fromGradientToIndex when nominal', () {
    var actual = PhysicalPerformanceProcessor.fromGradientToIndex_(10);
    expect(actual, 8);
    actual = PhysicalPerformanceProcessor.fromGradientToIndex_(0.10);
    expect(actual, 6);
    actual = PhysicalPerformanceProcessor.fromGradientToIndex_(20);
    expect(actual, 10);
    actual = PhysicalPerformanceProcessor.fromGradientToIndex_(-30);
    expect(actual, 0);
    actual = PhysicalPerformanceProcessor.fromGradientToIndex_(-5.13);
    expect(actual, 5);
    actual = PhysicalPerformanceProcessor.fromGradientToIndex_(-9.99);
    expect(actual, 5);
  });

  test('Test frommIndexToGradientBin when nominal', () {
    var actual = PhysicalPerformanceProcessor.fromIndexToGradientBin_(8);
    expect(actual, 10);
    actual = PhysicalPerformanceProcessor.fromIndexToGradientBin_(6);
    expect(actual, 0);
    actual = PhysicalPerformanceProcessor.fromIndexToGradientBin_(10);
    expect(actual, 20);
    actual = PhysicalPerformanceProcessor.fromIndexToGradientBin_(0);
    expect(actual, -30);
    actual = PhysicalPerformanceProcessor.fromIndexToGradientBin_(5);
    expect(actual, -5);
    actual = PhysicalPerformanceProcessor.fromIndexToGradientBin_(4);
    expect(actual, -10);
  });

  test('Test computeDistanceAndTimeEffortPerGradient when constant gradient', () {
    List<GeodesicPoint> route = [
      GeodesicPoint(0, 0, 0, 10),
      GeodesicPoint(0.001, 0.001, 1000, 10),
      GeodesicPoint(0.002, 0.002, 2000, 10),
      GeodesicPoint(0.003, 0.003, 3000, 10),
    ];
    List<(double, int)> chartData = List.generate(PhysicalPerformanceProcessor.gradientLength, (i) => (0, 0));
    PhysicalPerformanceProcessor.computeDistanceAndTimeEffortPerGradient_(route, chartData);
    expect(chartData.length, 11);
    var (d, t) = chartData[6];
    // expect(d, moreOrLessEquals(157.4295336 * 3, epsilon: 0.01));
    expect(d, moreOrLessEquals(0, epsilon: 0.01));
    // expect(t, 1000 * 3);
    expect(t, 0);
  });

  test('Test computeAverageSpeed', () {
    List<(double, int)> chartData = [
      (100, 5000), // Gradient -30
      (100, 10000),
      (100, 15000),
      (100, 10000),
      (100, 10000),
      (100, 10000),
      (100, 10000), // Gradient 0
    ];
    var actual = PhysicalPerformanceProcessor.computeAverageSpeed_(chartData);
    expect(actual, 36);
  });

  test('Test computeGradientSpeedDifferences', () {
    List<(double, int)> chartData = [
      (100, 5000),
      (100, 10000),
      (100, 15000),
      (100, 10000),
    ];
    List<EffortBin> actual = PhysicalPerformanceProcessor.computeGradientSpeedDifferences_(chartData, 36);
    expect(actual.length, 1);
    // expect(actual[0].effortPercent, 72-36);
    // expect(actual[1].effortPercent, 0);
    // expect(actual[2].effortPercent, 24-36);
    // expect(actual[3].effortPercent, 0);
  });
}
