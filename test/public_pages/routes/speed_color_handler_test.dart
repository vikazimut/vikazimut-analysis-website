import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/public_pages/routes/speed_color_handler.dart';

void main() {
  test('Test colorIndexFromVelocity cas foot case 1', () {
    var actual = SpeedFootoColorHandler().colorIndexFromVelocity(0);
    expect(actual, 0);
  });

  test('Test colorIndexFromVelocity cas foot case 2', () {
    var actual = SpeedFootoColorHandler().colorIndexFromVelocity(10);
    expect(actual, 3);
  });

  test('Test colorIndexFromVelocity cas foot case 3', () {
    var actual = SpeedFootoColorHandler().colorIndexFromVelocity(14);
    expect(actual, 3);
  });

  test('Test colorIndexFromVelocity cas foot case 4', () {
    var actual = SpeedFootoColorHandler().colorIndexFromVelocity(15);
    expect(actual, 4);
  });

  test('Test colorIndexFromVelocity cas foot case 4+', () {
    var actual = SpeedFootoColorHandler().colorIndexFromVelocity(16);
    expect(actual, 4);
  });
}
