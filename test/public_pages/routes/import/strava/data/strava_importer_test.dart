import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/public_pages/model/geodesic_point.dart';
import 'package:vikazimut_website/public_pages/routes/import/strava/data/model_run_activity.dart';
import 'package:vikazimut_website/public_pages/routes/import/strava/strava_importer.dart';

void main() {
  test('Test area intersect when equals', () {
    Area a = Area([10, -10], [-10, 10]);
    Area b = Area([10, -10], [-10, 10]);
    expect(a.intersects(b), true);
    expect(b.intersects(a), true);
  });

  test('Test area intersect when nested', () {
    Area a = Area([10, -10], [-10, 10]);
    Area b = Area([5, -5], [-5, 5]);
    expect(a.intersects(b), true);
    expect(b.intersects(a), true);
  });

  test('Test area intersect when disjointed', () {
    Area a = Area([10, -10], [-10, 10]);
    Area b = Area([10, 20], [-10, 30]);
    expect(a.intersects(b), false);
    expect(b.intersects(a), false);
  });

  test('Test area intersect when overlapped', () {
    Area a = Area([10, -10], [-10, 10]);
    Area b = Area([0, 0], [-20, 20]);
    expect(a.intersects(b), true);
    expect(b.intersects(a), true);
  });

  test('Test _extractGeodesicPointsFromRunActivity', () {
    var runActivity = RunActivity(
      positions: [[49.281253, -0.702113], [49.281217, -0.702129]],
      altitudes: [44.2, 44.3],
      times: [0, 2],
    );

    List<GeodesicPoint> waypoints = StravaImporter().extractGeodesicPointsFromRunActivity_(runActivity);
    expect(waypoints.length, 2);

    expect(waypoints[0].latitude, 49.281253);
    expect(waypoints[0].longitude, -0.702113);
    expect(waypoints[1].latitude, 49.281217);
    expect(waypoints[1].longitude, -0.702129);

    expect(waypoints[0].altitude, 44.2);
    expect(waypoints[1].altitude, 44.3);

    expect(waypoints[0].timestampInMillisecond, 0);
    expect(waypoints[1].timestampInMillisecond, 2000);
  });
}
