import 'dart:io';
import 'dart:typed_data';

import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/public_pages/model/geodesic_point.dart';
import 'package:vikazimut_website/public_pages/routes/import/file/file_format/gpx_file_format.dart';

void main() {
  test('test empty gpx', () {
    File xmlFile = File('test/resources/luc-sur-mer-empty.gpx');
    String contents = xmlFile.readAsStringSync();
    var actual = GpxFileFormat().checkContent(convertStringToUint8List(contents));
    expect(actual, 'import_gpx_server_bad_gpx_file');
  });

  test('test regular gpx', () {
    File xmlFile = File('test/resources/luc-sur-mer.gpx');
    String contents = xmlFile.readAsStringSync();
    var actual = GpxFileFormat().checkContent(convertStringToUint8List(contents));
    expect(actual, null);
  });

  test('test wrong gpx', () {
    File xmlFile = File('test/resources/luc-sur-mer-wrong.gpx');
    String contents = xmlFile.readAsStringSync();
    var actual = GpxFileFormat().checkContent(convertStringToUint8List(contents));
    expect(actual, 'import_gpx_server_bad_gpx_file');
  });

  test('Test extractWaypoints_ when empty', () {
    String gpxText = """
               <gpx version="1.1" creator="TomTom.2014 with Barometer" xsi:schemaLocation="http://www.topografix.com/GPX/1/1" xmlns="http://www.topografix.com/GPX/1/1" xmlns:gpxtpx="http://www.garmin.com/xmlschemas/TrackPointExtension/v1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                    <metadata>
                        <name>Activity</name>
                    </metadata>
                    <trk>
                        <name>Running</name>
                        <trkseg>
                        </trkseg>
                    </trk>
                </gpx>
""";
    var actual = GpxFileFormat().extractWaypoints_(gpxText);
    expect(actual, []);
  });

  test('Test extractWaypoints_ when empty', () {
    String gpxText = """
               <gpx version="1.1" creator="TomTom.2014 with Barometer" xsi:schemaLocation="http://www.topografix.com/GPX/1/1" xmlns="http://www.topografix.com/GPX/1/1" xmlns:gpxtpx="http://www.garmin.com/xmlschemas/TrackPointExtension/v1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                    <metadata>
                        <name>Activity</name>
                    </metadata>
                    <trk>
                        <name>Running</name>
                        <trkseg>
                        </trkseg>
                    </trk>
                </gpx>
""";
    var actual = GpxFileFormat().extractWaypoints_(gpxText);
    expect(actual, []);
  });

  test('Test extractWaypoints_', () {
    String gpxText = """
               <gpx version="1.1" creator="TomTom.2014 with Barometer" xsi:schemaLocation="http://www.topografix.com/GPX/1/1" xmlns="http://www.topografix.com/GPX/1/1" xmlns:gpxtpx="http://www.garmin.com/xmlschemas/TrackPointExtension/v1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                    <metadata>
                        <name>Activity</name>
                    </metadata>
                    <trk>
                        <name>Running</name>
                        <trkseg>
                            <trkpt lat="49.328515" lon="-0.389206">
                                <ele>9.8</ele>
                                <time>1970-01-01T00:01:22.000Z</time>
                                <extensions>
                                    <gpxtpx:TrackPointExtension>
                                        <gpxtpx:hr>99</gpxtpx:hr>
                                    </gpxtpx:TrackPointExtension>
                                </extensions>
                            </trkpt>
                            <trkpt lat="49.328519" lon="-0.389189">
                                <ele>9.8</ele>
                                <time>1970-01-01T00:02:23.000Z</time>
                                <extensions>
                                    <gpxtpx:TrackPointExtension>
                                        <gpxtpx:hr>105</gpxtpx:hr>
                                    </gpxtpx:TrackPointExtension>
                                </extensions>
                            </trkpt>
                            <trkpt lat="49.328529" lon="-0.389173">
                                <ele>9.8</ele>
                                <time>1970-01-01T00:04:24.000Z</time>
                                <extensions>
                                    <gpxtpx:TrackPointExtension>
                                        <gpxtpx:hr>107</gpxtpx:hr>
                                    </gpxtpx:TrackPointExtension>
                                </extensions>
                            </trkpt>
                            <trkpt lat="49.328543" lon="-0.389156">
                                <ele>9.7</ele>
                                <time>1970-01-01T00:08:25.000Z</time>
                                <extensions>
                                    <gpxtpx:TrackPointExtension>
                                        <gpxtpx:hr>109</gpxtpx:hr>
                                    </gpxtpx:TrackPointExtension>
                                </extensions>
                            </trkpt>
                        </trkseg>
                    </trk>
                </gpx>

""";

    List<GeodesicPoint> expected = [
      GeodesicPoint(49.328515, -0.389206, 82000, 9.8),
      GeodesicPoint(49.328519, -0.389189, 143000, 9.8),
      GeodesicPoint(49.328529, -0.389173, 264000, 9.8),
      GeodesicPoint(49.328543, -0.389156, 505000, 9.7),
    ];
    const baseTime = 82000;
    var actual = GpxFileFormat().extractWaypoints_(gpxText);
    for (int i = 0; i < expected.length; i++) {
      expect(actual[i].latitude, expected[i].latitude);
      expect(actual[i].longitude, expected[i].longitude);
      expect(actual[i].timestampInMillisecond, expected[i].timestampInMillisecond - baseTime);
      expect(actual[i].altitude, expected[i].altitude);
    }
  });
}

Uint8List convertStringToUint8List(String str) {
  final List<int> codeUnits = str.codeUnits;
  final Uint8List unit8List = Uint8List.fromList(codeUnits);

  return unit8List;
}
