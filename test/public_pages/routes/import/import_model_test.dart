import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/public_pages/routes/import/import_model.dart';

void main() {
  test('Test format validate() when true', () {
    Format f = Format();
    f.validate(1);
    expect(f.isValid, true);
    f.validate(0);
    expect(f.isValid, true);
  });

  test('Test format validate() when false', () {
    Format f = Format();
    f.validate(-1);
    expect(f.isValid, false);
    f.validate(2);
    expect(f.isValid, false);
  });

  test('Test format validate() when null', () {
    Nickname n = Nickname();
    n.validate(null);
    expect(n.isValid, false);
  });

  test('Test format validate() when empty', () {
    Nickname n = Nickname();
    n.validate("");
    expect(n.isValid, false);
  });

  test('Test format valid() when true', () {
    Nickname n = Nickname();
    n.validate("Régi's@_-1");
    expect(n.isValid, true);
  });

  test('Test format valid() when false', () {
    Nickname n = Nickname();
    n.validate("u!%");
    expect(n.isValid, false);
  });
}
