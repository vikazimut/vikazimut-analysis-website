import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/public_pages/routes/profile/speed_calculator.dart';

void main() {

  test('Test computeSpeedInKmh with no time', () {
    double distanceInMeter = 0;
    int timeInMillisecond = 0;
    double speed = SpeedCalculator.computeSpeedInKmh(distanceInMeter, timeInMillisecond);
    expect(speed, 0);
  });

  test('Test computeSpeedInKmh regular without no elevation gain', () {
    double distanceInMeter = 10;
    int timeInMillisecond = 1000;

    double speed = SpeedCalculator.computeSpeedInKmh(distanceInMeter, timeInMillisecond);
    expect(speed, 36);
  });

  test('Test slopeAdjustment_', () {
    double actual0 = SpeedCalculator.slopeAdjustment(-100);
    expect(actual0, closeTo(1.5, 0.1));
    double actual6 = SpeedCalculator.slopeAdjustment(-30);
    expect(actual6, closeTo(1.5, 0.1));
    double actual5 = SpeedCalculator.slopeAdjustment(-20);
    expect(actual5, closeTo(1.05, 0.01));
    double actual4 = SpeedCalculator.slopeAdjustment(-10);
    expect(actual4, closeTo(0.88, 0.01));
    double actual1 = SpeedCalculator.slopeAdjustment(0);
    expect(actual1, 1);
    double actual2 = SpeedCalculator.slopeAdjustment(10);
    expect(actual2, closeTo(1.48, 0.01));
    double actual3 = SpeedCalculator.slopeAdjustment(20);
    expect(actual3, closeTo(2.3, 0.01));
    double actual8 = SpeedCalculator.slopeAdjustment(100);
    expect(actual8, closeTo(2.3, 0.01));
  });

  test('Test computeSpeedInKmh regular with elevation gain', () {
    double distanceInMeter = 10; // 10 m
    int timeInMillisecond = 3600; // 10 km/h
    double elevationGain = 1; // 1 m elevation / 10 m -> 10%

    double speed = SpeedCalculator.computeSpeedInKmhWeightedByElevationGain(distanceInMeter, timeInMillisecond, elevationGain);
    expect(speed, moreOrLessEquals(14.8, epsilon: 0.1));
  });

}
