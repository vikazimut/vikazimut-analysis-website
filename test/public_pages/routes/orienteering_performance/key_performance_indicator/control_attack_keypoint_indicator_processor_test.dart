import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/public_pages/model/geodesic_point.dart';
import 'package:vikazimut_website/public_pages/routes/orienteering_performance/key_performance_indicator/keypoint_indicator_processor.dart';

void main() {
  test('Test removeValidationMomentFromRoute case 1', () {
    List<GeodesicPoint> points = [
      GeodesicPoint(0, 0, 0, 10),
      GeodesicPoint(0.0001, 0.0001, 1000, 10),
      GeodesicPoint(0.0002, 0.0002, 3000, 30),
      GeodesicPoint(0.0003, 0.0003, 4000, 40),
    ];
    final actual = ControlAttackKeypointIndicatorProcessor().cropRouteFromDistance_(points, 16);
    expect(actual.length, 3);
    expect(actual[0].timestampInMillisecond, 0);
    expect(actual[0].latitude, 0);
    expect(actual[2].timestampInMillisecond, 4000 - 1032);
    expect(actual[2].latitude, 0.0001984);
    expect(actual[2].longitude, 0.0001984);
    expect(actual[2].altitude, 29.68);
  });

  test('Test removeValidationMomentFromRoute case 2', () {
    List<GeodesicPoint> points = [
      GeodesicPoint(0, 0, 0, 10),
      GeodesicPoint(0.0001, 0.0001, 1000, 10),
      GeodesicPoint(0.0002, 0.0002, 3000, 30),
      GeodesicPoint(0.0003, 0.0003, 4000, 40),
    ];
    final actual = ControlAttackKeypointIndicatorProcessor().cropRouteFromDistance_(points, 32);
    expect(actual.length, 2);
    expect(actual[0].timestampInMillisecond, 0);
    expect(actual[0].latitude, 0);
    expect(actual[1].timestampInMillisecond, 4000 - 3032);
    expect(actual[1].latitude, moreOrLessEquals(0.0003 - 0.0002032, epsilon: 0.000001));
    expect(actual[1].altitude, 10.0);
  });

  test('Test removeValidationMomentFromRoute case 3', () {
    List<GeodesicPoint> points = [
      GeodesicPoint(0, 0, 0, 10),
      GeodesicPoint(0.0001, 0.0001, 1000, 10),
      GeodesicPoint(0.0002, 0.0002, 3000, 30),
      GeodesicPoint(0.0003, 0.0003, 4000, 40),
    ];
    final actual = ControlAttackKeypointIndicatorProcessor().cropRouteFromDistance_(points, 100);
    expect(actual.length, 4);
    expect(actual[0].latitude, 0);
    expect(actual[1].latitude, 0.0001);
    expect(actual[2].latitude, 0.0002);
    expect(actual[3].latitude, 0.0003);
  });
  test('Test skipLast2Seconds cas nominal 1', () {
    List<GeodesicPoint> points = [
      GeodesicPoint(0, 0, 0, 10),
      GeodesicPoint(1, 1, 1000, 20),
      GeodesicPoint(2, 2, 4000, 40), // 6000
      GeodesicPoint(3, 3, 8000, 80),
    ];
    final actual = ControlAttackKeypointIndicatorProcessor().cropRouteFromTime_(points, 2 * 1000);
    expect(actual[3].latitude, 2.5);
    expect(actual[3].longitude, 2.5);
    expect(actual[3].timestampInMillisecond, 6000);
    expect(actual[3].altitude, 60);
  });

  test('Test skipLast2Seconds cas nominal 2', () {
    List<GeodesicPoint> points = [
      GeodesicPoint(0, 0, 0, 10),
      GeodesicPoint(1, 1, 1000, 10),
      GeodesicPoint(2, 2, 5000, 50),
      GeodesicPoint(3, 3, 6000, 40),
    ];
    final actual = ControlAttackKeypointIndicatorProcessor().cropRouteFromTime_(points, 2 * 1000);
    expect(actual[2].latitude, 1.75);
    expect(actual[2].longitude, 1.75);
    expect(actual[2].timestampInMillisecond, 4000);
    expect(actual[2].altitude, 40);
  });

  test('Test skipLast2Seconds cas nominal 3', () {
    List<GeodesicPoint> points = [
      GeodesicPoint(0, 0, 0, 10),
      GeodesicPoint(0.5, 0.5, 500, 10),
      GeodesicPoint(1, 1, 1000, 10),
      GeodesicPoint(2, 2, 4000, 50),
      GeodesicPoint(3, 3, 6000, 40),
    ];
    final actual = ControlAttackKeypointIndicatorProcessor().cropRouteFromTime_(points, 2 * 1000);
    expect(actual.length, 4);
    expect(actual[3].latitude, 2);
    expect(actual[3].longitude, 2);
    expect(actual[3].timestampInMillisecond, 4000);
    expect(actual[3].altitude, 50);
  });
}
