import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/public_pages/model/geodesic_point.dart';
import 'package:vikazimut_website/public_pages/routes/orienteering_performance/key_performance_indicator/keypoint_indicator_processor.dart';

void main() {
  test('Test calculateSpeedScore case perfect', () {
    final double actual = HandrailKeypointIndicatorProcessor.calculateAverageSpeedScore_(11.0, 10.0);
    expect(actual, 10);
  });

  test('Test calculateSpeedScore case 0', () {
    final double actual = HandrailKeypointIndicatorProcessor.calculateAverageSpeedScore_(0, 10.0);
    expect(actual, 0);
  });

  test('Test calculateSpeedScore case 5', () {
    final double actual = HandrailKeypointIndicatorProcessor.calculateAverageSpeedScore_(5, 10.0);
    expect(actual, 5);
  });

  test('Test calculateSpeedScore case 8', () {
    final double actual = HandrailKeypointIndicatorProcessor.calculateAverageSpeedScore_(8, 10.0);
    expect(actual, 8);
  });

  test('test countStops when no route', () {
    const double minSpeedForMovementInKmh = 2;
    List<double> speedsInKmh = [];
    List<GeodesicPoint> points = [];
    var countStops = HandrailKeypointIndicatorProcessor.countStops_(speedsInKmh, points, minSpeedForMovementInKmh, 10, 10);
    expect(countStops, 0);
  });

  test('test countStops when 1 stop', () {
    const double minSpeedForMovementInKmh = 2;
    List<double> speedsInKmh = [0, 1, 2, 10];
    List<GeodesicPoint> points = [
      GeodesicPoint(0, 0, 0),
      GeodesicPoint(0, 0, 10000),
      GeodesicPoint(0, 0, 20000),
      GeodesicPoint(0, 0, 30000),
      GeodesicPoint(0, 0, 40000),
    ];
    var countStops = HandrailKeypointIndicatorProcessor.countStops_(speedsInKmh, points, minSpeedForMovementInKmh, 10, 10);
    expect(countStops, 1);
  });

  test('test countStops when 3 stops', () {
    const double minSpeedForMovementInKmh = 2;
    List<double> speedsInKmh = [0, 1, 2, 10, 2, 2, 10, 10, 1, 1];
    List<GeodesicPoint> points = [
      GeodesicPoint(0, 0, 0),
      GeodesicPoint(0, 0, 10000),
      GeodesicPoint(0, 0, 11000),
      GeodesicPoint(0, 0, 12000),
      GeodesicPoint(0, 0, 40000),
      GeodesicPoint(0, 0, 41000),
      GeodesicPoint(0, 0, 51000),
      GeodesicPoint(0, 0, 70000),
      GeodesicPoint(0, 0, 81000),
      GeodesicPoint(0, 0, 92000),
    ];
    var countStops = HandrailKeypointIndicatorProcessor.countStops_(speedsInKmh, points, minSpeedForMovementInKmh, 10, 10);
    expect(countStops, 3);
  });

  test('test countStops when 2 stops but the second is not enough long', () {
    const double minSpeedForMovementInKmh = 2;
    List<double> speedsInKmh = [0, 1, 2, 10, 2, 2];
    List<GeodesicPoint> points = [
      GeodesicPoint(0, 0, 0),
      GeodesicPoint(0, 0, 10000),
      GeodesicPoint(0, 0, 20000),
      GeodesicPoint(0, 0, 30000),
      GeodesicPoint(0, 0, 31000),
      GeodesicPoint(0, 0, 32000),
    ];
    var countStops = HandrailKeypointIndicatorProcessor.countStops_(speedsInKmh, points, minSpeedForMovementInKmh, 10, 10);
    expect(countStops, 1);
  });

  test('test calculateSpeedStandardDeviation when empty', () {
    List<double> speedsEffortInKmh = [];
    double score = HandrailKeypointIndicatorProcessor.calculateSpeedStandardDeviation_(speedsEffortInKmh);
    expect(score, 0);
  });

  test('test calculateSpeedStandardDeviation when nominal', () {
    List<double> speedsEffortInKmh = [2, 1.8, 2.2, 2];
    double score = HandrailKeypointIndicatorProcessor.calculateSpeedStandardDeviation_(speedsEffortInKmh);
    expect(score, moreOrLessEquals(0.14, epsilon: 0.01));
  });

  test('test calculateDiscrepancyScore when 0', () {
    double score = HandrailKeypointIndicatorProcessor.calculateEffortDiscrepancyScore_(0);
    expect(score, 10);
  });

  test('test calculateDiscrepancyScore when 2', () {
    double score = HandrailKeypointIndicatorProcessor.calculateEffortDiscrepancyScore_(2);
    expect(score, 10);
  });

  test('test calculateDiscrepancyScore when 7', () {
    double score = HandrailKeypointIndicatorProcessor.calculateEffortDiscrepancyScore_(7);
    expect(score, 5);
  });

  test('test calculateDiscrepancyScore when 10', () {
    double score = HandrailKeypointIndicatorProcessor.calculateEffortDiscrepancyScore_(10);
    expect(score, 2);
  });

  test('test calculateDiscrepancyScore when 12', () {
    double score = HandrailKeypointIndicatorProcessor.calculateEffortDiscrepancyScore_(12);
    expect(score, 0);
  });

  test('test calculateDiscrepancyScore when 15', () {
    double score = HandrailKeypointIndicatorProcessor.calculateEffortDiscrepancyScore_(15);
    expect(score, 0);
  });
}
