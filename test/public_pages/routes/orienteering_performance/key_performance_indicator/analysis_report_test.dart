import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/public_pages/routes/orienteering_performance/key_performance_indicator/analysis_report.dart';

void main() {
  test('Creation of empty orienteering_performance report', () {
    var report = AnalysisReport();
    expect(report.score, 10);
  });

  test('Creation of comment', () {
    var comment1 = Comment();
    comment1.uTurn.add(LegComment(1, "1"));
    comment1.speed.add(LegComment(1, "1"));
    comment1.direction.add(LegComment(1, "1"));
    comment1.stop.add(LegComment(1, "1"));

    var comment2 = Comment();
    comment2.uTurn.add(LegComment(2, "2"));
    comment2.speed.add(LegComment(2, "2"));
    comment2.direction.add(LegComment(2, "2"));
    comment2.stop.add(LegComment(2, "2"));

    comment1.add(comment2);
    expect(comment1.uTurn[0].legIndex, 1);
    expect(comment1.uTurn[1].legIndex, 2);
    expect(comment1.speed[0].legIndex, 1);
    expect(comment1.speed[1].legIndex, 2);
    expect(comment1.direction[0].legIndex, 1);
    expect(comment1.direction[1].legIndex, 2);
    expect(comment1.stop[0].legIndex, 1);
    expect(comment1.stop[1].legIndex, 2);
  });
}