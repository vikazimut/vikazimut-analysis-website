import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/public_pages/routes/color_picker.dart';

void main() {
  test('Test color picker get and restore color', () {
    ColorPicker colorPicker = ColorPicker();
    var color1 = colorPicker.pickColor();
    colorPicker.restoreColor(color1);
    var color2 = colorPicker.pickColor();
    expect(color1, color2);
  });
}
