import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/public_pages/routes/split_time/export_split_time_as_csv.dart';

void main() async {
  test('Test removeEndOfLine', () {
    String text = "Test with a line ended by\n";
    String expected = "Test with a line ended by ";
    String actual = ExportSplitTimeAsCsv.removeEndOfLine(text);
    expect(actual, expected);
  });
}
