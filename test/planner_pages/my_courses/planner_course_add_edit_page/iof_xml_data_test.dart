import 'dart:io';

import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/planner_pages/my_courses/planner_course_add_edit_page/iof_xml_data.dart';
import 'package:xml/xml.dart';

void main() {
  test('Test check xml format nominal case', () {
    String xmlHeader = """<?xml version="1.0" encoding="UTF-8" ?>
<CourseData xmlns="http://www.orienteering.org/datastandard/3.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" iofVersion="3.0" createTime="2020-07-13T16:26:16.120+02:00" creator="OCAD 12.4.0.1684">
</CourseData>
""";
    var result = IofXmlData.checkFormat(xmlHeader);
    expect(result, true);
  });

  test('Test check xml format error case', () {
    String xmlHeader = """<?xml version="1.0" encoding="UTF-8" ?>
<!-- Generator: OCAD Version 12.4.0 -->
<kml xmlns="http://www.opengis.net/kml/2.2">
</kml>
""";
    var result = IofXmlData.checkFormat(xmlHeader);
    expect(result, false);
  });

  test('Test read XML file with one course', () {
    File xmlFile = File('test/resources/38/38.xml');
    String xmlAsString = xmlFile.readAsStringSync();
    List<IofXmlData> courses = IofXmlData.getAllCoursesFromXML(xmlAsString);
    for (var i = 0; i < courses.length; ++i) {
      expect(courses[i].courseName, "Blainville Historique");
    }
  });

  test('Test read XML file with multiple courses', () {
    File xmlFile = File('test/resources/bleu/bleu.xml');
    String xmlAsString = xmlFile.readAsStringSync();
    List<IofXmlData> courses = IofXmlData.getAllCoursesFromXML(xmlAsString);
    List<String> expected = ["Violet", "Violet Court", "JOrange", "Bleu"];
    for (var i = 0; i < courses.length; ++i) {
      expect(courses[i].courseName, expected[i]);
    }
  });

  test('Test getAllCoursesFromXML', () {
    File xmlFile = File('test/resources/lacanau/Circuits Lacanau Nord.Courses.xml');
    String xmlAsString = xmlFile.readAsStringSync();
    List<IofXmlData> courses = IofXmlData.getAllCoursesFromXML(xmlAsString);
    List<String> expected = ["Bleu", "Jaune", "Violet C", "Violet L"];
    for (var i = 0; i < courses.length; ++i) {
      expect(courses[i].courseName, expected[i]);
    }
  });

  test('Test removeAllButTheSelectedCourseXml', () {
    File xmlFile = File('test/resources/lacanau/Circuits Lacanau Nord.Courses.xml');
    String xmlAsString = xmlFile.readAsStringSync();
    String courses = IofXmlData.removeAllButTheSelectedCourseXml_(xmlAsString, "Jaune");
    var xmlDocument = XmlDocument.parse(courses);
    Iterable<XmlElement> courseFolder = xmlDocument.findAllElements("Course");
    expect(courseFolder.length, 1);
    String name = courseFolder.elementAt(0).getElement("Name")!.innerText;
    expect(name, "Jaune");
  });

  test('Test removeMapChanges with no map change', () {
    String xmlAsString = """
<?xml version="1.0" encoding="UTF-8"?>
<CourseData xmlns="http://www.orienteering.org/datastandard/3.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" iofVersion="3.0" createTime="2024-03-26T08:13:16.897+01:00" creator="OCAD 2020.7.5.4304">
  <Event>
    <Name>Test</Name>
  </Event>
  <RaceCourseData>
    <Control>
      <Id>S1</Id>
      <Position lng="-1.045108" lat="48.828853"/>
      <MapPosition x="11.1" y="-109.4" unit="mm"/>
    </Control>
    <Course>
      <Name>Violet long</Name>
      <Length>14450</Length>
      <Climb>260</Climb>
      <CourseControl type="Start">
        <Control>S1</Control>
      </CourseControl>
      <CourseControl type="Control">
        <Control>59</Control>
        <LegLength>107</LegLength>
      </CourseControl>
      <CourseControl type="Control">
        <Control>87</Control>
        <LegLength>383</LegLength>
      </CourseControl>
    </Course>
  </RaceCourseData>
</CourseData>
  """;
    String xml = IofXmlData.removeIntermediateStarts_(xmlAsString);
    expect(xml, xmlAsString);
  });

  test('Test removeMapChanges with map change', () {
    String xmlAsString = """
<?xml version="1.0" encoding="UTF-8"?>
<CourseData xmlns="http://www.orienteering.org/datastandard/3.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" iofVersion="3.0" createTime="2024-03-26T08:13:16.897+01:00" creator="OCAD 2020.7.5.4304">
  <Event>
    <Name>Test</Name>
  </Event>
  <RaceCourseData>
    <Control>
      <Id>S1</Id>
      <Position lng="-1.045108" lat="48.828853"/>
      <MapPosition x="11.1" y="-109.4" unit="mm"/>
    </Control>
    <Course>
      <Name>Violet long</Name>
      <Length>14450</Length>
      <Climb>260</Climb>
      <CourseControl type="Start">
        <Control>S1</Control>
      </CourseControl>
      <CourseControl type="Control">
        <Control>59</Control>
        <LegLength>107</LegLength>
      </CourseControl>
      <CourseControl type="Start">
        <Control>S4</Control>
        <LegLength>3</LegLength>
      </CourseControl>
      <CourseControl type="Control">
        <Control>87</Control>
        <LegLength>383</LegLength>
      </CourseControl>
      <CourseControl type="Start">
        <Control>S5</Control>
        <LegLength>4</LegLength>
      </CourseControl>
    </Course>
  </RaceCourseData>
</CourseData>
  """;
    String xml = IofXmlData.removeIntermediateStarts_(xmlAsString);
    var xmlDocument = XmlDocument.parse(xml);
    Iterable<XmlElement> courseFolder = xmlDocument.findAllElements("CourseControl");
    expect(courseFolder.length, 3);
  });

  test('Test removeUnusedTags when nominal', () {
    String xmlAsString = """
<?xml version="1.0" encoding="UTF-8"?>
<CourseData xmlns="http://www.orienteering.org/datastandard/3.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" iofVersion="3.0" createTime="2024-03-26T08:13:16.897+01:00" creator="OCAD 2020.7.5.4304">
  <Event>
    <Name>Test</Name>
  </Event>
  <RaceCourseData>
    <Map>
      <Scale>5000</Scale>
      <!--  extent of ocad map -->
      <MapPositionTopLeft x="-475.0" y="340.2" unit="mm"/>
      <MapPositionBottomRight x="171.7" y="-214.3" unit="mm"/>
     </Map>
    <Control>
      <Id>S1</Id>
      <Position lng="-1.045108" lat="48.828853"/>
      <MapPosition x="11.1" y="-109.4" unit="mm"/>
    </Control>
    <Course>
      <Name>Violet long</Name>
      <Length>14450</Length>
      <Climb>260</Climb>
      <CourseControl type="Start">
        <Control>S1</Control>
      </CourseControl>
      <CourseControl type="Control">
        <Control>59</Control>
        <LegLength>107</LegLength>
      </CourseControl>
      <CourseControl type="Start">
        <Control>S4</Control>
        <LegLength>3</LegLength>
      </CourseControl>
      <CourseControl type="Control">
        <Control>87</Control>
        <LegLength>383</LegLength>
      </CourseControl>
      <CourseControl type="Start">
        <Control>S5</Control>
        <LegLength>4</LegLength>
      </CourseControl>
    </Course>
    <TeamCourseAssignment>
      <BibNumber>1</BibNumber>
      <TeamMemberCourseAssignment>
        <Leg>1</Leg>
        <CourseName>A_AB</CourseName>
        <CourseFamily>A</CourseFamily>
      </TeamMemberCourseAssignment>
      <TeamMemberCourseAssignment>
        <Leg>2</Leg>
        <CourseName>A_BC</CourseName>
        <CourseFamily>A</CourseFamily>
      </TeamMemberCourseAssignment>
      <TeamMemberCourseAssignment>
        <Leg>3</Leg>
        <CourseName>A_CA</CourseName>
        <CourseFamily>A</CourseFamily>
      </TeamMemberCourseAssignment>
    </TeamCourseAssignment>
    <TeamCourseAssignment>
      <BibNumber>2</BibNumber>
      <TeamMemberCourseAssignment>
        <Leg>1</Leg>
        <CourseName>A_BC</CourseName>
        <CourseFamily>A</CourseFamily>
      </TeamMemberCourseAssignment>
      <TeamMemberCourseAssignment>
        <Leg>2</Leg>
        <CourseName>A_CA</CourseName>
        <CourseFamily>A</CourseFamily>
      </TeamMemberCourseAssignment>
      <TeamMemberCourseAssignment>
        <Leg>3</Leg>
        <CourseName>A_AB</CourseName>
        <CourseFamily>A</CourseFamily>
      </TeamMemberCourseAssignment>
    </TeamCourseAssignment>
    <ClassCourseAssignment>
      <ClassName>vert</ClassName>
      <CourseName>vert</CourseName>
    </ClassCourseAssignment>
    <ClassCourseAssignment>
      <ClassName>blue</ClassName>
      <CourseName>bleu</CourseName>
    </ClassCourseAssignment>
  </RaceCourseData>
</CourseData>
  """;
    String xml = IofXmlData.removeUnusedTags(xmlAsString);
    var xmlDocument = XmlDocument.parse(xml);
    Iterable<XmlElement> courseFolder1 = xmlDocument.findElements("TeamCourseAssignment");
    expect(courseFolder1.length, 0);
    Iterable<XmlElement> courseFolder2 = xmlDocument.findElements("TeamCourseAssignment");
    expect(courseFolder2.length, 0);
    var race = xmlDocument.findAllElements("RaceCourseData").first;
    Iterable<XmlElement> courseFolder3 = race.findElements("Course");
    expect(courseFolder3.length, 1);
    Iterable<XmlElement> courseFolder4 = race.findElements("Control");
    expect(courseFolder4.length, 1);
    Iterable<XmlElement> courseFolder5 = race.findElements("Map");
    expect(courseFolder5.length, 1);
  });

  test('Test check kml format nominal case', () {
    String xmlAsString = """
<?xml version="1.0" encoding="utf-8"?>
<CourseData xmlns="http://www.orienteering.org/datastandard/3.0" iofVersion="3.0" createTime="2023-07-31T08:39:47.1373436+02:00" creator="Purple Pen version 3.4.1">
  <Event>
    <Name>Mémos Mepieu</Name>
  </Event>
  <RaceCourseData>
    <Map>
      <Scale>15000</Scale>
      <MapPositionTopLeft x="-230.8" y="182.78" />
      <MapPositionBottomRight x="77.61" y="-264.56" />
    </Map>
    <Control type="Start">
      <Id>STA1</Id>
      <Position lng="5.4414791885322709" lat="45.721724769189954" />
      <MapPosition x="-28.65" y="-98.37" />
    </Control>
    <Course>
      <Name>Mémos</Name>
      <Length>4800</Length>
      <CourseControl type="Finish">
        <Control>FIN1</Control>
        <LegLength>361</LegLength>
      </CourseControl>
    </Course>
    <Course>
      <Name>Tête d'héroïne</Name>
      <Length>6800</Length>
      <CourseControl type="Start">
        <Control>STA2</Control>
      </CourseControl>
    </Course>
  </RaceCourseData>
</CourseData>
    """;
    var result = IofXmlData.getAllCoursesFromXML(xmlAsString);
    expect(result[0].courseName, "Mémos");
    expect(result[1].courseName, "Tête d'héroïne");
  });
}
