import 'dart:io';
import 'dart:typed_data';

import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/planner_pages/my_courses/planner_course_add_edit_page/map_image_data.dart';

void main() {
  test('Test check xml format nominal case', () async {
    File xmlFile = File('test/resources/bleu/tile_0_0.jpg');
    Uint8List data = await xmlFile.readAsBytes();
    var result = MapImageData.checkFormat("test/resources/bleu/tile_0_0.jpg", data);
    expect(result, true);
  });
}
