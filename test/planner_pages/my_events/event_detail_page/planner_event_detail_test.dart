import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/planner_pages/my_events/event_detail_page/planner_event_detail.dart';

void main() {
  test('test event detail get type name 0', () {
    var actual = EventDetail.getTypeName_(0);
    expect(actual, "constant_type_championship");
  });

  test('test event detail get type name 1', () {
    var actual = EventDetail.getTypeName_(1);
    expect(actual, "constant_type_points");
  });

  test('test event detail get type name 1', () {
    var actual = EventDetail.getTypeName_(2);
    expect(actual, "constant_type_cumulative_time");
  });
}
