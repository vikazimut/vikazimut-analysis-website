import 'dart:convert';

import 'package:corsac_jwt/corsac_jwt.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/authentication/identifier_user.dart';

void main() {
  test('Test is admin true', () {
    const token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE2NzEyOTA1OTYsImV4cCI6MTY3MTI5NDE5Niwicm9sZXMiOlsiUk9MRV9BRE1JTiIsIlJPTEVfVVNFUiJdLCJ1c2VybmFtZSI6ImVyaWMifQ.P2259oRYaLAOoXQnLLI_K0ClDEVYg-NgmvBONaqLCJBcYhMikxLoP-XVNlcGBtZMBaS10Ko8J4yrpn0FbRqRv3xLl6K9Minj0qzI8EaTNiwpL2FuZRPWElVfscRsY2ZZ_xAki7Hq9gl-BG-B9JfnKIi7PjP8Jmz4tyBD9CIZ8_6fFV_81GCFfepMELPCfZxMv_q6dPO-3nxJzllC9aS9XIv2OOakBnW1LVmRlAytg2Pbpsf6Ii9haVGjoPJbc-uAoH1GF2GHsQ1cbhOmtevsucsq3hxT1SKP_n8J-x-J_yQLFyO-tPoSYC4ObgzEPBzIuRvu0ZwkSwU05LxTx2Nb3A";
    final decodedToken = JWT.parse(token);
    final data = decodedToken.claims;
    expect(IdentifiedUser.isAdmin(data['roles']), true);
  });

  test('Test is admin false', () {
    const String token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE2NzEyOTIyMDksImV4cCI6MTY3MTI5NTgwOSwicm9sZXMiOlsiUk9MRV9VU0VSIl0sInVzZXJuYW1lIjoiRW5zaWNhZW4ifQ.IcVWcYuyxfnV2b7Q8QHUI_-yapW6YjBiC2QYP_AmQ0ts4MUYuBcf68l7cIbcGygSgYxk1hQb6TGtBWrecb7S-ZSUSYKyWx6Ic85PHL0-SbK3q4yrPVtCXOg4OjQMV5Q4dwO9S9ybLVrNPzy4x2oRlARgXfxTC9dOxdOMcutdhPuqSyZLyeKlv2LsADmVbvgcxBM2GXouKuLQ4ZEwS3Vy5dc7weOkHNCrrw3tJfmbVJIo1pzMt2jOVLTMNiSNSscBC3EvO4PKHEQmD0KgLmXl6cRcTDtkvgHiyt_3JwqihXeK7V70YZbUhZiJ0Y1DTp0ZC2by8uGTtTR1zlD1VtquJA";
    final decodedToken = JWT.parse(token);
    final data = decodedToken.claims;
    expect(IdentifiedUser.isAdmin(data['roles']), false);
  });

  test('Test create user from token', () {
    const token = {"token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE2NzEyOTIyMDksImV4cCI6MTY3MTI5NTgwOSwicm9sZXMiOlsiUk9MRV9VU0VSIl0sInVzZXJuYW1lIjoiRW5zaWNhZW4ifQ.IcVWcYuyxfnV2b7Q8QHUI_-yapW6YjBiC2QYP_AmQ0ts4MUYuBcf68l7cIbcGygSgYxk1hQb6TGtBWrecb7S-ZSUSYKyWx6Ic85PHL0-SbK3q4yrPVtCXOg4OjQMV5Q4dwO9S9ybLVrNPzy4x2oRlARgXfxTC9dOxdOMcutdhPuqSyZLyeKlv2LsADmVbvgcxBM2GXouKuLQ4ZEwS3Vy5dc7weOkHNCrrw3tJfmbVJIo1pzMt2jOVLTMNiSNSscBC3EvO4PKHEQmD0KgLmXl6cRcTDtkvgHiyt_3JwqihXeK7V70YZbUhZiJ0Y1DTp0ZC2by8uGTtTR1zlD1VtquJA"};
    final user = IdentifiedUser.fromTokens(jsonEncode(token));
    expect(user.username, "Ensicaen");
    expect(user.admin, false);
  });
}
