import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/common/lat_lon_box.dart';
import 'package:vikazimut_website/utils/map_coordinate_util.dart';

void main() {
  test('Test convertGeoPointIntoMapCoordinates with regular points', () {
    LatLonBox latLonBox = const LatLonBox(north: 10, south: 4, west: -2, east: 2, rotation: 0);
    int imageWidth = 1000;
    int imageHeight = 2000;
    double latitude = 8;
    double longitude = 0;
    Offset actual = MapCoordinateUtil.convertGeoPointIntoMapCoordinates(latitude, longitude, latLonBox, imageWidth, imageHeight);
    Offset expected = const Offset(500, 666);
    expect(actual, expected);
  });
}
