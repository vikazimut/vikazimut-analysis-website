import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/utils/time.dart';

void main() {
  test('Test formatTimeAsStringWithMilliseconds simple case', () {
    String actual = timeToStringWithMilliseconds(60 * 1000);
    expect(actual, "00:01:00.000");
  });

  test('Test formatTimeAsStringWithMilliseconds nominal case', () {
    String actual = timeToStringWithMilliseconds(843270);
    expect(actual, "00:14:03.270");
  });

  test('Test formatTimeAsStringWithMilliseconds negative case', () {
    String actual = timeToStringWithMilliseconds(-1);
    expect(actual, "-1");
  });

  test('Test formatTimeAsString > 1h', () {
    String actual = timeToString(1 * 60 * 60 * 1000);
    expect(actual, '01:00:00');
  });

  test('Test formatTimeAsString < 1h without hour', () {
    String actual = timeToString(1 * 60 * 1000, withHour: false);
    expect(actual, '01:00');
  });

  test('Test formatTimeAsString > 1h with hour', () {
    String actual = timeToString(1 * 60 * 1000, withHour: true);
    expect(actual, '0:01:00');
  });

  test('Test convertISO8601DateToMilliseconds', () {
    var actual = convertISO8601DateToMilliseconds("2021-11-14T18:54:33+01:00");
    expect(actual, 1636912473000);
  });
}
