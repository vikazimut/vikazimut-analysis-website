import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:vikazimut_website/l10n/l10n.dart';

import 'event_list_gateway.dart';
import 'event_list_model.dart';
import 'event_list_page.dart';

class EventListPresenter {
  final EventListPageState _view;

  const EventListPresenter(this._view);

  Future<List<EventData>> fetchEvents(String url, int plannerId) async {
    return EventListGateway.fetchEvents(url, plannerId);
  }

  Future<void> deleteEvent(int id, String eventName) async {
    http.Response response = await EventListGateway.deleteEvent(id);
    if (response.statusCode == HttpStatus.ok) {
      _view.displayToast(L10n.getString("delete_course_success"));
      _view.update();
      _view.displayToast(L10n.getString("planner_event_list_page_delete_success_message", [eventName]));
    } else {
      _view.error(response.body);
    }
  }

  void addEvent(EventFormData data) async {
    http.Response response = await EventListGateway.addCourse(data);
    if (response.statusCode == HttpStatus.ok) {
      _view.update();
    } else {
      _view.error(response.body);
    }
  }
}
