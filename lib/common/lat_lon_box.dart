import 'package:flutter/foundation.dart';

@immutable
class LatLonBox {
  final double north;
  final double east;
  final double south;
  final double west;
  final double rotation;

  const LatLonBox({
    required this.north,
    required this.east,
    required this.south,
    required this.west,
    required this.rotation,
  });

  List<List<double>> calculateFourQuadrilateralCorners() {
    return [
      [north, west],
      [north, east],
      [south, east],
      [south, west],
    ];
  }
}
