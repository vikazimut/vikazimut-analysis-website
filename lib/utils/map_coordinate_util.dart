import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:vikazimut_website/common/lat_lon_box.dart';
import 'package:vikazimut_website/utils/utils.dart';

class MapCoordinateUtil {
  static Offset convertGeoPointIntoMapCoordinates(double latitude, double longitude, LatLonBox boundingBox, int imageWidth, int imageHeight) {
    int x = (longitude - boundingBox.west) * imageWidth ~/ (boundingBox.east - boundingBox.west);
    int y = (latitude - boundingBox.north) * imageHeight ~/ (boundingBox.south - boundingBox.north);
    x -= imageWidth ~/ 2;
    y -= imageHeight ~/ 2;
    double theta = deg2rad(boundingBox.rotation);
    double x1 = (x * math.cos(theta) - y * math.sin(theta));
    double y1 = (x * math.sin(theta) + y * math.cos(theta));
    x1 += imageWidth / 2;
    y1 += imageHeight / 2;
    return Offset(x1, y1);
  }
}
