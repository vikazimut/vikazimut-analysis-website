import 'package:intl/intl.dart';
import 'package:vikazimut_website/l10n/l10n.dart';

int convertISO8601DateToMilliseconds(String date) {
  DateTime milliseconds = DateTime.parse(date);
  return milliseconds.millisecondsSinceEpoch;
}

String timeToString(int milliseconds, {bool withHour = true}) {
  Duration duration = Duration(milliseconds: milliseconds);
  String hours = (duration.inHours > 0)
      ? "${(duration.inHours).toString().padLeft(2, "0")}:"
      : withHour
          ? "0:"
          : "";
  return "$hours${(duration.inMinutes % 60).toString().padLeft(2, "0")}:${(duration.inSeconds % 60).toString().padLeft(2, "0")}";
}

String timeToStringWithMilliseconds(int milliseconds) {
  if (milliseconds < 0) return milliseconds.toString();
  Duration duration = Duration(milliseconds: milliseconds);
  return "${(duration.inHours).toString().padLeft(2, "0")}"
      ":${(duration.inMinutes % 60).toString().padLeft(2, "0")}"
      ":${(duration.inSeconds % 60).toString().padLeft(2, "0")}"
      ".${(duration.inMilliseconds % 1000).toString().padLeft(3, "0")}";
}

String getLocalizedDate(int dateInMillisecondsSinceEpoch) {
  return _getLocalizedDate(dateInMillisecondsSinceEpoch, L10n.locale.toString());
}

String getAbsoluteLocalizedDate(int dateInMillisecondsSinceEpoch) {
  var dateTime = DateTime.fromMillisecondsSinceEpoch(dateInMillisecondsSinceEpoch);
  final DateFormat formatter = DateFormat.yMMMd(L10n.locale.toString());
  return formatter.format(dateTime);
}

String _getLocalizedDate(int dateInMillisecondsSinceEpoch, String locale) {
  var dateTime = DateTime.fromMillisecondsSinceEpoch(dateInMillisecondsSinceEpoch, isUtc: true);
  final DateFormat formatter = DateFormat.yMMMd(locale);
  return formatter.format(dateTime);
}

String getLocalizedDateWithTime(int dateInMillisecondsSinceEpoch) {
  return _getLocalizedDateWithTime(dateInMillisecondsSinceEpoch, L10n.locale.toString());
}

String _getLocalizedDateWithTime(int dateInMillisecondsSinceEpoch, String locale) {
  var dateTime = DateTime.fromMillisecondsSinceEpoch(dateInMillisecondsSinceEpoch, isUtc: true);
  final DateFormat formatter = DateFormat.yMMMMEEEEd(locale).add_Hm();
  return formatter.format(dateTime);
}
