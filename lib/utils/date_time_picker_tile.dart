// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/theme/theme.dart';

class DateTimePickerTile extends StatefulWidget {
  final Color? borderColor;
  final DateTimePickerController controller;
  final void Function(int? date) onSelect;

  const DateTimePickerTile({required this.onSelect, this.borderColor, required this.controller});

  @override
  DateTimePickerTileState createState() => DateTimePickerTileState();
}

class DateTimePickerTileState extends State<DateTimePickerTile> {
  final TextEditingController _dateController = TextEditingController();
  final TextEditingController _timeController = TextEditingController();
  late DateTime? _selectedDate;
  late int _timeOffsetHours;

  @override
  void initState() {
    widget.controller.setView(this);
    if (widget.controller.dateInMillisecond != null) {
      DateTime date = DateTime.fromMillisecondsSinceEpoch(widget.controller.dateInMillisecond!);
      _dateController.text = _dateToString(date);
      _timeController.text = _timeToString(date);
      _selectedDate = date;
      _timeOffsetHours = date.timeZoneOffset.inHours;
    } else {
      _selectedDate = null;
      _timeOffsetHours = DateTime.now().timeZoneOffset.inHours;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Wrap(
        runSpacing: 10,
        children: <Widget>[
          InkWell(
            onTap: () {
              _selectDate(context);
            },
            child: SizedBox(
              width: 120,
              child: TextFormField(
                textAlign: TextAlign.center,
                enabled: false,
                controller: _dateController,
                style: TextStyle(color: Theme.of(context).colorScheme.tertiary),
                decoration: InputDecoration(
                  hintText: L10n.getString("data_time_picker_date_hint_text"),
                  disabledBorder: const OutlineInputBorder(
                    borderSide: BorderSide(width: 2, color: kOrangeColor),
                  ),
                  filled: true,
                  fillColor: Theme.of(context).scaffoldBackgroundColor,
                ),
              ),
            ),
          ),
          const SizedBox(width: 10),
          InkWell(
            onTap: () {
              _selectTime(context);
            },
            child: SizedBox(
              width: 90,
              child: TextFormField(
                textAlign: TextAlign.center,
                enabled: false,
                controller: _timeController,
                style: TextStyle(color: Theme.of(context).colorScheme.tertiary),
                decoration: InputDecoration(
                  hintText: L10n.getString("data_time_picker_time_hint_text"),
                  disabledBorder: const OutlineInputBorder(
                    borderSide: BorderSide(width: 2, color: kOrangeColor),
                  ),
                  filled: true,
                  fillColor: Theme.of(context).scaffoldBackgroundColor,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> _selectDate(BuildContext context) async {
    final DateTime? selectedDate = await showDatePicker(
      context: context,
      initialDate: _getDateFromDate(_selectedDate),
      initialDatePickerMode: DatePickerMode.day,
      firstDate: DateTime(2021),
      lastDate: DateTime(2222),
      currentDate: DateTime.now().toUtc(),
      builder: (context, child) {
        return Theme(
          data: Theme.of(context).copyWith(
            colorScheme: const ColorScheme.light(
              primary: kOrangeColor,
              onSurface: kOrangeColor,
            ),
            textButtonTheme: TextButtonThemeData(
              style: TextButton.styleFrom(disabledForegroundColor: kOrangeColor),
            ),
          ),
          child: child!,
        );
      },
    );
    if (selectedDate != null) {
      setState(() {
        _selectedDate = _updateDateWithDate(_selectedDate, selectedDate);
        _dateController.text = _dateToString(_selectedDate!);
        _timeController.text = _timeToString(_selectedDate!);
        widget.onSelect(_selectedDate!.millisecondsSinceEpoch);
      });
    }
  }

  Future<void> _selectTime(BuildContext context) async {
    final TimeOfDay? selectedTime = await showTimePicker(
      context: context,
      initialTime: _getTimeFromDate(_selectedDate),
      builder: (context, child) {
        return Theme(
          data: Theme.of(context).copyWith(
            colorScheme: const ColorScheme.light(
              primary: kOrangeColor,
              onSurface: kOrangeColor,
            ),
            textButtonTheme: TextButtonThemeData(
              style: TextButton.styleFrom(disabledForegroundColor: kOrangeColor),
            ),
            timePickerTheme: _myTimePickerTheme(ThemeData().timePickerTheme),
          ),
          child: child!,
        );
      },
    );
    if (selectedTime != null) {
      setState(() {
        _selectedDate = _updateDateWithTime(_selectedDate, selectedTime);
        _dateController.text = _dateToString(_selectedDate!);
        _timeController.text = _timeToString(_selectedDate!);
        widget.onSelect(_selectedDate!.millisecondsSinceEpoch);
      });
    }
  }

  TimeOfDay _getTimeFromDate(DateTime? date) {
    if (date == null) {
      return TimeOfDay.fromDateTime(DateTime.now());
    } else {
      return TimeOfDay.fromDateTime(date.toUtc());
    }
  }

  DateTime _getDateFromDate(DateTime? selectedDate) {
    if (selectedDate == null) {
      return DateTime.now();
    } else {
      return _selectedDate!;
    }
  }

  DateTime _updateDateWithDate(DateTime? initialDate, DateTime time) {
    if (initialDate == null) {
      return DateTime(time.year, time.month, time.day, 12 + _timeOffsetHours, 0);
    } else {
      return DateTime(time.year, time.month, time.day, initialDate.hour, initialDate.minute);
    }
  }

  DateTime _updateDateWithTime(DateTime? initialDate, TimeOfDay time) {
    if (initialDate == null) {
      var now = DateTime.now();
      return DateTime(now.year, now.month, now.day, time.hour + _timeOffsetHours, time.minute);
    } else {
      return DateTime(initialDate.year, initialDate.month, initialDate.day, time.hour + _timeOffsetHours, time.minute);
    }
  }

  static String _dateToString(DateTime date) {
    return DateFormat.yMd(L10n.locale.languageCode).format(date.toUtc());
  }

  static String _timeToString(DateTime date) {
    return DateFormat.Hm(L10n.locale.languageCode).format(date.toUtc());
  }

  void _update() {
    if (mounted) {
      setState(() {
        if (widget.controller.dateInMillisecond != null) {
          DateTime date = DateTime.fromMillisecondsSinceEpoch(widget.controller.dateInMillisecond!, isUtc: true);
          _dateController.text = _dateToString(date);
          _timeController.text = _timeToString(date);
        } else {
          _dateController.clear();
          _timeController.clear();
        }
      });
    }
  }

  TimePickerThemeData _myTimePickerTheme(TimePickerThemeData base) {
    Color myTimePickerMaterialStateColorFunc(Set<WidgetState> states, {bool withBackgroundColor = false}) {
      const Set<WidgetState> interactiveStates = <WidgetState>{
        WidgetState.pressed,
        WidgetState.hovered,
        WidgetState.focused,
        WidgetState.selected,
      };
      if (states.any(interactiveStates.contains)) {
        return kOrangeColor.withValues(alpha: 0.3);
      }
      return Colors.transparent;
    }

    return base.copyWith(
      hourMinuteTextColor: kOrangeColor,
      hourMinuteColor: WidgetStateColor.resolveWith((Set<WidgetState> states) => myTimePickerMaterialStateColorFunc(states, withBackgroundColor: true)),
      dayPeriodTextColor: kOrangeColor,
      dayPeriodColor: WidgetStateColor.resolveWith(myTimePickerMaterialStateColorFunc),
      dialHandColor: kOrangeColor,
    );
  }
}

class DateTimePickerController {
  DateTimePickerTileState? _view;
  int? _dateInMillisecond;

  int? get dateInMillisecond => _dateInMillisecond;

  set dateInMillisecond(int? value) {
    _dateInMillisecond = value;
    _view?._update();
  }

  void setView(DateTimePickerTileState view) {
    _view = view;
  }
}
