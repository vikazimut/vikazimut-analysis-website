// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:vikazimut_website/theme/theme.dart';

import 'screen_helper.dart';

class ErrorContainer extends StatelessWidget {
  final String errorMessage;

  const ErrorContainer(this.errorMessage);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 12),
      child: Container(
        margin: const EdgeInsets.only(bottom: 10),
        padding: const EdgeInsets.all(12),
        constraints: const BoxConstraints(maxWidth: ScreenHelper.desktopMinWidth),
        width: double.infinity,
        color: kDangerBackgroundColor,
        child: Text(
          errorMessage,
          style: const TextStyle(color: kDangerFontColor),
        ),
      ),
    );
  }
}
