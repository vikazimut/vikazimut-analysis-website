// coverage:ignore-file
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:vikazimut_website/l10n/l10n.dart';

class GlobalErrorWidget extends StatelessWidget {
  final String _errorResponse;

  const GlobalErrorWidget(this._errorResponse);

  @override
  Widget build(BuildContext context) {
    String? errorMessage;
    if (_errorResponse.startsWith("{")) {
      final jsonError = json.decode(_errorResponse) as Map<String, dynamic>;
      errorMessage = jsonError["message"];
    } else {
      errorMessage = _errorResponse;
    }
    String title;
    String message;
    if (errorMessage == "error_invalid_request_argument") {
      title = L10n.getString("page_error_invalid_title");
      message = L10n.getString("page_error_invalid_message");
    } else if (errorMessage == "unauthorized") {
      title = L10n.getString("page_error_private_title");
      message = L10n.getString("page_error_private_message");
    } else {
      title = L10n.getString("page_error_server_title");
      message = errorMessage ?? "Unknown error!";
    }
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            title.toUpperCase(),
            style: Theme.of(context).textTheme.displayLarge!.copyWith(color: Colors.red),
          ),
          const SizedBox(
            height: 15,
          ),
          const SizedBox(
            height: 10,
          ),
          DefaultTextStyle.merge(
            style: const TextStyle(fontSize: 18),
            child: Text(message),
          ),
        ],
      ),
    );
  }
}
