// coverage:ignore-file
import 'dart:async';

import 'package:flutter/material.dart';

class CustomPlainButton extends StatelessWidget {
  final String label;
  final Color backgroundColor;
  final Color? foregroundColor;
  final VoidCallback? onPressed;
  final bool autofocus;

  const CustomPlainButton({
    required this.label,
    required this.backgroundColor,
    this.foregroundColor,
    required this.onPressed,
    this.autofocus = false,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      autofocus: autofocus,
      style: ElevatedButton.styleFrom(
        backgroundColor: backgroundColor,
        padding: const EdgeInsets.all(15),
        minimumSize: const Size(100, 36),
      ),
      onPressed: onPressed,
      child: Text(
        label,
        textAlign: TextAlign.center,
        style: TextStyle(color: foregroundColor),
      ),
    );
  }
}

class CustomOutlinedButton extends StatelessWidget {
  final String label;
  final Color backgroundColor;
  final Color foregroundColor;
  final VoidCallback? onPressed;
  final bool autofocus;

  const CustomOutlinedButton({
    required this.label,
    required this.backgroundColor,
    required this.foregroundColor,
    required this.onPressed,
    this.autofocus = false,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return OutlinedButton(
      autofocus: autofocus,
      style: OutlinedButton.styleFrom(
        side: BorderSide(color: backgroundColor),
        padding: const EdgeInsets.all(15),
      ),
      onPressed: onPressed,
      child: Text(
        label,
        style: TextStyle(color: foregroundColor),
      ),
    );
  }
}

class CustomLongPressPlainButtonWidget extends StatelessWidget {
  final String label;
  final VoidCallback onPressed;
  final VoidCallback? onDoublePressed;
  final Color backgroundColor;
  final Color foregroundColor;

  const CustomLongPressPlainButtonWidget({
    required this.label,
    required this.onPressed,
    this.onDoublePressed,
    required this.backgroundColor,
    required this.foregroundColor,
  });

  @override
  Widget build(BuildContext context) {
    Timer? timer;
    bool isLongPressed = false;
    return Container(
      constraints: const BoxConstraints(
        minWidth: 100,
        minHeight: 36,
      ),
      child: Material(
        color: backgroundColor,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(30)),
        ),
        child: Padding(
          padding: const EdgeInsets.all(10),
          child: InkWell(
            splashColor: Colors.transparent,
            hoverColor: Colors.transparent,
            highlightColor: Colors.transparent,
            onTapCancel: () => timer?.cancel(),
            onTapDown: (_) {
              timer = Timer(const Duration(seconds: 3), () {
                onDoublePressed?.call();
                isLongPressed = true;
              });
            },
            onTapUp: (_) {
              timer?.cancel();
              if (!isLongPressed) {
                onPressed.call();
              } else {
                isLongPressed = false;
              }
            },
            child: Text(
              label,
              textAlign: TextAlign.center,
              style: TextStyle(color: foregroundColor),
            ),
          ),
        ),
      ),
    );
  }
}
