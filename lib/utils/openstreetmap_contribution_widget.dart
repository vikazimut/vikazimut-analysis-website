// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:vikazimut_website/theme/theme.dart';

class OpenStreetMapAttributionWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.bottomRight,
      child: ColoredBox(
        color: kOrangeColorUltraLight,
        child: GestureDetector(
          onTap: () => launchUrl(Uri.parse("https://www.openstreetmap.org/copyright")),
          child: const Padding(
            padding: EdgeInsets.symmetric(horizontal: 3),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                MouseRegion(
                  cursor: SystemMouseCursors.click,
                  child: Text("© OpenStreetMap contributors", style: TextStyle(fontSize: 12, color: Colors.black)),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
