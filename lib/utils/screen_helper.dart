// coverage:ignore-file
import 'package:flutter/material.dart';

typedef WidgetBuilder = Widget Function(BuildContext);

class ScreenHelper extends StatelessWidget {
  final WidgetBuilder mobile;
  final WidgetBuilder tablet;
  final WidgetBuilder desktop;

  const ScreenHelper({
    super.key,
    required this.desktop,
    required this.mobile,
    required this.tablet,
  });

  static bool isMobile(BuildContext context) => MediaQuery.of(context).size.width < 800.0;

  static const double mobileMaxWidth = 800.0;

  static bool isTablet(BuildContext context) => MediaQuery.of(context).size.width >= 800.0 && MediaQuery.of(context).size.width < 1200.0;
  static const double tabletMaxWidth = 1200.0;

  static bool isDesktop(BuildContext context) => MediaQuery.of(context).size.width >= 1200.0;
  static const double desktopMinWidth = 1200.0;

  static double getMobileMaxWidth(BuildContext context) => MediaQuery.of(context).size.width;

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        if (isDesktop(context)) {
          return desktop(context);
        } else if (isTablet(context)) {
          return tablet(context);
        } else {
          return mobile(context);
        }
      },
    );
  }
}
