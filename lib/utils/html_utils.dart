import 'dart:convert';
import 'dart:js_interop';
import 'dart:typed_data';

import 'package:web/web.dart';

void downloadStringInFile(String filename, String contents) {
  final Uint8List bytes = utf8.encode(contents);
  final JSArrayBuffer data = Uint8List.fromList(bytes).buffer.toJS;
  final Blob blob = Blob([data].toJS);
  final String url = URL.createObjectURL(blob);
  final HTMLAnchorElement anchor = HTMLAnchorElement();
  anchor.href = url;
  anchor.download = filename;
  anchor.click();
}

void downloadDataInFile(List<int> data, String filename) {
  final JSArrayBuffer encodedData = Uint8List.fromList(data).buffer.toJS;
  final Blob blob = Blob([encodedData].toJS);
  final String url = URL.createObjectURL(blob);
  final HTMLAnchorElement anchor = HTMLAnchorElement();
  anchor.href = url;
  anchor.download = filename;
  anchor.click();
}
