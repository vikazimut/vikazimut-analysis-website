import 'dart:math' as math;

double deg2rad(double degree) => degree * math.pi / 180.0;

double rad2deg(double radian) => radian * 180.0 / math.pi;

double computeMedianValue(List<double> list) {
  assert(list.isNotEmpty);
  var listCopy = list.toList()..sort((a, b) => a.compareTo(b));
  return listCopy[(listCopy.length - 1) ~/ 2];
}

const int userIdentifierLength = 30;
final RegExp userIdentifierRegExp = RegExp(r"[ \da-zA-Zá-úÁ-Ú_.@\-']");

final RegExp phoneRegExp = RegExp(r"^\+?[ \d.-]*");
