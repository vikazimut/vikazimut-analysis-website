const String openStreetMapUrlTemplate = "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png";

const String KEY_LANGUAGE = "language";
const String KEY_THEME = "theme";
const String KEY_LOCATION = "user_location";

const String SERVER = "https://vikazimut.vikazim.fr";
// const String SERVER = "http://127.0.0.1:8000";
const String API_SERVER = "$SERVER/api";

const String SERVER_POST_GPX_URL = "$SERVER/data/track";

const String USER_LOCATION_SERVER_URL = "https://ipapi.co/json";

const double COLLAPSIBLE_MAX_WIDTH = 300;
const double COLLAPSIBLE_MIN_WIDTH = 35;

const double MAX_NUMBER_OF_SELECTED_TRACKS = 15;

const int CHAMPIONSHIP_EVENT = 0;
const int POINTS_EVENT = 1;
const int CUMULATIVE_TIME_EVENT = 2;

const double informationTextFontSize = 14;
