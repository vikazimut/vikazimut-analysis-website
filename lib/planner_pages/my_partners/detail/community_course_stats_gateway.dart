import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;

import 'community_course_stats_model.dart';

class CommunityCourseGateway {
  static Future<List<CommunityCourseStatsModel>> fetchCourses(String url) async {
    final response = await http.get(
      Uri.parse(url),
    );
    if (response.statusCode == HttpStatus.ok) {
      return (json.decode(response.body) as List).map((data) => CommunityCourseStatsModel.fromJson(data)).toList();
    } else {
      return Future.error(response.body);
    }
  }
}
