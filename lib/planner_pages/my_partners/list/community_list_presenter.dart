import 'community_data.dart';
import 'community_list_gateway.dart';

class CommunityListPagePresenter {
  static int currentDisplayedRowIndex = 0;

  const CommunityListPagePresenter();

  Future<List<CommunityData>> fetchCommunities(int plannerId) async {
    if (plannerId < -1) {
      return Future.error("Unauthorized request");
    }
    try {
      return CommunityListGateway.fetchCommunities(plannerId);
    } catch (_) {
      return Future.error("Access token error");
    }
  }
}
