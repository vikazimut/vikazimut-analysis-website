import 'package:encrypt/encrypt.dart' as encrypt;
import 'package:vikazimut_website/keys.dart';

class CommunityData {
  final String name;

  CommunityData({
    required this.name,
  });

  factory CommunityData.fromJson(Map<String, dynamic> json) {
    return CommunityData(
      name: json["name"] as String,
    );
  }

  String encryptName() {
    final key = encrypt.Key.fromBase64(Secret.key);
    final iv = encrypt.IV.fromBase64(Secret.initializationVector);
    final encrypter = encrypt.Encrypter(encrypt.AES(key, mode: encrypt.AESMode.cbc));
    encrypt.Encrypted? encrypted = encrypter.encrypt(name, iv: iv);
    return Uri.encodeQueryComponent(encrypted.base64);
  }
}
