import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:vikazimut_website/common/footer.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/menu/responsive_scaffold_widget.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/screen_helper.dart';

import 'markdown/style_sheet.dart';
import 'markdown/widget.dart';
import 'table_of_contents.dart';

class PlannerDocPage extends StatefulWidget {
  static const String routePath = '/planner/doc';
  final String? section;
  static const Color _kTipForegroundColor = Color(0xff3e8554);
  static const Color _kTipBackgroundColor = Color(0xffd6e3da);

  const PlannerDocPage({this.section});

  @override
  State<PlannerDocPage> createState() => _PlannerDocPageState();
}

class _PlannerDocPageState extends State<PlannerDocPage> {
  final _presenter = DocPresenter();
  late Future<String> _documentation;
  final ScrollController _scrollController = ScrollController();
  List<IndexInTableOfContents> _tableOfContents = [];

  @override
  void initState() {
    _documentation = _presenter.readDocFile(widget.section);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ResponsiveScaffoldWidget(
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            FutureBuilder<String>(
                future: _documentation,
                builder: (context, snapshot) {
                  if (snapshot.hasError) {
                    return Expanded(child: Center(child: Text("Internal error: ${snapshot.error}")));
                  }
                  if (!snapshot.hasData) {
                    return const Expanded(child: Center(child: CircularProgressIndicator()));
                  }
                  _tableOfContents = _presenter.extractTableOfContents(snapshot.data!);
                  return Flexible(
                    child: Container(
                      constraints: const BoxConstraints(maxWidth: ScreenHelper.desktopMinWidth),
                      child: ScreenHelper.isDesktop(context) ? _buildDesktopContents(context, snapshot.data!, _tableOfContents) : _buildMobileContents(context, snapshot.data!, _tableOfContents),
                    ),
                  );
                }),
            const SizedBox(height: 40),
            Footer(),
          ],
        ),
      ),
    );
  }

  Widget _buildDesktopContents(
    BuildContext context,
    String contents,
    List<IndexInTableOfContents> tableOfContents,
  ) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        TableOfContents(tableOfContents: tableOfContents),
        Expanded(
          child: Markdown(
            data: contents,
            selectable: true,
            tableOfContents: tableOfContents,
            onTapLink: (String text, String? href, String title) {
              if (href != null) {
                launchUrl(Uri.parse(href));
              }
            },
            styleSheet: _buildMarkdownStyleSheet(context),
          ),
        ),
      ],
    );
  }

  MarkdownStyleSheet _buildMarkdownStyleSheet(BuildContext context) {
    return MarkdownStyleSheet(
      a: const TextStyle(color: kOrangeColor),
      h1: TextStyle(color: Theme.of(context).hintColor, fontSize: 40),
      h1Padding: const EdgeInsets.fromLTRB(10, 25, 10, 10),
      h2: TextStyle(fontSize: 32, color: Theme.of(context).hintColor),
      h3: TextStyle(fontSize: 24, color: Theme.of(context).hintColor),
      h4: const TextStyle(fontSize: 18, color: PlannerDocPage._kTipForegroundColor),
      code: TextStyle(fontSize: 14, color: Colors.pink[300]!, backgroundColor: Colors.transparent),
      pPadding: const EdgeInsets.only(bottom: 10),
      p: Theme.of(context).textTheme.bodyMedium?.copyWith(height: 1.7),
      codeblockDecoration: const BoxDecoration(color: Colors.black),
      blockquote: const TextStyle(color: kTextColorLightTheme),
      blockquotePadding: const EdgeInsets.fromLTRB(15, 15, 5, 15),
      blockquoteDecoration: const BoxDecoration(
        color: PlannerDocPage._kTipBackgroundColor,
        border: Border(
          left: BorderSide(color: PlannerDocPage._kTipForegroundColor, width: 4),
        ),
      ),
      horizontalRuleDecoration: const BoxDecoration(
        border: Border(
          top: BorderSide(
            width: 2.0,
            color: kOrangeColor,
          ),
        ),
      ),
    );
  }

  Widget _buildMobileContents(
    BuildContext context,
    String contents,
    List<IndexInTableOfContents> tableOfContents,
  ) {
    return Scrollbar(
      thumbVisibility: true,
      controller: _scrollController,
      child: SingleChildScrollView(
        controller: _scrollController,
        child: Markdown(
          data: contents,
          selectable: true,
          tableOfContents: tableOfContents,
          onTapLink: (String text, String? href, String title) {
            if (href != null) {
              launchUrl(Uri.parse(href));
            }
          },
          styleSheet: _buildMarkdownStyleSheet(context),
        ),
      ),
    );
  }

  void scrollToItem(String section) {
    int? index = int.tryParse(section);
    if (index != null && _tableOfContents[index - 1].key.currentContext != null) {
      final BuildContext context = _tableOfContents[index - 1].key.currentContext!;
      Scrollable.ensureVisible(context);
    }
  }
}

class DocPresenter {
  Future<String> readDocFile(String? section) async {
    return await rootBundle.loadString('assets/doc/doc-$section-${L10n.locale.languageCode}.md');
  }

  List<IndexInTableOfContents> extractTableOfContents(String contents) {
    final headerRegex = RegExp(r'^(##?) (.*)', multiLine: true);
    List<IndexInTableOfContents> tableOfContents = headerRegex.allMatches(contents).map((m) {
      return IndexInTableOfContents(m.group(1)!.length, m.group(2)!, GlobalKey());
    }).toList();
    return tableOfContents;
  }
}

class IndexInTableOfContents {
  int level;
  String title;
  GlobalKey<State<StatefulWidget>> key;

  IndexInTableOfContents(this.level, this.title, this.key);
}
