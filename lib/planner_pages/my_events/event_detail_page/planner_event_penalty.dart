import 'dart:convert';

class Penalty {
  int? eventCourseId;
  int? participantId;
  int? missingPunchPenalty;
  int? overtimePenalty;

  dynamic toJson() {
    return json.encode({
      "event_course_id": eventCourseId,
      "participant_id": participantId,
      "missing_punch_penalty": missingPunchPenalty,
      "overtime_penalty": overtimePenalty,
    });
  }
}
