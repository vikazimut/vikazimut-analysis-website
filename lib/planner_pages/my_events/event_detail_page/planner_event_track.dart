class EventTrack {
  final int totalScoreWithPenalty;
  final int missingPunchPenaltyCount;
  final int overtimePenaltyCount;
  final int totalTimeWithPenalty;
  final int totalTime;
  final int grossTotalScore;
  final int missingPunchPenalty;
  final int overtimePenalty;
  final List<int> punchTimes;

  EventTrack({
    required this.totalScoreWithPenalty,
    required this.missingPunchPenaltyCount,
    required this.overtimePenaltyCount,
    required this.totalTime,
    required this.totalTimeWithPenalty,
    required this.punchTimes,
    required this.grossTotalScore,
    required this.missingPunchPenalty,
    required this.overtimePenalty,
  });

  factory EventTrack.fromJson(List<dynamic> json) {
    return EventTrack(
      totalScoreWithPenalty: int.tryParse("${json[0]}") ?? 0,
      missingPunchPenaltyCount: int.tryParse("${json[1]}") ?? 0,
      overtimePenaltyCount: int.tryParse("${json[2]}") ?? 0,
      totalTime: int.tryParse("${json[3]}") ?? 0,
      totalTimeWithPenalty: int.tryParse("${json[4]}") ?? 0,
      punchTimes: (json[5] as List<dynamic>).cast<int>(),
      grossTotalScore: int.tryParse("${json[6]}") ?? 0,
      missingPunchPenalty: int.tryParse("${json[7]}") ?? 0,
      overtimePenalty: int.tryParse("${json[8]}") ?? 0,
    );
  }
}
