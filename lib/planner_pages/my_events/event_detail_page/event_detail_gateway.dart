import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:vikazimut_website/authentication/identifier_user.dart';
import 'package:vikazimut_website/constants.dart' as constants;

import 'planner_event_detail.dart';

class EventDetailGateway {
  static Future<EventDetail?> loadEventInfo(int eventId) async {
    var user = await IdentifiedUser.getUser();
    var accessToken = user?.accessToken;
    if (accessToken == null) {
      return Future.error("Access token error");
    } else {
      final response = await http.get(
        Uri.parse("${constants.API_SERVER}/planner/my-event/detail/$eventId"),
        headers: {
          HttpHeaders.authorizationHeader: "Bearer $accessToken",
        },
      );
      if (response.statusCode == HttpStatus.ok) {
        var data = jsonDecode(response.body);
        try {
          return EventDetail.fromJson(data);
        } catch (e) {
          return Future.error(e.toString());
        }
      } else {
        return Future.error(response.body);
      }
    }
  }
}
