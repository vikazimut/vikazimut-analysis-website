import 'package:flutter/foundation.dart';

@immutable
class EventCourse {
  final int id;
  final int courseId;
  final String courseName;
  final int format;
  final int maxTime;
  final int missingPunchPenalty;
  final int overtimePenalty;
  final List<String> checkpointIds;

  const EventCourse({
    required this.id,
    required this.courseId,
    required this.courseName,
    required this.format,
    required this.maxTime,
    required this.missingPunchPenalty,
    required this.overtimePenalty,
    required this.checkpointIds,
  });

  factory EventCourse.fromJson(Map<String, dynamic> json) {
    return EventCourse(
      id: json["id"] as int,
      courseId: json["courseId"] as int,
      courseName: json["name"] as String,
      format: json["format"] as int,
      maxTime: json["maxTime"] as int,
      missingPunchPenalty: json["missingPunchPenalty"] as int,
      overtimePenalty: json["overtimePenalty"] as int,
      checkpointIds: (json["checkpoints"] as List<dynamic>).cast<String>(),
    );
  }
}