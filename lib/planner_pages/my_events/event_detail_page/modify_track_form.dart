// coverage:ignore-file
part of 'planner_event_detail_page.dart';

class _ModifyTrackFormDialog {
  final int courseId;
  final int participantId;
  int? _trackId;
  final ValueNotifier<int> _validated = ValueNotifier(0);

  _ModifyTrackFormDialog(this.courseId, this.participantId);

  Widget _buildPopupAddCourse(BuildContext context) {
    return AlertDialog(
      title: Text(
        L10n.getString("event_detail_track_modify_title"),
        style: const TextStyle(color: kOrangeColor),
      ),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(L10n.getString("event_detail_track_modify_select_track")),
          const SizedBox(height: 12),
          FutureBuilder<List<_Track>>(
              future: _Presenter._getTracks(courseId),
              builder: (BuildContext context, AsyncSnapshot<List<_Track>> snapshot) {
                if (snapshot.hasError) {
                  return GlobalErrorWidget(snapshot.error.toString());
                } else if (snapshot.hasData) {
                  return StatefulBuilder(builder: (BuildContext context, StateSetter dropDownState) {
                    return CustomDropdownButton<int>(
                      height: 42,
                      value: _trackId,
                      items: snapshot.data!.map((track) => DropdownMenuItem<int>(value: track.id, child: Text(track.name))).toList(),
                      onChanged: (int? value) {
                        dropDownState(() {
                          _trackId = value;
                          _validated.value++;
                        });
                      },
                    );
                  });
                } else {
                  return const SizedBox(height: 42);
                }
              }),
        ],
      ),
      actions: <Widget>[
        AnimatedBuilder(
          animation: _validated,
          builder: (_, __) =>
              ElevatedButton(
                onPressed: (_trackId != null) ?
                    () => Navigator.of(context).pop(_trackId)
                    :null,
                style: ElevatedButton.styleFrom(
                  backgroundColor: kOrangeColor,
                  padding: const EdgeInsets.all(15),
                ),
                child: Text(
                  L10n.getString("event_detail_track_modify_button_ok"),
                ),
              ),
        ),
        CustomOutlinedButton(
          foregroundColor: kOrangeColor,
          backgroundColor: kOrangeColor,
          onPressed: () {
            Navigator.of(context).pop(null);
          },
          label: L10n.getString("event_detail_track_modify_button_cancel"),
        ),
      ],
    );
  }

  Future<int?> execute(BuildContext context) async {
    return await showDialog<int>(
      context: context,
      builder: (BuildContext context) => _buildPopupAddCourse(context),
    );
  }
}

class _Presenter {
  static Future<List<_Track>>? _getTracks(int courseId) async {
    const String GET_URL = "${constants.API_SERVER}/planner/my-course/tracks";
    var user = await IdentifiedUser.getUser();
    var accessToken = user?.accessToken;
    if (accessToken == null) {
      return Future.error("Access token error");
    } else {
      final response = await http.get(
        Uri.parse("$GET_URL/$courseId"),
        headers: {
          HttpHeaders.authorizationHeader: "Bearer $accessToken",
        },
      );
      if (response.statusCode == HttpStatus.ok) {
        return (json.decode(response.body) as List).map((data) => _Track.fromJson(data)).toList();
      } else {
        return Future.error(response.body);
      }
    }
  }
}

class _Track {
  final int id;
  final String name;

  _Track({required this.id, required this.name});

  factory _Track.fromJson(Map<String, dynamic> json) {
    return _Track(
      id: json['id'] as int,
      name: json['name'] as String,
    );
  }
}
