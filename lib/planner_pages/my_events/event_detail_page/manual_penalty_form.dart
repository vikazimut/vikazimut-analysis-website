// coverage:ignore-file
part of 'planner_event_detail_page.dart';

class _ManualPenaltyFormDialog {
  final Penalty _penalty = Penalty();
  final Participant _participant;
  final EventCourse _eventCourse;
  final int? _missingPunchPenalty;
  final int? _overtimePenalty;
  final ValueNotifier<int> _validated = ValueNotifier(0);

  _ManualPenaltyFormDialog(
    this._participant,
    this._eventCourse,
    this._missingPunchPenalty,
    this._overtimePenalty,
  );

  Widget _buildPopupAddCourse(BuildContext context) {
    _penalty.participantId = _participant.id;
    _penalty.eventCourseId = _eventCourse.id;
    _penalty.missingPunchPenalty = _missingPunchPenalty;
    _penalty.overtimePenalty = _overtimePenalty;
    return AlertDialog(
      insetPadding: EdgeInsets.zero,
      title: Text(
        L10n.getString("manual_penalty_form_title"),
        style: const TextStyle(color: kOrangeColor),
      ),
      content: Container(
        constraints: const BoxConstraints(minWidth: 350),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text("${L10n.getString("manual_penalty_form_participant_label")} ${_participant.nickname}"),
            const SizedBox(height: 8),
            Text("${L10n.getString("manual_penalty_form_course_label")} ${_eventCourse.courseName}"),
            const SizedBox(height: 15),
            Text(L10n.getString("manual_penalty_form_missing_label")),
            Center(
              child: IntegerPickerWidget(
                initialValue: _penalty.missingPunchPenalty!,
                onChanged: (int value) {
                  _penalty.missingPunchPenalty = value;
                  _validated.value++;
                },
              ),
            ),
            const SizedBox(height: 15),
            Text(L10n.getString("manual_penalty_form_overtime_label")),
            Center(
              child: IntegerPickerWidget(
                initialValue: _penalty.overtimePenalty!,
                onChanged: (int value) {
                  _penalty.overtimePenalty = value;
                  _validated.value++;
                },
              ),
            ),
          ],
        ),
      ),
      actions: <Widget>[
        AnimatedBuilder(
          animation: _validated,
          builder: (_, __) => ElevatedButton(
            onPressed: (_validateValues()) ? () => Navigator.of(context).pop(_penalty) : null,
            style: ElevatedButton.styleFrom(
              backgroundColor: kOrangeColor,
              padding: const EdgeInsets.all(15),
            ),
            child: Text(L10n.getString("manual_penalty_form_change_button")),
          ),
        ),
        CustomOutlinedButton(
          foregroundColor: kOrangeColor,
          backgroundColor: kOrangeColor,
          onPressed: () {
            Navigator.of(context).pop(null);
          },
          label: L10n.getString("manual_penalty_form_cancel_button"),
        ),
      ],
    );
  }

  bool _validateValues() => _penalty.missingPunchPenalty != _missingPunchPenalty || _penalty.overtimePenalty != _overtimePenalty;

  Future<Penalty?> execute(BuildContext context) async {
    return await showDialog<Penalty?>(
      context: context,
      builder: (BuildContext context) => _buildPopupAddCourse(context),
    );
  }
}
