// coverage:ignore-file
library;

import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:http/http.dart' as http;
import 'package:sprintf/sprintf.dart';
import 'package:vikazimut_website/authentication/identifier_user.dart';
import 'package:vikazimut_website/common/footer.dart';
import 'package:vikazimut_website/constants.dart' as constants;
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/menu/responsive_scaffold_widget.dart';
import 'package:vikazimut_website/planner_pages/my_events/event_detail_page/planner_event_participant.dart';
import 'package:vikazimut_website/public_pages/routes/route_list_page_view.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/alert_dialog.dart';
import 'package:vikazimut_website/utils/container_decoration.dart';
import 'package:vikazimut_website/utils/custom_button.dart';
import 'package:vikazimut_website/utils/custom_dropdown_button.dart';
import 'package:vikazimut_website/utils/dropdown_button.dart';
import 'package:vikazimut_website/utils/error_container.dart';
import 'package:vikazimut_website/utils/global_error_widget.dart';
import 'package:vikazimut_website/utils/html_utils.dart';
import 'package:vikazimut_website/utils/integer_picker_widget.dart';
import 'package:vikazimut_website/utils/screen_helper.dart';
import 'package:vikazimut_website/utils/time.dart';

import '../event_course_page/planner_event_course_view.dart';
import '../event_participants/planner_event_participant_view.dart';
import 'event_detail_gateway.dart';
import 'planner_event_course.dart';
import 'planner_event_detail.dart';
import 'planner_event_penalty.dart';

part 'csv_generator.dart';
part 'manual_penalty_form.dart';
part 'modify_track_form.dart';

class PlannerEventDetailPage extends StatefulWidget {
  static const String routePath = '/planner/events/detail';
  final int eventId;

  PlannerEventDetailPage(args) : eventId = int.parse(args);

  @override
  State<PlannerEventDetailPage> createState() => _PlannerEventDetailPageState();
}

class _PlannerEventDetailPageState extends State<PlannerEventDetailPage> {
  final _EventPagePresenter _eventPagePresenter = _EventPagePresenter();
  final ScrollController _scrollController = ScrollController();
  Future<EventDetail?>? _eventDetail;
  String? _errorMessage;

  @override
  void initState() {
    _eventPagePresenter.setView(this);
    _eventDetail = EventDetailGateway.loadEventInfo(widget.eventId);
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ResponsiveScaffoldWidget(
      body: Column(
        children: [
          Flexible(
            child: ContainerDecoration(
              child: FutureBuilder<EventDetail?>(
                future: _eventDetail,
                builder: (BuildContext context, AsyncSnapshot<EventDetail?> snapshot) {
                  if (snapshot.hasError) {
                    return GlobalErrorWidget(snapshot.error.toString());
                  } else if (snapshot.hasData) {
                    return Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 5),
                          child: Text(
                            snapshot.data!.name,
                            style: Theme.of(context).textTheme.displayMedium,
                          ),
                        ),
                        Expanded(
                          child: _EventInfoDetailWidget(
                            snapshot.data!,
                            _eventPagePresenter,
                          ),
                        ),
                        const SizedBox(height: 10),
                        if (_errorMessage != null) ErrorContainer(_errorMessage!),
                        Wrap(
                          runSpacing: 5,
                          direction: Axis.horizontal,
                          alignment: WrapAlignment.center,
                          spacing: 5,
                          children: [
                            CustomPlainButton(
                              backgroundColor: kOrangeColor,
                              label: L10n.getString("planner_event_detail_page_update_button"),
                              onPressed: () {
                                _eventPagePresenter.update(snapshot.data!.id);
                              },
                            ),
                            CustomPlainButton(
                              backgroundColor: kInfoColor,
                              label: L10n.getString("planner_event_detail_page_csv_button"),
                              onPressed: () {
                                String data = _eventPagePresenter.exportCsv(snapshot.data!);
                                downloadStringInFile("${widget.eventId}.csv", data);
                              },
                            ),
                          ],
                        ),
                        const SizedBox(height: 5),
                        Wrap(
                          runSpacing: 5,
                          direction: Axis.horizontal,
                          alignment: WrapAlignment.center,
                          spacing: 5,
                          children: [
                            CustomPlainButton(
                              label: L10n.getString("planner_event_list_page_button_courses"),
                              backgroundColor: kInfoColor,
                              onPressed: () => GoRouter.of(context).go("${PlannerEventCourseView.routePath}/${widget.eventId}"),
                            ),
                            CustomPlainButton(
                              label: L10n.getString("planner_event_list_page_button_participants"),
                              backgroundColor: kWarningColor,
                              foregroundColor: Colors.black,
                              onPressed: () => GoRouter.of(context).go("${PlannerEventParticipantView.routePath}/${widget.eventId}"),
                            ),
                          ],
                        ),
                        if (!ScreenHelper.isMobile(context)) const SizedBox(height: 5),
                      ],
                    );
                  } else {
                    return const Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                },
              ),
            ),
          ),
          Footer(),
        ],
      ),
    );
  }

  void update() {
    if (mounted) {
      setState(() {
        _errorMessage = null;
        _eventDetail = EventDetailGateway.loadEventInfo(widget.eventId);
      });
    }
  }

  void displayToastMessage(String message) {
    ScaffoldMessenger.of(context)
      ..hideCurrentSnackBar()
      ..showSnackBar(
        SnackBar(content: Text(message)),
      );
  }

  void showError(String message) {
    setState(() {
      _errorMessage = message;
    });
  }

  Future<void> displayErrors(String errorMessage) async {
    await showErrorDialog(context, title: L10n.getString("page_error_server_title"), message: errorMessage);
  }
}

class _EventPagePresenter {
  _PlannerEventDetailPageState? _view;

  void update(int eventId) async {
    const String URL = "${constants.API_SERVER}/planner/my-event/update-ranking/%d";
    var user = await IdentifiedUser.getUser();
    var accessToken = user?.accessToken;
    if (accessToken == null) {
      return Future.error("Access token error");
    } else {
      final response = await http.post(
        Uri.parse(sprintf(URL, [eventId])),
        headers: {
          HttpHeaders.authorizationHeader: "Bearer $accessToken",
        },
      );
      if (response.statusCode == HttpStatus.ok) {
        _view?.displayToastMessage(L10n.getString("event_detail_updated"));
        _view?.update();
      } else {
        _view?.displayErrors(response.body);
      }
    }
  }

  void setView(_PlannerEventDetailPageState view) {
    _view = view;
  }

  String exportCsv(EventDetail eventDetails) {
    return _CSVGenerator.createCSV(eventDetails);
  }

  void addManualPenalty(int id, Penalty data) async {
    var user = await IdentifiedUser.getUser();
    var accessToken = user?.accessToken;
    if (accessToken == null) {
      return Future.error("Access token error");
    } else {
      const String URL = "${constants.API_SERVER}/planner/my-event/%d/add-penalty";
      final response = await http.patch(
        Uri.parse(sprintf(URL, [id])),
        headers: {
          HttpHeaders.authorizationHeader: "Bearer $accessToken",
        },
        body: data.toJson(),
      );
      if (response.statusCode == HttpStatus.ok) {
        _view?.update();
      } else {
        _view?.showError(response.body);
      }
    }
  }
}

class _EventInfoDetailWidget extends StatelessWidget {
  final EventDetail eventDetails;
  final _EventPagePresenter presenter;
  final TextEditingController textEditingController = TextEditingController();

  _EventInfoDetailWidget(this.eventDetails, this.presenter);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            child: Text.rich(
              TextSpan(
                children: [
                  TextSpan(
                    text: L10n.getString("event_info_type"),
                    style: Theme.of(context).textTheme.headlineMedium!.copyWith(color: kOrangeColor),
                  ),
                  TextSpan(
                    text: " ${eventDetails.typeName}",
                    style: Theme.of(context).textTheme.headlineMedium,
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            child: DefaultTabController(
              length: 2,
              child: Column(
                children: [
                  Container(
                    constraints: const BoxConstraints(maxWidth: 500),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(
                        25.0,
                      ),
                    ),
                    child: TabBar(
                      labelColor: Colors.white,
                      labelPadding: const EdgeInsets.only(top: 10, bottom: 10),
                      tabs: [
                        Text("${L10n.getString("event_info_participants")} (${eventDetails.participants.length})"),
                        Text("${L10n.getString("event_info_courses")} (${eventDetails.eventCourses.length})"),
                      ],
                    ),
                  ),
                  Expanded(
                    child: TabBarView(
                      children: [
                        Align(
                          alignment: Alignment.topCenter,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: _EventInfoParticipantsWidget(
                              eventDetails: eventDetails,
                              presenter: presenter,
                            ),
                          ),
                        ),
                        Align(
                          alignment: Alignment.topCenter,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: _EventInfoCoursesWidget(
                              eventCourses: eventDetails.eventCourses,
                              eventType: eventDetails.type,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class _EventInfoParticipantsWidget extends StatefulWidget {
  final EventDetail eventDetails;
  final _EventPagePresenter eventPagePresenter;

  const _EventInfoParticipantsWidget({required this.eventDetails, required _EventPagePresenter presenter}) : eventPagePresenter = presenter;

  @override
  State<_EventInfoParticipantsWidget> createState() => _EventInfoParticipantsWidgetState();
}

class _EventInfoParticipantsWidgetState extends State<_EventInfoParticipantsWidget> {
  final ScrollController _horizontalScrollController = ScrollController();
  final ScrollController _verticalScrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return Scrollbar(
      controller: _verticalScrollController,
      interactive: true,
      thumbVisibility: true,
      child: Scrollbar(
        controller: _horizontalScrollController,
        interactive: true,
        thumbVisibility: true,
        notificationPredicate: (ScrollNotification notification) => notification.depth == 1,
        child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          controller: _verticalScrollController,
          child: SingleChildScrollView(
            controller: _horizontalScrollController,
            scrollDirection: Axis.horizontal,
            child: Center(child: _buildTable(context)),
          ),
        ),
      ),
    );
  }

  Widget _buildTable(BuildContext context) {
    return DataTable(
      dataRowMaxHeight: 70,
      dataRowMinHeight: 70,
      border: TableBorder.all(
        style: BorderStyle.solid,
        width: 1,
        color: kOrangeColorDisabled,
      ),
      horizontalMargin: 10,
      showBottomBorder: true,
      columnSpacing: ScreenHelper.isDesktop(context) ? 15 : 5,
      columns: <DataColumn>[
        const DataColumn(
          label: Expanded(
            child: Text(
              "#",
              textAlign: TextAlign.center,
            ),
          ),
        ),
        DataColumn(
          label: Expanded(
            child: Text(
              L10n.getString("event_info_participants_nickname"),
              textAlign: TextAlign.center,
            ),
          ),
        ),
        if (widget.eventDetails.type == constants.CHAMPIONSHIP_EVENT || widget.eventDetails.type == constants.POINTS_EVENT)
          DataColumn(
            label: Expanded(
              child: Text(
                L10n.getString("event_info_participants_total_score"),
                textAlign: TextAlign.center,
              ),
            ),
          ),
        DataColumn(
          label: Expanded(
            child: Text(
              L10n.getString("event_info_participants_total_time"),
              textAlign: TextAlign.center,
            ),
          ),
        ),
        if (widget.eventDetails.type == constants.POINTS_EVENT)
          DataColumn(
            label: Expanded(
              child: Text(
                L10n.getString("event_info_participants_gross_score"),
                textAlign: TextAlign.center,
              ),
            ),
          ),
        DataColumn(
          label: Expanded(
            child: Text(
              L10n.getString("event_info_participants_total_penalty"),
              textAlign: TextAlign.center,
            ),
          ),
        ),
        DataColumn(
          label: Expanded(
            child: Text(
              L10n.getString("event_info_participants_progress"),
              textAlign: TextAlign.center,
            ),
          ),
        ),
        for (EventCourse course in widget.eventDetails.eventCourses)
          DataColumn(
            label: Expanded(
              child: Text(
                "${course.courseName}\n${L10n.getString("event_info_participants_penalties")}",
                textAlign: TextAlign.center,
              ),
            ),
          ),
      ],
      rows: [
        for (int i = 0; i < widget.eventDetails.participants.length; i++)
          DataRow(
            color: i.isEven ? WidgetStateProperty.all(Theme.of(context).colorScheme.inversePrimary) : WidgetStateProperty.all(Theme.of(context).colorScheme.surface),
            cells: <DataCell>[
              DataCell(
                Container(
                  alignment: Alignment.center,
                  child: Text(
                    "${i + 1}",
                  ),
                ),
              ),
              DataCell(
                Text(widget.eventDetails.participants[i].nickname),
              ),
              if (widget.eventDetails.type == constants.CHAMPIONSHIP_EVENT || widget.eventDetails.type == constants.POINTS_EVENT)
                DataCell(
                  Container(alignment: Alignment.center, child: Text("${widget.eventDetails.participants[i].totalScore}")),
                ),
              DataCell(
                Container(alignment: Alignment.center, child: Text(timeToString(widget.eventDetails.participants[i].totalTime, withHour: true))),
              ),
              if (widget.eventDetails.type == constants.POINTS_EVENT)
                DataCell(
                  Container(alignment: Alignment.center, child: Text("${widget.eventDetails.participants[i].grossScore}")),
                ),
              if (widget.eventDetails.type == constants.CHAMPIONSHIP_EVENT || widget.eventDetails.type == constants.CUMULATIVE_TIME_EVENT)
                DataCell(
                  Container(alignment: Alignment.center, child: Text(timeToString(widget.eventDetails.participants[i].totalPenalty, withHour: true))),
                ),
              if (widget.eventDetails.type == constants.POINTS_EVENT)
                DataCell(
                  Container(alignment: Alignment.center, child: Text("${widget.eventDetails.participants[i].totalPenalty}")),
                ),
              DataCell(
                Container(alignment: Alignment.center, child: Text("${widget.eventDetails.participants[i].progress} / ${widget.eventDetails.eventCourses.length}")),
              ),
              if (widget.eventDetails.type == constants.CHAMPIONSHIP_EVENT || widget.eventDetails.type == constants.POINTS_EVENT)
                for (int j = 0; j < widget.eventDetails.participants[i].eventTracks.length; j++)
                  DataCell(
                    Container(
                      alignment: Alignment.center,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("${widget.eventDetails.participants[i].eventTracks[j].totalScoreWithPenalty} (${widget.eventDetails.participants[i].eventTracks[j].missingPunchPenaltyCount}|${widget.eventDetails.participants[i].eventTracks[j].overtimePenaltyCount})"),
                          Padding(
                            padding: const EdgeInsets.only(top: 2.0),
                            child: DropDownButton(
                              buttonText: L10n.getString("planner_event_detail_track_button"),
                              itemTexts: [
                                L10n.getString("event_detail_track_modify_title"),
                                L10n.getString("planner_event_detail_page_penalty_button"),
                              ],
                              dense: true,
                              backgroundColor: (widget.eventDetails.participants[i].modifiedStates[j]) ? kWarningColor : kSuccessColor,
                              foregroundColor: (widget.eventDetails.participants[i].modifiedStates[j]) ? Colors.black : Colors.white,
                              onChanged: (int? value) async {
                                if (value == 0) {
                                  int? trackId = await _ModifyTrackFormDialog(widget.eventDetails.eventCourses[j].courseId, widget.eventDetails.participants[i].id).execute(context);
                                  if (trackId != null) {
                                    bool result = await _EventInfoParticipantsPresenter.modifyParticipantTrack(widget.eventDetails.eventCourses[j].id, widget.eventDetails.participants[i].id, trackId);
                                    if (result) {
                                      widget.eventPagePresenter._view?.update();
                                    }
                                  }
                                } else {
                                  Penalty? penalty = await _ManualPenaltyFormDialog(
                                    widget.eventDetails.participants[i],
                                    widget.eventDetails.eventCourses[j],
                                    widget.eventDetails.participants[i].eventTracks[j].missingPunchPenaltyCount,
                                    widget.eventDetails.participants[i].eventTracks[j].overtimePenaltyCount,
                                  ).execute(context);
                                  if (penalty != null) {
                                    widget.eventPagePresenter.addManualPenalty(widget.eventDetails.id, penalty);
                                  }
                                }
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
              else
                for (int j = 0; j < widget.eventDetails.participants[i].eventTracks.length; j++)
                  DataCell(
                    Container(
                      alignment: Alignment.center,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("${timeToString(widget.eventDetails.participants[i].eventTracks[j].totalTimeWithPenalty)} (${widget.eventDetails.participants[i].eventTracks[j].missingPunchPenaltyCount}|${widget.eventDetails.participants[i].eventTracks[j].overtimePenaltyCount})"),
                          Padding(
                            padding: const EdgeInsets.only(top: 2.0),
                            child: DropDownButton(
                              buttonText: L10n.getString("planner_event_detail_track_button"),
                              itemTexts: [
                                L10n.getString("event_detail_track_modify_title"),
                                L10n.getString("planner_event_detail_page_penalty_button"),
                              ],
                              dense: true,
                              backgroundColor: (widget.eventDetails.participants[i].modifiedStates[j]) ? kWarningColor : kSuccessColor,
                              foregroundColor: (widget.eventDetails.participants[i].modifiedStates[j]) ? Colors.black : Colors.white,
                              onChanged: (int? value) async {
                                if (value == 0) {
                                  int? trackId = await _ModifyTrackFormDialog(widget.eventDetails.eventCourses[j].courseId, widget.eventDetails.participants[i].id).execute(context);
                                  if (trackId != null) {
                                    bool result = await _EventInfoParticipantsPresenter.modifyParticipantTrack(widget.eventDetails.eventCourses[j].id, widget.eventDetails.participants[i].id, trackId);
                                    if (result) {
                                      widget.eventPagePresenter._view?.update();
                                    }
                                  }
                                } else {
                                  Penalty? penalty = await _ManualPenaltyFormDialog(
                                    widget.eventDetails.participants[i],
                                    widget.eventDetails.eventCourses[j],
                                    widget.eventDetails.participants[i].eventTracks[j].missingPunchPenaltyCount,
                                    widget.eventDetails.participants[i].eventTracks[j].overtimePenaltyCount,
                                  ).execute(context);
                                  if (penalty != null) {
                                    widget.eventPagePresenter.addManualPenalty(widget.eventDetails.id, penalty);
                                  }
                                }
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
            ],
          )
      ],
    );
  }

  String getTimeFromString(String string) {
    try {
      var integer = int.parse(string);
      return timeToString(integer, withHour: true);
    } catch (_) {
      return string;
    }
  }

  void update() {
    if (mounted) {
      setState(() {});
    }
  }
}

class _EventInfoParticipantsPresenter {
  static Future<bool> modifyParticipantTrack(int eventCourseId, int participantId, int trackId) async {
    var user = await IdentifiedUser.getUser();
    var accessToken = user?.accessToken;
    if (accessToken == null) {
      return Future.error("Access token error");
    } else {
      final response = await http.patch(
        Uri.parse("${constants.API_SERVER}/planner/my-event/modify-event-track"),
        headers: {
          HttpHeaders.authorizationHeader: "Bearer $accessToken",
        },
        body: jsonEncode({
          "event_course_id": eventCourseId,
          "participant_id": participantId,
          "track_id": trackId,
        }),
      );
      if (response.statusCode == HttpStatus.ok) {
        return true;
      } else {
        return false;
      }
    }
  }
}

class _EventInfoCoursesWidget extends StatelessWidget {
  final List<EventCourse> eventCourses;
  final int eventType;
  final ScrollController _horizontalScrollController = ScrollController();
  final ScrollController _verticalScrollController = ScrollController();

  _EventInfoCoursesWidget({required this.eventCourses, required this.eventType});

  @override
  Widget build(BuildContext context) {
    return Scrollbar(
      controller: _verticalScrollController,
      interactive: true,
      thumbVisibility: true,
      child: Scrollbar(
        notificationPredicate: (ScrollNotification notification) => notification.depth == 1,
        controller: _horizontalScrollController,
        interactive: true,
        thumbVisibility: true,
        child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          controller: _verticalScrollController,
          child: SingleChildScrollView(
            controller: _horizontalScrollController,
            scrollDirection: Axis.horizontal,
            child: Center(child: _buildTable(context)),
          ),
        ),
      ),
    );
  }

  Widget _buildTable(BuildContext context) {
    bool isPointUnit = eventType == 1;
    return DataTable(
      border: TableBorder.all(
        style: BorderStyle.solid,
        width: 1,
        color: kOrangeColorDisabled,
      ),
      dataRowColor: WidgetStateProperty.all(Theme.of(context).colorScheme.surface),
      horizontalMargin: 10,
      showBottomBorder: true,
      columnSpacing: ScreenHelper.isDesktop(context) ? 15 : 5,
      columns: <DataColumn>[
        DataColumn(
          label: Expanded(
            child: Text(
              L10n.getString("event_info_courses_course"),
              textAlign: TextAlign.center,
            ),
          ),
        ),
        DataColumn(
          label: Expanded(
            child: Text(
              L10n.getString("event_info_courses_order"),
              textAlign: TextAlign.center,
            ),
          ),
        ),
        DataColumn(
          label: Expanded(
            child: Text(
              L10n.getString("event_info_courses_time_limit"),
              textAlign: TextAlign.center,
            ),
          ),
        ),
        DataColumn(
          label: Expanded(
            child: Text(
              L10n.getString("event_info_courses_pm_penalty"),
              textAlign: TextAlign.center,
            ),
          ),
        ),
        DataColumn(
          label: Expanded(
            child: Text(
              L10n.getString("event_info_courses_overtime_penalty"),
              textAlign: TextAlign.center,
            ),
          ),
        ),
      ],
      rows: [
        for (EventCourse course in eventCourses)
          DataRow(
            cells: <DataCell>[
              DataCell(
                SizedBox(
                  width: ScreenHelper.isDesktop(context) ? 300 : 120,
                  child: TextButton(
                    onPressed: () {
                      GoRouter.of(context).go('${RouteListPageView.routePath}/${course.courseId}');
                    },
                    style: TextButton.styleFrom(
                      alignment: Alignment.centerLeft,
                      foregroundColor: kOrangeColor,
                      padding: const EdgeInsets.all(5),
                    ),
                    child: Text(course.courseName, style: const TextStyle(fontSize: 16)),
                  ),
                ),
              ),
              DataCell(
                Container(
                  alignment: Alignment.center,
                  child: Text(
                    (course.format == 0) ? L10n.getString("event_info_courses_order_preset") : L10n.getString("event_info_courses_order_free"),
                  ),
                ),
              ),
              DataCell(
                Container(
                  alignment: Alignment.center,
                  child: Text(
                    course.maxTime == 0 ? "-" : timeToString(course.maxTime, withHour: true),
                  ),
                ),
              ),
              DataCell(
                Container(
                  alignment: Alignment.center,
                  child: (isPointUnit) ? Text('${course.missingPunchPenalty}') : Text(timeToString(course.missingPunchPenalty * 1000, withHour: true)),
                ),
              ),
              DataCell(
                Container(
                  alignment: Alignment.center,
                  child: (isPointUnit) ? Text('${course.overtimePenalty}') : Text(timeToString(course.overtimePenalty * 1000, withHour: true)),
                ),
              ),
            ],
          )
      ],
    );
  }
}
