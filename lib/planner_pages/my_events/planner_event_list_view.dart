// coverage:ignore-file
import 'package:vikazimut_website/common/event_list/event_list_page.dart';
import 'package:vikazimut_website/constants.dart' as constants;

class PlannerEventListView extends EventListPage {
  static const String routePath = '/planner/events';
  static const String GET_URL = '${constants.API_SERVER}/planner/my-events/%d';

  PlannerEventListView(String? args)
      : super(
          title: "planner_event_list_page_title",
          isAdmin: false,
          url: GET_URL,
          args: args,
        );
}
