import 'package:vikazimut_website/common/lat_lon_box.dart';

class KmlData {
  final LatLonBox georeferencing;
  final String imageFilename;
  final String? courseName;

  KmlData({required this.georeferencing, required this.imageFilename, required this.courseName});

  String toXml() {
    return '<?xml version="1.0" encoding="UTF-8"?>'
        '<kml xmlns="http://www.opengis.net/kml/2.2">'
        '<Folder>'
        '<name></name>'
        '<GroundOverlay>'
        '<name></name>'
        '<Icon><href>files/tile_0_0.jpg</href></Icon>'
        '<LatLonBox>'
        '<north>${georeferencing.north}</north>'
        '<south>${georeferencing.south}</south>'
        '<east>${georeferencing.east}</east>'
        '<west>${georeferencing.west}</west>'
        '<rotation>${georeferencing.rotation}</rotation>'
        '</LatLonBox>'
        '</GroundOverlay>'
        '</Folder>'
        '</kml>';
  }
}
