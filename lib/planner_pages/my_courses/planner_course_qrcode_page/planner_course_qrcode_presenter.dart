// coverage:ignore-file
import 'package:vikazimut_website/public_pages/model/checkpoint.dart';

import 'planner_course_qrcode_data.dart';
import 'qrcode_generator.dart';

class QrCodeGeneratorPresenter {
  static Future<List<int>> downloadCourseQrCode(int courseId, String name) async => await QrCodeGenerator.downloadCourseQrCode(courseId, name);

  static Future<List<int>> downloadArchiveWithCourseQrCodes(int courseId, List<Checkpoint> checkpoints) async => await QrCodeGenerator.downloadArchiveWithCourseQrCodes(courseId, checkpoints);

  static Future<PlannerCourseDataModel?> fetchCourseData({required int courseId}) => PlannerCourseDataModel.fetchCourseData(courseId: courseId);
}
