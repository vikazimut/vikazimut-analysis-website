// coverage:ignore-file
import 'package:vikazimut_website/common/download_stats/download_stats_page.dart';
import 'package:vikazimut_website/constants.dart' as constants;

class PlannerDownloadStatsPage extends DownloadStatsPage {
  static const String routePath = '/planner/download-stats';
  static const URL = '${constants.API_SERVER}/planner/download-stats';
  static int currentDisplayedRowIndex = 0;

  const PlannerDownloadStatsPage() : super(routePath, URL, false);
}
