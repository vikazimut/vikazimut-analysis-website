import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vikazimut_website/constants.dart' as constants;

import 'theme.dart';

class ThemeCubit extends Cubit<ThemeData> {
  ThemeCubit(super.theme);

  Future<void> switchTheme() async {
    String theme;
    if (state == AppTheme.lightTheme) {
      emit(AppTheme.darkTheme);
      theme = "dark";
    } else {
      emit(AppTheme.lightTheme);
      theme = "light";
    }
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString(constants.KEY_THEME, theme);
  }
}
