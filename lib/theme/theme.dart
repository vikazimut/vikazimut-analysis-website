// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

const Color kOrangeColor = Color(0xFFff671f);
const Color kOrangeColorLight = Color(0xFFffe8de);
const Color kOrangeColorUltraLight = Color(0xFFFAF5F0);
const Color kOrangeColorDisabled = Color(0xFFfdae89);
const Color kOrangeColorDarkLight = Color(0xFFfecfab);
const Color kOrangeColorDark = Color(0xFFBB4B16);

const Color kGreenColor = Color(0xFF528053);
const Color kGreenColorLight = Color(0xFF8ac44b);
const Color kDarkColor = Color.fromRGBO(7, 17, 26, 1);

const Color kBorderColor = Color(0xFFD6D6D6);
const Color kCaptionColor = Color.fromRGBO(166, 177, 187, 1);
const Color kSuccessColor = Color(0xFF28a745);
const Color kDangerColor = Color(0xFFdc3545);
const Color kDangerBackgroundColor = Color(0xfff8d7da);
const Color kDangerFontColor = Color(0xffa72c26);
const Color kInfoColor = Color(0xff17a3b9);
const Color kInfoBackgroundColor = Color(0xffadd8e6);
const Color kInfoTextColor = Color(0xff007bff);
const Color kBlueColor = Color(0xff007bff);

const Color kWarningColor = Color(0xFFfec007);
const Color kWarningLightColor = Color(0xFFfff3cd);
const Color kWarningFontColor = Color(0xFF856404);

const Color kVioline = Color(0xFFA626FF);

const Color kBlackGrayColor = Color(0xff272b2e);
const Color kBlackColor = Color(0xff1f2326);
const Color kBlackColorLight = Color(0xff454545);
const Color kTextColorDarkTheme = Color(0xFFBDBDBD);
const Color kTextColorGrayTheme = Color(0xFF999999);
const Color kTextColorLightTheme = Color(0xFF5d6778);

const Color kTextDarkColor = Color(0xFF666666);
const Color kRed = Color(0xffd01514);

const Color kDisabledColor = Color(0xFF757575);

class AppTheme {
  const AppTheme._();

  static final ThemeData lightTheme = _buildLightTheme();

  static final ThemeData darkTheme = _buildDarkTheme();

  static bool isDarkMode(BuildContext context) => Theme.of(context).brightness == Brightness.dark;

  static ThemeData _buildDarkTheme() {
    final ThemeData base = ThemeData(
      brightness: Brightness.dark,
      fontFamily: 'OpenSans',
    );
    return base.copyWith(
      visualDensity: VisualDensity.compact,
      primaryColor: kOrangeColor,
      primaryColorLight: kOrangeColorLight,
      primaryColorDark: kOrangeColorDark,
      scaffoldBackgroundColor: kBlackGrayColor,
      cardColor: Colors.white,
      unselectedWidgetColor: kOrangeColor,
      disabledColor: Colors.grey,
      hintColor: kTextColorGrayTheme,
      colorScheme: base.colorScheme.copyWith(
        primary: kOrangeColor,
        secondary: kGreenColor,
        surface: kBlackColor,
        tertiary: kTextColorDarkTheme,
        outlineVariant: kOrangeColor,
        inversePrimary: kBlackColorLight,
        onPrimary: Colors.white,
      ),
      appBarTheme: const AppBarTheme(
        backgroundColor: kBlackGrayColor,
        foregroundColor: kOrangeColor,
        systemOverlayStyle: SystemUiOverlayStyle.dark,
      ),
      tabBarTheme: TabBarTheme(
        labelColor: kBlackColor,
        labelStyle: const TextStyle(color: Colors.black),
        unselectedLabelStyle: const TextStyle(),
        unselectedLabelColor: kOrangeColor,
        indicator: BoxDecoration(
          color: kOrangeColor,
          borderRadius: BorderRadius.circular(4),
        ),
        indicatorSize: TabBarIndicatorSize.tab,
      ),
      cardTheme: const CardTheme(color: kBlackColor, surfaceTintColor: kBlackGrayColor),
      buttonTheme: base.buttonTheme.copyWith(
        buttonColor: kGreenColor,
        disabledColor: kOrangeColorDisabled,
        textTheme: ButtonTextTheme.primary,
      ),
      textTheme: base.textTheme.copyWith(
        displayLarge: const TextStyle(fontSize: 64, color: kTextColorDarkTheme, fontWeight: FontWeight.w500),
        displayMedium: const TextStyle(fontSize: 2 * 14, color: kOrangeColor, fontWeight: FontWeight.w500),
        displaySmall: const TextStyle(fontSize: 1.75 * 14, color: kTextColorDarkTheme),

        // headlineLarge,
        headlineMedium: const TextStyle(fontSize: 1.5 * 14, color: kTextColorDarkTheme),
        headlineSmall: const TextStyle(fontSize: 1.25 * 14, color: kTextColorDarkTheme),

        titleLarge: base.textTheme.titleLarge!.copyWith(color: kGreenColor),
        titleMedium: const TextStyle(fontSize: 16, color: kTextColorDarkTheme),
        titleSmall: TextStyle(fontSize: 20, color: Colors.grey[600]),

        bodyLarge: const TextStyle(fontSize: 16, color: kTextColorDarkTheme, height: 1.25),
        bodyMedium: const TextStyle(fontSize: 16, color: kTextColorDarkTheme, height: 1.25),
        bodySmall: const TextStyle(fontSize: 14, color: kOrangeColorLight, height: 1.25),

        labelLarge: const TextStyle(fontSize: 17, color: kInfoColor),
        // labelMedium: const TextStyle(fontSize: 17, color: kInfoColor),
        // labelSmall: const TextStyle(fontSize: 17, color: kInfoColor),
      ),
      dataTableTheme: DataTableThemeData(
        dataTextStyle: const TextStyle(color: kTextColorDarkTheme),
        headingRowColor: WidgetStateProperty.all(kOrangeColor),
        headingTextStyle: const TextStyle(fontSize: 12, color: Colors.white),
      ),
      inputDecorationTheme: InputDecorationTheme(
        isDense: true,
        floatingLabelStyle: const TextStyle(color: kOrangeColor),
        focusColor: kOrangeColor,
        labelStyle: TextStyle(color: Colors.grey[600]),
        helperStyle: const TextStyle(color: kTextColorDarkTheme),
        hintStyle: TextStyle(color: Colors.grey[600]),
        border: const OutlineInputBorder(),
        focusedBorder: const OutlineInputBorder(
          borderSide: BorderSide(
            width: 2,
            color: kOrangeColor,
          ),
        ),
      ),
      textButtonTheme: TextButtonThemeData(
        style: TextButton.styleFrom(
          foregroundColor: Colors.black,
        ),
      ),
      elevatedButtonTheme: ElevatedButtonThemeData(
        style: ElevatedButton.styleFrom(foregroundColor: Colors.white),
      ),
      outlinedButtonTheme: OutlinedButtonThemeData(
        style: OutlinedButton.styleFrom(
          foregroundColor: kOrangeColor,
          textStyle: const TextStyle(fontSize: 16),
          padding: const EdgeInsets.all(15),
          side: const BorderSide(color: kOrangeColor, width: 1),
        ),
      ),
      switchTheme: SwitchThemeData(
        // activeColor: kOrangeColor,
        // activeTrackColor: Colors.black,
        // inactiveTrackColor: Colors.black,
        thumbColor: WidgetStateProperty.resolveWith<Color?>((Set<WidgetState> states) {
          if (states.contains(WidgetState.disabled)) {
            return Colors.black;
          }
          if (states.contains(WidgetState.selected)) {
            return kOrangeColor;
          }
          return null;
        }),
        trackColor: WidgetStateProperty.resolveWith<Color?>((Set<WidgetState> states) {
          if (states.contains(WidgetState.disabled)) {
            return Colors.black;
          }
          if (states.contains(WidgetState.selected)) {
            return kOrangeColorDarkLight;
          }
          return null;
        }),
      ),
      radioTheme: RadioThemeData(
        fillColor: WidgetStateProperty.resolveWith<Color?>((Set<WidgetState> states) {
          if (states.contains(WidgetState.disabled)) {
            return null;
          }
          if (states.contains(WidgetState.selected)) {
            return kOrangeColor;
          }
          return null;
        }),
      ),
      checkboxTheme: CheckboxThemeData(
        fillColor: WidgetStateProperty.resolveWith<Color?>((Set<WidgetState> states) {
          if (states.contains(WidgetState.disabled)) {
            return null;
          }
          if (states.contains(WidgetState.selected)) {
            return kOrangeColor;
          }
          return null;
        }),
      ),
      progressIndicatorTheme: const ProgressIndicatorThemeData(
        color: kGreenColor,
        circularTrackColor: kOrangeColor,
        linearTrackColor: kOrangeColorUltraLight,
      ),
      dialogTheme: DialogThemeData(
        backgroundColor: kBlackGrayColor,
      ),
    );
  }

  static ThemeData _buildLightTheme() {
    final ThemeData base = ThemeData(
      brightness: Brightness.light,
      fontFamily: 'OpenSans',
    );

    return base.copyWith(
      visualDensity: VisualDensity.compact,
      primaryColor: kOrangeColor,
      primaryColorLight: kOrangeColorLight,
      primaryColorDark: kOrangeColorDark,
      scaffoldBackgroundColor: Colors.white,
      cardColor: Colors.white,
      unselectedWidgetColor: kOrangeColor,
      disabledColor: Colors.grey,
      hintColor: kTextColorLightTheme,
      cardTheme: const CardTheme(color: Colors.white, surfaceTintColor: Colors.white),
      colorScheme: base.colorScheme.copyWith(
        primary: kOrangeColor,
        secondary: kGreenColor,
        surface: kOrangeColorUltraLight,
        tertiary: Colors.black,
        surfaceTint: kOrangeColor,
        outlineVariant: kOrangeColorDisabled,
        inversePrimary: kOrangeColorLight,
        onPrimary: Colors.black,
      ),
      appBarTheme: const AppBarTheme(
        foregroundColor: Colors.white,
        backgroundColor: kOrangeColor,
        systemOverlayStyle: SystemUiOverlayStyle.light,
      ),
      tabBarTheme: TabBarTheme(
        labelColor: Colors.white,
        labelStyle: const TextStyle(color: Colors.white),
        unselectedLabelColor: kOrangeColorDisabled,
        unselectedLabelStyle: const TextStyle(),
        indicator: BoxDecoration(
          color: kOrangeColor,
          borderRadius: BorderRadius.circular(4),
        ),
        indicatorSize: TabBarIndicatorSize.tab,
      ),
      dataTableTheme: DataTableThemeData(
        dataTextStyle: const TextStyle(color: Colors.black),
        headingRowColor: WidgetStateProperty.all(kOrangeColor),
        headingTextStyle: const TextStyle(fontSize: 12, color: Colors.white),
      ),
      buttonTheme: base.buttonTheme.copyWith(
        buttonColor: kGreenColor,
        disabledColor: kOrangeColorDisabled,
        textTheme: ButtonTextTheme.primary,
      ),
      textTheme: base.textTheme.copyWith(
        displayLarge: const TextStyle(fontSize: 64, color: Colors.black, fontWeight: FontWeight.w500),
        displayMedium: const TextStyle(fontSize: 2 * 14, color: kOrangeColor, fontWeight: FontWeight.w500),
        displaySmall: const TextStyle(fontSize: 1.75 * 14, color: Colors.black),

        // headlineLarge,
        headlineMedium: const TextStyle(fontSize: 1.5 * 14, color: Colors.black),
        headlineSmall: const TextStyle(fontSize: 1.25 * 14, color: Colors.black),

        titleLarge: base.textTheme.titleLarge!.copyWith(color: kGreenColor),
        titleMedium: const TextStyle(fontSize: 16, color: Colors.black),
        titleSmall: const TextStyle(fontSize: 14, color: Colors.black),

        bodyLarge: const TextStyle(fontSize: 16, color: Colors.black, height: 1.25),
        bodyMedium: const TextStyle(fontSize: 16, color: Colors.black, height: 1.25),
        bodySmall: const TextStyle(fontSize: 14, color: Colors.black, height: 1.25),

        labelLarge: const TextStyle(fontSize: 16, color: kTextColorDarkTheme),
        labelMedium: const TextStyle(fontSize: 14, color: kTextColorDarkTheme),
        labelSmall: const TextStyle(fontSize: 12, color: kTextColorDarkTheme),

        // overline,
      ),
      textButtonTheme: TextButtonThemeData(
        style: TextButton.styleFrom(
          foregroundColor: Colors.black,
        ),
      ),
      elevatedButtonTheme: ElevatedButtonThemeData(
        style: ElevatedButton.styleFrom(foregroundColor: Colors.white),
      ),
      inputDecorationTheme: InputDecorationTheme(
        isDense: true,
        floatingLabelStyle: const TextStyle(color: kOrangeColor),
        focusColor: kOrangeColor,
        labelStyle: TextStyle(color: Colors.grey[600]),
        helperStyle: const TextStyle(color: kTextColorDarkTheme),
        hintStyle: TextStyle(color: Colors.grey[600]),
        border: const OutlineInputBorder(),
        focusedBorder: const OutlineInputBorder(
          borderSide: BorderSide(
            width: 2,
            color: kOrangeColor,
          ),
        ),
      ),
      outlinedButtonTheme: OutlinedButtonThemeData(
        style: OutlinedButton.styleFrom(
          foregroundColor: kOrangeColor,
          textStyle: const TextStyle(fontSize: 16),
          padding: const EdgeInsets.all(15),
          side: const BorderSide(color: kOrangeColor, width: 1),
        ),
      ),
      switchTheme: SwitchThemeData(
        thumbColor: WidgetStateProperty.resolveWith<Color?>((Set<WidgetState> states) {
          if (states.contains(WidgetState.disabled)) {
            return null;
          }
          if (states.contains(WidgetState.selected)) {
            return kOrangeColor;
          }
          return null;
        }),
        trackColor: WidgetStateProperty.resolveWith<Color?>((Set<WidgetState> states) {
          if (states.contains(WidgetState.disabled)) {
            return null;
          }
          if (states.contains(WidgetState.selected)) {
            return kOrangeColorDisabled;
          }
          return null;
        }),
      ),
      radioTheme: RadioThemeData(
        fillColor: WidgetStateProperty.resolveWith<Color?>((Set<WidgetState> states) {
          if (states.contains(WidgetState.disabled)) {
            return null;
          }
          if (states.contains(WidgetState.selected)) {
            return kGreenColor;
          }
          return null;
        }),
      ),
      checkboxTheme: CheckboxThemeData(
        fillColor: WidgetStateProperty.resolveWith<Color?>((Set<WidgetState> states) {
          if (states.contains(WidgetState.disabled)) {
            return null;
          }
          if (states.contains(WidgetState.selected)) {
            return kOrangeColor;
          }
          return null;
        }),
      ),
      progressIndicatorTheme: const ProgressIndicatorThemeData(
        color: kGreenColor,
        circularTrackColor: kOrangeColor,
        linearTrackColor: kOrangeColorUltraLight,
      ),
      dialogTheme: DialogThemeData(
        backgroundColor: Colors.white,
      ),
    );
  }
}
