// coverage:ignore-file
import 'package:flutter/material.dart';

// Workaround to associate appBar with action and endDrawer
class CustomScaffold extends Scaffold {
  CustomScaffold({
    required AppBar appBar,
    required Widget super.body,
    required GlobalKey<ScaffoldState> super.key,
    super.endDrawer,
  }) : super(
          appBar: endDrawer != null && appBar.actions != null && appBar.actions!.isNotEmpty ? _buildEndDrawerButton(appBar, key) : appBar,
        );

  static AppBar _buildEndDrawerButton(AppBar myAppBar, GlobalKey<ScaffoldState> keyScaffold) {
    myAppBar.actions?.add(
      IconButton(
        icon: const Icon(Icons.menu),
        onPressed: () => !keyScaffold.currentState!.isEndDrawerOpen ? keyScaffold.currentState!.openEndDrawer() : null,
      ),
    );
    return myAppBar;
  }
}
