import 'package:flutter/material.dart';

@immutable
class HeaderItem {
  final String? title;
  final void Function(BuildContext)? onTap;
  final void Function(BuildContext)? onLongTap;
  final IconData? icon;

  const HeaderItem._()
      : title = null,
        onTap = null,
        onLongTap = null,
        icon = null;

  const HeaderItem({
    required this.title,
    required this.onTap,
    required this.icon,
    this.onLongTap,
  });

  factory HeaderItem.empty() => const HeaderItem._();

  bool get isEmpty => title == null;
}
