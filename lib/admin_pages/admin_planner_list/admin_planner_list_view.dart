// coverage:ignore-file
library;

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:vikazimut_website/common/footer.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/menu/responsive_scaffold_widget.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/container_decoration.dart';
import 'package:vikazimut_website/utils/custom_button.dart';
import 'package:vikazimut_website/utils/custom_dropdown_button.dart';
import 'package:vikazimut_website/utils/error_container.dart';
import 'package:vikazimut_website/utils/global_error_widget.dart';
import 'package:vikazimut_website/utils/screen_helper.dart';
import 'package:vikazimut_website/utils/utils.dart';

import 'admin_planner_data.dart';
import 'admin_planner_data_table.dart';
import 'admin_planner_list_presenter.dart';

part 'admin_add_planner_form.dart';

class AdminPlannerListView extends StatefulWidget {
  static const String routePath = '/admin/planners';

  const AdminPlannerListView();

  @override
  State<AdminPlannerListView> createState() => AdminPlannerListViewState();
}

class AdminPlannerListViewState extends State<AdminPlannerListView> {
  late final AdminPlannerListPagePresenter _presenter;
  late Future<List<AdminPlannerData>> _planners;
  final ScrollController _verticalScrollController = ScrollController();
  final GlobalKey<PaginatedDataTableState> _tableKey = GlobalKey<PaginatedDataTableState>();
  final ValueNotifier<int> _plannerCount = ValueNotifier<int>(-1);
  String? _errorMessage;

  @override
  void initState() {
    _presenter = AdminPlannerListPagePresenter(this);
    _planners = _presenter.fetchPlanners();
    _planners.then<void>((List<AdminPlannerData> list) => _plannerCount.value = list.length).catchError((_) {});
    super.initState();
  }

  @override
  void dispose() {
    _verticalScrollController.dispose();
    _plannerCount.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ResponsiveScaffoldWidget(
      body: Padding(
        padding: ScreenHelper.isDesktop(context) ? const EdgeInsets.all(0) : const EdgeInsets.all(8),
        child: Column(
          children: [
            Flexible(
              child: ContainerDecoration(
                constraints: const BoxConstraints(maxWidth: ScreenHelper.desktopMinWidth),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Align(
                      alignment: Alignment.topCenter,
                      child: ValueListenableBuilder<int>(
                        valueListenable: _plannerCount,
                        builder: (context, value, widget) {
                          return Text(
                            L10n.getString("admin_planner_list_page_title") + (value < 0 ? "" : " ($value)"),
                            style: Theme.of(context).textTheme.displayMedium,
                          );
                        },
                      ),
                    ),
                    Expanded(
                      child: FutureBuilder<List<AdminPlannerData>>(
                        future: _planners,
                        builder: (context, AsyncSnapshot<List<AdminPlannerData>> snapshot) {
                          if (snapshot.hasError) {
                            return GlobalErrorWidget(snapshot.error.toString());
                          } else if (!snapshot.hasData) {
                            return const Center(child: CircularProgressIndicator());
                          } else {
                            var planners = snapshot.data!.reversed.toList();
                            return Scrollbar(
                              controller: _verticalScrollController,
                              thumbVisibility: true,
                              interactive: true,
                              child: SingleChildScrollView(
                                controller: _verticalScrollController,
                                scrollDirection: Axis.vertical,
                                child: SizedBox(
                                  width: ScreenHelper.tabletMaxWidth - 20,
                                  child: Theme(
                                    data: Theme.of(context).copyWith(
                                      textTheme: const TextTheme(bodySmall: TextStyle(color: kOrangeColor)),
                                      cardTheme: CardTheme(color: Theme.of(context).scaffoldBackgroundColor),
                                      iconTheme: const IconThemeData(color: Colors.white),
                                    ),
                                    child: AdminPlannerDataTable(_tableKey, _presenter, planners),
                                  ),
                                ),
                              ),
                            );
                          }
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
            const SizedBox(height: 5),
            CustomPlainButton(
                label: L10n.getString("admin_planner_list_page_button_add_planner"),
                backgroundColor: kOrangeColor,
                onPressed: () async {
                  AdminPlannerData? data = await _AdminAddPlannerFormDialog().showAndWait(context);
                  if (data != null) {
                    _presenter.addPlanner(data);
                  }
                }),
            if (_errorMessage != null) ErrorContainer(_errorMessage!),
            Footer(),
          ],
        ),
      ),
    );
  }

  void update() {
    setState(() {
      _errorMessage = null;
      _planners = _presenter.fetchPlanners();
      _planners.then<void>((List<AdminPlannerData> list) => _plannerCount.value = list.length).catchError((_) {});
    });
  }

  void error(String message) {
    setState(() {
      _errorMessage = message;
    });
  }

  void displayToast(String message) {
    ScaffoldMessenger.of(context)
      ..hideCurrentSnackBar()
      ..showSnackBar(
        SnackBar(content: Text(message)),
      );
  }
}
