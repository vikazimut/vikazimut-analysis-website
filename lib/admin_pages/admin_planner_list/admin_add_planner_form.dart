part of 'admin_planner_list_view.dart';

class _AdminAddPlannerFormDialog {
  final AdminPlannerData _plannerData = AdminPlannerData()..languageCode = "fr";
  final ValueNotifier<bool> _validated = ValueNotifier(false);

  Widget _buildPopupAddCourse(BuildContext context) {
    return AlertDialog(
      title: Text(
        L10n.getString("admin_planner_add_dialog_title"),
        style: const TextStyle(color: kOrangeColor),
      ),
      content: Container(
        constraints: const BoxConstraints(minWidth: 300),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            TextField(
              autofocus: true,
              decoration: InputDecoration(labelText: L10n.getString("admin_planner_add_dialog_username")),
              keyboardType: TextInputType.name,
              inputFormatters: [
                FilteringTextInputFormatter.allow(userIdentifierRegExp),
                LengthLimitingTextInputFormatter(userIdentifierLength),
              ],
              onChanged: (String value) {
                _plannerData.username = value;
                _validated.value = checkCompleteness();
              },
            ),
            const SizedBox(height: 15),
            TextField(
              decoration: InputDecoration(labelText: L10n.getString("admin_planner_add_dialog_lastname")),
              keyboardType: TextInputType.name,
              inputFormatters: [
                FilteringTextInputFormatter.allow(userIdentifierRegExp),
                LengthLimitingTextInputFormatter(userIdentifierLength),
              ],
              onChanged: (String value) {
                _plannerData.lastName = value;
                _validated.value = checkCompleteness();
              },
            ),
            const SizedBox(height: 15),
            TextField(
              decoration: InputDecoration(labelText: L10n.getString("admin_planner_add_dialog_firstname")),
              keyboardType: TextInputType.name,
              inputFormatters: [
                FilteringTextInputFormatter.allow(userIdentifierRegExp),
                LengthLimitingTextInputFormatter(userIdentifierLength),
              ],
              onChanged: (String value) {
                _plannerData.firstName = value;
                _validated.value = checkCompleteness();
              },
            ),
            const SizedBox(height: 15),
            TextField(
              keyboardType: TextInputType.emailAddress,
              inputFormatters: [
                LengthLimitingTextInputFormatter(60),
              ],
              decoration: InputDecoration(labelText: L10n.getString("admin_planner_add_dialog_email")),
              onChanged: (String value) {
                _plannerData.email = value;
                _validated.value = checkCompleteness();
              },
            ),
            const SizedBox(height: 15),
            TextField(
              keyboardType: TextInputType.number,
              decoration: InputDecoration(labelText: L10n.getString("admin_planner_add_dialog_phone")),
              inputFormatters: [
                FilteringTextInputFormatter.allow(phoneRegExp),
                LengthLimitingTextInputFormatter(20),
              ],
              onChanged: (String value) {
                _plannerData.phone = value;
                _validated.value = checkCompleteness();
              },
            ),
            const SizedBox(height: 15),
            StatefulBuilder(builder: (BuildContext context, StateSetter dropDownState) {
              return CustomDropdownButton<String>(
                value: _plannerData.languageCode,
                hintMessage: Text(
                  L10n.getString("admin_planner_add_dialog_language"),
                  style: TextStyle(color: Colors.grey[600]),
                ),
                onChanged: (String? newValue) {
                  dropDownState(() {
                    _plannerData.languageCode = newValue!;
                    _validated.value = checkCompleteness();
                  });
                },
                items: L10n.locales.entries
                    .map<DropdownMenuItem<String>>(
                      (MapEntry<String, Localization> item) => DropdownMenuItem<String>(
                        value: item.value.code,
                        child: Text(
                          item.value.language,
                          style: const TextStyle(color: kOrangeColor),
                        ),
                      ),
                    )
                    .toList(),
              );
            }),
          ],
        ),
      ),
      actions: <Widget>[
        AnimatedBuilder(
          animation: _validated,
          builder: (_, __) => ElevatedButton(
            onPressed: _validated.value ? () => Navigator.of(context).pop(_plannerData) : null,
            style: ElevatedButton.styleFrom(
              backgroundColor: kOrangeColor,
              padding: const EdgeInsets.all(15),
            ),
            child: Text(L10n.getString("admin_planner_add_dialog_button_ok")),
          ),
        ),
        CustomOutlinedButton(
          backgroundColor: kOrangeColor,
          foregroundColor: kOrangeColor,
          onPressed: () => Navigator.of(context).pop(null),
          label: L10n.getString("admin_planner_add_dialog_button_cancel"),
        ),
      ],
    );
  }

  bool checkCompleteness() {
    return _plannerData.username.trim().isNotEmpty && _plannerData.email.trim().isNotEmpty && _plannerData.languageCode != null;
  }

  Future<AdminPlannerData?> showAndWait(BuildContext context) async {
    return await showDialog<AdminPlannerData?>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) => _buildPopupAddCourse(context),
    );
  }
}
