import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/planner_pages/my_courses/planner_course_list_page.dart';
import 'package:vikazimut_website/planner_pages/my_events/planner_event_list_view.dart';
import 'package:vikazimut_website/planner_pages/profile/planner_profile_page.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/alert_dialog.dart';
import 'package:vikazimut_website/utils/custom_button.dart';
import 'package:vikazimut_website/utils/diacritic.dart';
import 'package:vikazimut_website/utils/time.dart';

import 'admin_planner_data.dart';
import 'admin_planner_list_presenter.dart';

class AdminPlannerDataTable extends StatefulWidget {
  final List<AdminPlannerData> items;
  final AdminPlannerListPagePresenter presenter;
  final GlobalKey<PaginatedDataTableState> tableKey;

  const AdminPlannerDataTable(this.tableKey, this.presenter, this.items);

  @override
  State<AdminPlannerDataTable> createState() => AdminPlannerDataTableState();
}

class AdminPlannerDataTableState extends State<AdminPlannerDataTable> {
  final TextEditingController _searchFieldController = TextEditingController();
  _PlannerDataSource? _plannerDataSource;
  late List<AdminPlannerData> _initialPlannerList;
  static String _currentSearchUsername = "";
  static int? _sortColumnIndex;
  static bool _sortAscending = false;

  @override
  void initState() {
    _initialPlannerList = widget.items.toList(growable: false);
    _searchFieldController.text = _currentSearchUsername;
    if (_sortColumnIndex != null) _sort(_sortColumnIndex!, _sortAscending);
    _plannerDataSource = _PlannerDataSource(widget.presenter, _initialPlannerList, _currentSearchUsername, context: context);
    super.initState();
  }

  @override
  void didUpdateWidget(covariant AdminPlannerDataTable oldWidget) {
    _initialPlannerList = widget.items.toList(growable: false);
    _plannerDataSource = _PlannerDataSource(widget.presenter, _initialPlannerList, _currentSearchUsername, context: context);
    super.didUpdateWidget(oldWidget);
  }

  @override
  void dispose() {
    _searchFieldController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final int rowPerPage = (height - 360) ~/ 45;
    return PaginatedDataTable(
      key: widget.tableKey,
      initialFirstRowIndex: AdminPlannerListPagePresenter.currentDisplayedRowIndex,
      header: Padding(
        padding: const EdgeInsets.all(3),
        child: TextField(
          controller: _searchFieldController,
          style: TextStyle(color: Theme.of(context).colorScheme.tertiary),
          decoration: InputDecoration(
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(25.0),
              borderSide: const BorderSide(
                width: 2,
                color: kOrangeColor,
              ),
            ),
            labelText: L10n.getString("admin_planner_list_page_search_hint_text"),
            border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
          ),
          onChanged: (searchText) {
            setState(() {
              _currentSearchUsername = searchText;
              widget.tableKey.currentState!.pageTo(0);
              _plannerDataSource = _PlannerDataSource(widget.presenter, _initialPlannerList, searchText, context: context);
            });
          },
        ),
      ),
      dataRowMaxHeight: 45,
      dataRowMinHeight: 45,
      columnSpacing: 1.0,
      horizontalMargin: 0,
      rowsPerPage: rowPerPage,
      showFirstLastButtons: true,
      showEmptyRows: false,
      sortColumnIndex: _sortColumnIndex,
      sortAscending: _sortAscending,
      showCheckboxColumn: false,
      arrowHeadColor: kOrangeColor,
      onPageChanged: (int value) => AdminPlannerListPagePresenter.currentDisplayedRowIndex = value,
      columns: [
        DataColumn(
          label: Expanded(
            child: Container(
              alignment: Alignment.center,
              margin: EdgeInsets.zero,
              padding: EdgeInsets.zero,
              child: Text(
                L10n.getString("admin_planner_list_page_table_column1"),
                style: const TextStyle(fontSize: 16, color: Colors.white),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          onSort: (int columnIndex, bool ascending) {
            _sortAndUpdate(columnIndex, ascending);
          },
        ),
        DataColumn(
          label: Expanded(
            child: Container(
              alignment: Alignment.center,
              child: Text(
                L10n.getString("admin_planner_list_page_table_column2"),
                style: const TextStyle(fontSize: 16, color: Colors.white),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          onSort: (int columnIndex, bool ascending) {
            _sortAndUpdate(columnIndex, ascending);
          },
        ),
        DataColumn(
          label: Expanded(
            child: Container(
              alignment: Alignment.center,
              child: Text(
                L10n.getString("admin_planner_list_page_table_column3"),
                style: const TextStyle(fontSize: 16, color: Colors.white),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          numeric: true,
          onSort: (int columnIndex, bool ascending) {
            _sortAndUpdate(columnIndex, ascending);
          },
        ),
        DataColumn(
          label: Expanded(
            child: Container(
              alignment: Alignment.center,
              child: Text(
                L10n.getString("admin_planner_list_page_table_column4"),
                style: const TextStyle(fontSize: 16, color: Colors.white),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          numeric: true,
          onSort: (int columnIndex, bool ascending) {
            _sortAndUpdate(columnIndex, ascending);
          },
        ),
        const DataColumn(
          label: SizedBox.shrink(),
        ),
      ],
      source: _plannerDataSource!,
    );
  }

  void _sortAndUpdate(int columnIndex, bool ascending) {
    _sort(columnIndex, ascending);
    setState(() {
      widget.tableKey.currentState!.pageTo(0);
      _plannerDataSource = _PlannerDataSource(widget.presenter, _initialPlannerList, _currentSearchUsername, context: context);
    });
  }

  void _sort(int columnIndex, bool ascending) {
    if (_sortColumnIndex != columnIndex) {
      _sortColumnIndex = columnIndex;
    } else {
      _sortAscending = ascending;
    }
    _initialPlannerList.sort((AdminPlannerData a, AdminPlannerData b) {
      if (_sortColumnIndex == 0) {
        return AdminPlannerData.sortOnUsername(_sortAscending, a, b);
      } else if (_sortColumnIndex == 2) {
        return AdminPlannerData.sortOnModificationDate(_sortAscending, a, b);
      } else if (_sortColumnIndex == 3) {
        return AdminPlannerData.sortOnCreationDate(_sortAscending, a, b);
      } else {
        return AdminPlannerData.sortOnCourseCount(_sortAscending, a, b);
      }
    });
  }
}

class _PlannerDataSource extends DataTableSource {
  late final List<AdminPlannerData> _plannerList;
  final BuildContext context;
  final AdminPlannerListPagePresenter presenter;

  _PlannerDataSource(this.presenter, List<AdminPlannerData> plannerList, String searchText, {required this.context}) {
    if (searchText.isNotEmpty) {
      var rawSearchText = searchText.toLowerCase().toRawString();
      _plannerList = plannerList.where((element) => element.username.toLowerCase().toRawString().contains(rawSearchText)).toList();
    } else {
      _plannerList = plannerList;
    }
  }

  @override
  bool get isRowCountApproximate => false;

  @override
  int get rowCount => _plannerList.length;

  @override
  int get selectedRowCount => 0;

  @override
  DataRow? getRow(int index) {
    assert(index >= 0 && index < _plannerList.length);
    final AdminPlannerData planner = _plannerList[index];
    return DataRow(color: index.isEven ? WidgetStateProperty.all(Theme.of(context).colorScheme.inversePrimary) : WidgetStateProperty.all(Theme.of(context).colorScheme.surface), cells: [
      DataCell(
        Container(
            padding: const EdgeInsets.only(left: 8),
            width: 250,
            child: Row(
              children: [
                const Icon(
                  Icons.person,
                  color: kOrangeColor,
                ),
                Text(planner.username),
              ],
            )),
      ),
      DataCell(
        Align(alignment: Alignment.center, child: Text('${planner.courseCount}')),
      ),
      DataCell(
        Align(alignment: Alignment.center, child: planner.modifiedDateInMillisecond < 0 ? const Text("-") : Text(getAbsoluteLocalizedDate(planner.modifiedDateInMillisecond))),
      ),
      DataCell(
        Align(alignment: Alignment.center, child: planner.createdDateInMillisecond < 0 ? const Text("-") : Text(getAbsoluteLocalizedDate(planner.createdDateInMillisecond))),
      ),
      DataCell(
        Row(
          children: [
            CustomPlainButton(
              label: L10n.getString("admin_planner_list_page_button_courses"),
              backgroundColor: kSuccessColor,
              onPressed: () => GoRouter.of(context).go("${PlannerCourseListPage.routePath}/${planner.id}"),
            ),
            const SizedBox(width: 2),
            CustomPlainButton(
              label: L10n.getString("admin_planner_list_page_button_events"),
              backgroundColor: kVioline,
              onPressed: () => GoRouter.of(context).go("${PlannerEventListView.routePath}/${planner.id}"),
            ),
            const SizedBox(width: 2),
            CustomPlainButton(
              label: L10n.getString("admin_planner_list_page_button_info"),
              backgroundColor: kWarningColor,
              foregroundColor: Colors.black,
              onPressed: () => GoRouter.of(context).go("${PlannerProfilePage.routePath}/${planner.id}"),
            ),
            const SizedBox(width: 2),
            CustomPlainButton(
              label: L10n.getString("admin_planner_list_page_button_change_password"),
              backgroundColor: kInfoColor,
              onPressed: () {
                showBinaryQuestionAlertDialog(
                  context: context,
                  title: L10n.getString("admin_planner_list_page_change_password_title"),
                  message: L10n.getString("admin_planner_list_page_change_password_message", [planner.username]),
                  okMessage: L10n.getString("admin_planner_list_page_change_password_ok_message"),
                  noMessage: L10n.getString("admin_planner_list_page_change_password_no_message"),
                  action: () {
                    presenter.changePlannerPassword(planner.id, planner.email);
                  },
                );
              },
            ),
            const SizedBox(width: 2),
            CustomPlainButton(
              label: L10n.getString("admin_planner_list_page_button_delete"),
              backgroundColor: kDangerColor,
              onPressed: () => {
                showBinaryQuestionAlertDialog(
                  context: context,
                  title: L10n.getString("admin_planner_list_page_delete_title"),
                  message: L10n.getString("admin_planner_list_page_delete_message", [planner.username]),
                  okMessage: L10n.getString("admin_planner_list_page_delete_ok_message"),
                  noMessage: L10n.getString("admin_planner_list_page_delete_no_message"),
                  action: () => presenter.deletePlanner(planner.id, planner.username),
                ),
              },
            ),
          ],
        ),
      ),
    ]);
  }
}
