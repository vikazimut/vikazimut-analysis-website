import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:vikazimut_website/constants.dart' as constants;
import 'package:vikazimut_website/l10n/l10n.dart';

import 'admin_planner_list_gateway.dart';
import 'admin_planner_data.dart';
import 'admin_planner_list_view.dart';

class AdminPlannerListPagePresenter {
  static const String _URL = '${constants.API_SERVER}/admin/planners';
  static const String _POST_ADD_URL = '${constants.API_SERVER}/admin/add-planner';
  static const String _URL_DELETE = "${constants.API_SERVER}/admin/delete-planner";
  static const String _URL_RESET_PASSWORD = "${constants.API_SERVER}/admin/change-planner-password";

  final AdminPlannerListViewState view;
  static int currentDisplayedRowIndex = 0;

  AdminPlannerListPagePresenter(this.view);

  Future<List<AdminPlannerData>> fetchPlanners() async {
    return AdminPlannerListGateway.fetchPlanners(_URL);
  }

  Future<void> deletePlanner(int plannerId, String plannerName) async {
    http.Response response = await AdminPlannerListGateway.deletePlanner("$_URL_DELETE/$plannerId");
    if (response.statusCode == HttpStatus.ok) {
      view.update();
      view.displayToast(L10n.getString("admin_planner_delete_success_message", [plannerName]));
    } else {
      view.error(response.body);
    }
  }

  Future<void> addPlanner(AdminPlannerData data) async {
    http.Response response = await AdminPlannerListGateway.addPlanner(_POST_ADD_URL, data);
    if (response.statusCode == HttpStatus.ok) {
      view.update();
      view.displayToast(L10n.getString("admin_planner_add_success_message", [data.username]));
    } else {
      view.error(response.body);
    }
  }

  void changePlannerPassword(int id, String email) async {
    http.Response response = await AdminPlannerListGateway.resetPassword(_URL_RESET_PASSWORD, id);
    if (response.statusCode == HttpStatus.ok) {
      view.update();
      view.displayToast(L10n.getString("admin_planner_change_password_success_message", [email]));
    } else {
      view.error(response.body);
    }
  }
}
