// coverage:ignore-file
import 'package:vikazimut_website/common/event_list/event_list_page.dart';
import 'package:vikazimut_website/constants.dart' as constants;

class AdminEventListPage extends EventListPage {
  static const String routePath = '/admin/events';
  static const String GET_URL = '${constants.API_SERVER}/admin/events';

  AdminEventListPage(String? args)
      : super(
          title: "admin_event_list_page_title",
          isAdmin: true,
          url: GET_URL,
          args: args,
        );
}
