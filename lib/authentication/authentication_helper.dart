import 'dart:convert';
import 'dart:io';

import 'package:corsac_jwt/corsac_jwt.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/keys.dart';
import 'package:vikazimut_website/common/connection_exception.dart';
import 'package:vikazimut_website/authentication/identifier_user.dart';

class AuthenticationHelper {
  static Future<IdentifiedUser?> postUserAuthenticationRequest(String url, {String? body, String? token}) async {
    Map<String, String> headers;
    if (token == null) {
      headers = <String, String>{"Content-Type": "application/json; charset=UTF-8"};
    } else {
      headers = {HttpHeaders.authorizationHeader: token};
    }
    http.Response response;
    try {
      response = await http.post(
        Uri.parse(url),
        headers: headers,
        body: body,
      );
    } catch (e) {
      throw ConnectionException("${L10n.getString("Server connection error")}\n($e)");
    }

    if (response.statusCode == HttpStatus.ok) {
      var accessToken = extractAccessTokenFromResponse(response.body);
      if (_verify(accessToken)) {
        var identifierUser = IdentifiedUser.fromTokens(response.body);
        await _storeTokens(response.body);
        return identifierUser;
      } else {
        throw ConnectionException(L10n.getString("login_server_bad_request"));
      }
    } else if (response.statusCode == HttpStatus.badRequest) {
      throw ConnectionException(L10n.getString("login_server_bad_request"));
    } else {
      var errorMessage = jsonDecode(response.body)["message"];
      if (errorMessage == "Too many failed login attempts, please try again in 1 minute.") {
        throw ConnectionException(L10n.getString("login_page_throttling_error"));
      } else {
        throw ConnectionException(L10n.getString("login_page_invalid_credentials"));
      }
    }
  }

  static Future<IdentifiedUser?> postUserRefreshAuthenticationRequest(String url, String refreshToken) async {
    try {
      final http.Response response = await http.post(
        Uri.parse(url),
        body: {"refresh_token": refreshToken},
      );
      if (response.statusCode == HttpStatus.ok) {
        var accessToken = extractAccessTokenFromResponse(response.body);
        if (_verify(accessToken)) {
          var identifierUser = IdentifiedUser.fromTokens(response.body);
          await _storeTokens(response.body);
          return identifierUser;
        } else {
          throw ConnectionException(L10n.getString("login_server_bad_request"));
        }
      } else if (response.statusCode == HttpStatus.badRequest) {
        throw ConnectionException(L10n.getString("login_server_bad_request"));
      } else {
        return null;
      }
    } catch (_) {
      throw ConnectionException(L10n.getString("Server connection error"));
    }
  }

  static String extractAccessTokenFromResponse(String response) {
    var jsonWebToken = jsonDecode(response);
    return jsonWebToken["token"];
  }

  static String extractRefreshTokenFromResponse(String response) {
    var jsonWebToken = jsonDecode(response);
    return jsonWebToken["refresh_token"];
  }

  static Future<void> _storeTokens(String tokens) async {
    await const FlutterSecureStorage().write(key: "jwt", value: tokens);
  }

  static void logout() {
    const FlutterSecureStorage().delete(key: "jwt");
  }

  static Future<String?>? getStoredTokens() async {
    return await const FlutterSecureStorage().read(key: "jwt");
  }

  static bool _verify(token) {
    final decodedToken = JWT.parse(token);
    var signer = JWTRsaSha256Signer(publicKey: PUBLIC_KEY);
    return decodedToken.verify(signer);
  }
}
