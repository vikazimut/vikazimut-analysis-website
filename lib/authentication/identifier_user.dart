import 'package:corsac_jwt/corsac_jwt.dart';
import 'package:flutter/material.dart';
import 'package:vikazimut_website/authentication/authentication_helper.dart';
import 'package:vikazimut_website/constants.dart' as constants;

class IdentifiedUser {
  final String tokens;
  final String username;
  final bool admin;

  IdentifiedUser({required this.username, required this.tokens, required this.admin});

  String get accessToken => AuthenticationHelper.extractAccessTokenFromResponse(tokens);

  factory IdentifiedUser.fromTokens(String tokens) {
    final accessToken = AuthenticationHelper.extractAccessTokenFromResponse(tokens);
    final decodedToken = JWT.parse(accessToken);
    final data = decodedToken.claims;
    return IdentifiedUser(
      username: data['username'],
      admin: isAdmin(data['roles']),
      tokens: tokens,
    );
  }

  static Future<IdentifiedUser?> getUser() async {
    var tokens = await AuthenticationHelper.getStoredTokens();
    if (tokens == null) {
      return null;
    }
    try {
      IdentifiedUser identifierUser = IdentifiedUser.fromTokens(tokens);
      if (await identifierUser._isNotAuthorized()) {
        AuthenticationHelper.logout();
        return null;
      }
      return identifierUser;
    } catch (_) {
      AuthenticationHelper.logout();
      return null;
    }
  }

  static Future<bool> isAuthenticated() async {
    IdentifiedUser? user = await getUser();
    if (user == null || await user._isNotAuthorized()) {
      return false;
    }
    return true;
  }

  static Future<bool> isAuthenticatedAsAdmin() async {
    IdentifiedUser? user = await getUser();
    if (user == null || await user._isNotAuthorized()) {
      return false;
    }
    return user.admin;
  }

  Future<bool> _isNotAuthorized() async {
    var accessToken = AuthenticationHelper.extractAccessTokenFromResponse(tokens);
    if (_isExpired(accessToken)) {
      try {
        var refreshToken = AuthenticationHelper.extractRefreshTokenFromResponse(tokens);
        IdentifiedUser? identifierUser = await AuthenticationHelper.postUserRefreshAuthenticationRequest(
          "${constants.API_SERVER}/token/refresh",
          refreshToken,
        );
        return identifierUser == null;
      } catch (_) {
        return true;
      }
    } else {
      return false;
    }
  }

  bool _isExpired(String token) {
    final decodedToken = JWT.parse(token);
    var dateInSeconds = decodedToken.expiresAt;
    if (dateInSeconds == null) {
      return true;
    }
    var expiredDate = DateTime.fromMillisecondsSinceEpoch(dateInSeconds * 1000, isUtc: false);
    var now = DateTime.now();
    return now.difference(expiredDate).inMilliseconds > 0;
  }

  @visibleForTesting
  static bool isAdmin(List<dynamic> data) {
    return data.contains("ROLE_ADMIN");
  }

  void logout() {
    AuthenticationHelper.logout();
  }
}
