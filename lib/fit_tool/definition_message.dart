import 'dart:typed_data';

import 'package:collection/collection.dart';

import 'developer_field.dart';
import 'developer_field_definition.dart';
import 'field_definition.dart';
import 'utils/type_sizes.dart';

abstract class Message {
  Message({this.localId = 0, this.globalId = 0, this.endian = Endian.little, this.size = 0}) {
    RangeError.checkValueInInterval(globalId, 0, maxUint16, 'globalId');
    RangeError.checkValueInInterval(localId, 0, maxUint8, 'localId');
  }

  // ID that provides an association between the definition message,
  // data message and the FIT message in the Global FIT Profile.
  int localId;

  // ID of the FIT message in the Global FIT Profile.
  final int globalId;
  final Endian endian;
  int size;

  void removeField(int id);

  void removeDeveloperField(int developerDataIndex, int id);
}

class DefinitionMessage extends Message {
  DefinitionMessage({
    super.endian = Endian.little,
    super.globalId = 0,
    super.localId = 0,
    List<FieldDefinition>? fieldDefinitions,
    List<DeveloperFieldDefinition>? developerFieldDefinitions,
  })  : fieldDefinitions = fieldDefinitions ?? [],
        developerFieldDefinitions = developerFieldDefinitions ?? [],
        super(size: calculateSize(fieldDefinitions, developerFieldDefinitions));

  final List<FieldDefinition> fieldDefinitions;
  final List<DeveloperFieldDefinition> developerFieldDefinitions;

  int get definedDataSize {
    var size = 0;
    for (var fieldDefinition in fieldDefinitions) {
      size += fieldDefinition.size;
    }

    for (var developerFieldDefinition in developerFieldDefinitions) {
      size += developerFieldDefinition.size;
    }

    return size;
  }

  FieldDefinition? getFieldDefinition(int id) {
    return fieldDefinitions.firstWhereOrNull((element) => element.id == id);
  }

  @override
  void removeField(int id) {
    fieldDefinitions.removeWhere((fieldDefinition) => fieldDefinition.id == id);
    size = calculateSize(fieldDefinitions, developerFieldDefinitions);
  }

  @override
  void removeDeveloperField(int developerDataIndex, int id) {
    developerFieldDefinitions.removeWhere((fieldDefinition) => fieldDefinition.developerDataIndex == developerDataIndex && fieldDefinition.id == id);
    size = calculateSize(fieldDefinitions, developerFieldDefinitions);
  }

  static DefinitionMessage fromBytes(Uint8List bytes, {bool hasDeveloperFields = false}) {
    final bd = ByteData.sublistView(bytes);
    var offset = 0;

    // reserved
    offset += 1;

    // architecture
    final Endian endian;
    if (bd.getUint8(offset) == 0) {
      endian = Endian.little;
    } else {
      endian = Endian.big;
    }
    offset += 1;

    // global id
    final globalId = bd.getUint16(offset, endian);
    offset += 2;

    // number of fields
    final fieldCount = bd.getUint8(offset);
    offset += 1;

    // Field definitions;
    final fieldDefinitions = <FieldDefinition>[];
    for (var i = 0; i < fieldCount; i++) {
      final fdBytes = Uint8List.sublistView(bytes, offset, offset + FieldDefinition.fieldDefinitionSize);
      final fieldDefinition = FieldDefinition.fromBytes(fdBytes);
      fieldDefinitions.add(fieldDefinition);
      offset += FieldDefinition.fieldDefinitionSize;
    }

    final developerFieldDefinitions = <DeveloperFieldDefinition>[];
    if (hasDeveloperFields) {
      // number of developer fields
      final devFieldCount = bd.getUint8(offset);
      offset += 1;

      for (var i = 0; i < devFieldCount; i++) {
        final fdBytes = Uint8List.sublistView(bytes, offset, offset + DeveloperFieldDefinition.fieldDefinitionSize);
        final fieldDefinition = DeveloperFieldDefinition.fromBytes(fdBytes);
        developerFieldDefinitions.add(fieldDefinition);
        offset += DeveloperFieldDefinition.fieldDefinitionSize;
      }
    }

    return DefinitionMessage(endian: endian, globalId: globalId, fieldDefinitions: fieldDefinitions, developerFieldDefinitions: developerFieldDefinitions);
  }

  static int calculateSize(List<FieldDefinition>? fieldDefinitions, List<DeveloperFieldDefinition>? developerFieldDefinitions) {
    final fieldDefinitionsCount = fieldDefinitions?.length ?? 0;
    final developerFieldsCount = developerFieldDefinitions?.length ?? 0;

    if (developerFieldsCount == 0) {
      return 5 + FieldDefinition.fieldDefinitionSize * fieldDefinitionsCount + DeveloperFieldDefinition.fieldDefinitionSize * developerFieldsCount;
    } else {
      return 5 + FieldDefinition.fieldDefinitionSize * fieldDefinitionsCount + 1 + DeveloperFieldDefinition.fieldDefinitionSize * developerFieldsCount;
    }
  }

  List<DeveloperField> getDeveloperFields(Map<int, Map<int, DeveloperField>> developerFieldsById) {
    final developerFields = <DeveloperField>[];

    for (var fieldDefinition in developerFieldDefinitions) {
      final developerField = developerFieldsById[fieldDefinition.developerDataIndex]![fieldDefinition.id];

      if (developerField != null) {
        final sizedDeveloperField = DeveloperField.from(developerField, size: fieldDefinition.size);
        developerFields.add(sizedDeveloperField);
      }
    }

    return developerFields;
  }
}
