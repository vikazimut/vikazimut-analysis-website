// ignore_for_file: constant_identifier_names
import 'fit_file_header.dart';

class Fit {
  static final ProtocolVersion protocolVersion = ProtocolVersion(2, 3);
  static final ProfileVersion profileVersion = ProfileVersion(21, 60);
}
