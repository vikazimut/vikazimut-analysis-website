class EventItem {
  int id;
  String name;
  int courses;
  int participants;

  EventItem({
    required this.id,
    required this.name,
    required this.courses,
    required this.participants,
  });

  factory EventItem.fromJson(Map<String, dynamic> json) {
    return EventItem(
      id: json["id"] as int,
      name: json["name"] as String,
      courses: json["courses"] as int,
      participants: json["participants"] as int,
    );
  }
}
