// coverage:ignore-file
import 'event_detail_page_view.dart';

class EventPageView extends EventDetailPageView {
  static const String routePath = '/event-detail';

  EventPageView(super.args);
}
