// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/theme/theme.dart';

import 'event_info_courses_widget.dart';
import 'event_info_participants_widget.dart';
import 'event_page_presenter.dart';
import 'model/event_details.dart';

class EventInfoDetailWidget extends StatelessWidget {
  final EventDetail eventDetails;
  final EventPagePresenter presenter;

  const EventInfoDetailWidget({required this.eventDetails, required this.presenter});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          child: Text.rich(
            TextSpan(children: [
              TextSpan(
                text: L10n.getString("event_info_type"),
                style: Theme.of(context).textTheme.headlineMedium!.copyWith(color: kOrangeColor),
              ),
              TextSpan(
                text: " ${eventDetails.typeName}",
                style: Theme.of(context).textTheme.headlineMedium,
              ),
            ]),
          ),
        ),
        Expanded(
          child: DefaultTabController(
            length: 2,
            child: Column(
              children: [
                Container(
                  constraints: const BoxConstraints(maxWidth: 500),
                  child: TabBar(
                    labelColor: Colors.white,
                    labelPadding: const EdgeInsets.only(top: 10, bottom: 10),
                    tabs: [
                      Text("${L10n.getString("event_info_participants")} (${eventDetails.participants.length})"),
                      Text("${L10n.getString("event_info_courses")} (${eventDetails.eventCourses.length})"),
                    ],
                  ),
                ),
                Expanded(
                  child: TabBarView(
                    children: [
                      Align(
                        alignment: Alignment.topCenter,
                        child: Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: EventInfoParticipantsWidget(
                            eventDetails: eventDetails,
                          ),
                        ),
                      ),
                      Align(
                        alignment: Alignment.topCenter,
                        child: Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: EventInfoCoursesWidget(
                            eventCourses: eventDetails.eventCourses,
                            eventType: eventDetails.type,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
