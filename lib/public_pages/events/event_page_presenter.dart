import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:vikazimut_website/constants.dart' as constants;

import 'event_detail_page_view.dart';
import 'model/event_details.dart';

class EventPagePresenter {
  static const String SERVER_POST_EVENT_REGISTER_URL = "${constants.API_SERVER}/events/register";
  EventDetailPageViewState? _view;

  Future<EventDetail?> loadEventInfo(int eventId) async {
    try {
      final response = await http.get(Uri.parse("${constants.API_SERVER}/events/info/$eventId"));
      if (response.statusCode == HttpStatus.ok) {
        var data = jsonDecode(response.body);
        return EventDetail.fromJson(data);
      } else {
        return Future.error(response.body);
      }
    } catch (_) {
      return Future.error("Server connection error");
    }
  }

  Future<void> sendRegisterParticipantToServer({required String nickname, required int eventId}) async {
    String jsonMessage = '{"nickname": "$nickname", "event_id": $eventId}';
    try {
      final response = await http.post(
        Uri.parse(SERVER_POST_EVENT_REGISTER_URL),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonMessage,
      );
      if (response.statusCode == HttpStatus.ok) {
        _view?.update(jsonDecode(response.body));
      } else if (response.statusCode == HttpStatus.badRequest) {
        _view?.showError1(jsonDecode(response.body));
      } else if (response.statusCode == HttpStatus.forbidden) {
        _view?.showError2(jsonDecode(response.body));
      }
    } catch (_) {
      return _view?.showError1("Server connection error");
    }
  }

  void setView(EventDetailPageViewState view) {
    _view = view;
  }
}
