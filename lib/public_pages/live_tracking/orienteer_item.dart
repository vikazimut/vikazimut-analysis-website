import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:vikazimut_website/public_pages/model/geodesic_point.dart';

import 'color_picker.dart';

class OrienteerItem {
  final int id;
  final String nickname;
  final int startDateInMilliseconds;
  int totalTimeInMilliseconds = 0;
  Color color = ColorPicker.unselectedColor;
  late List<GeodesicPoint> _currentPositions;
  late List<GeodesicPoint> _originalPositions;
  final List<GeodesicPoint> _tailPositions = [];
  int? score;
  bool isDisconnected = false;
  bool _finished = false;

  OrienteerItem({
    required List<GeodesicPoint> currentPositions,
    required this.id,
    required this.nickname,
    required this.startDateInMilliseconds,
    this.score,
  })  : _currentPositions = currentPositions,
        _originalPositions = List.from(currentPositions);

  @visibleForTesting
  set currentPositions(List<GeodesicPoint> positions) => _currentPositions = positions;

  @visibleForTesting
  List<GeodesicPoint> get currentPositions => _currentPositions;

  GeodesicPoint get currentPosition => _currentPositions.last;

  List<GeodesicPoint> get tailPositions => _tailPositions;

  bool get hasFinished => _finished;

  void finish() {
    if (!_finished) {
      _finished = true;
      totalTimeInMilliseconds = DateTime.now().millisecondsSinceEpoch - startDateInMilliseconds;
    }
  }

  @override
  bool operator ==(Object other) => (other is OrienteerItem) && other.id == id;

  @override
  int get hashCode => id.hashCode;

  factory OrienteerItem.fromJson(Map<String, dynamic> json) {
    final String? latitudes = json["latitudes"];
    final String? longitudes = json["longitudes"];
    List<GeodesicPoint> positions = parsePositions(latitudes!, longitudes!);
    final int? score = json["score"];
    return OrienteerItem(
      currentPositions: positions,
      id: json["id"] as int,
      nickname: json["nickname"],
      startDateInMilliseconds: json["startTimestamp"] * 1000,
      score: score,
    );
  }

  void updateTail() {
    final GeodesicPoint position = _currentPositions.last;
    if (_currentPositions.length > 1) {
      _currentPositions.removeLast();
    }
    if (_tailPositions.length > 3) {
      _tailPositions.removeLast();
    }
    _tailPositions.insert(0, position);
  }

  @visibleForTesting
  static String abbreviate_(String nickname) {
    return nickname.substring(0, math.min(3, nickname.length)).trim();
  }

  @visibleForTesting
  static List<GeodesicPoint> parsePositions(String latitudes, String longitudes) {
    List<String> split1 = latitudes.split("|");
    List<String> split2 = longitudes.split("|");
    List<GeodesicPoint> positions = [];
    for (int i = 0; i < math.min(split1.length, split2.length); i++) {
      double latitude = double.parse(split1[i]);
      double longitude = double.parse(split2[i]);
      positions.add(GeodesicPoint(latitude, longitude));
    }
    return positions;
  }

  bool isNotSameLocations(OrienteerItem orienteer) {
    var a = orienteer._originalPositions;
    var b = _originalPositions;
    if (a.length != b.length) {
      return true;
    }
    for (int index = 0; index < a.length; index += 1) {
      if (a[index].latitude != b[index].latitude || a[index].longitude != b[index].longitude) {
        return true;
      }
    }
    return false;
  }

  void updatePosition(OrienteerItem orienteer) {
    _currentPositions = orienteer._currentPositions;
    _originalPositions = orienteer._originalPositions;
    score = orienteer.score;
  }
}
