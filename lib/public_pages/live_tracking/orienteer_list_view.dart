// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/time.dart';

import 'live_tracking_presenter.dart';
import 'orienteer_item.dart';

class OrienteerListView extends StatefulWidget {
  final LiveTrackingPresenter presenter;

  const OrienteerListView({required this.presenter});

  @override
  State<OrienteerListView> createState() => OrienteerListViewState();
}

class OrienteerListViewState extends State<OrienteerListView> {
  String? _errorMessage;

  @override
  void initState() {
    widget.presenter.orienteerListView = this;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final orienteers = widget.presenter.orienteers;
    if (_errorMessage != null) {
      return Padding(
        padding: const EdgeInsets.fromLTRB(2, 20, 2, 0),
        child: Center(
            child: Text(
          _errorMessage!,
          style: const TextStyle(color: kRed),
        )),
      );
    } else if (orienteers.isEmpty && widget.presenter.isLiveStarted) {
      return Padding(
        padding: const EdgeInsets.fromLTRB(2, 20, 2, 0),
        child: Center(child: Text(L10n.getString("live_tracking_empty_list"))),
      );
    } else {
      return Column(
        children: [
          if (widget.presenter.isLiveTrackingCreator && orienteers.isNotEmpty)
            Padding(
              padding: const EdgeInsets.only(top: 8, bottom: 8),
              child: Text(
                L10n.getString("live_tracking_remove_message"),
                style: const TextStyle(
                  fontSize: 12,
                  fontStyle: FontStyle.italic,
                ),
              ),
            ),
          Padding(
            padding: const EdgeInsets.fromLTRB(2, 8.0, 2, 0),
            child: ListView.builder(
              shrinkWrap: true,
              itemCount: orienteers.length,
              itemBuilder: (context, index) {
                OrienteerItem orienteer = orienteers[index];
                return Dismissible(
                  key: UniqueKey(),
                  direction: (widget.presenter.isLiveTrackingCreator && !orienteer.hasFinished) ? DismissDirection.startToEnd : DismissDirection.none,
                  background: Container(
                    color: kRed,
                    padding: const EdgeInsets.symmetric(horizontal: 20.0),
                    alignment: AlignmentDirectional.centerStart,
                    child: const Icon(
                      Icons.delete,
                      color: Colors.white,
                    ),
                  ),
                  child: _buildOrienteerItem(orienteer),
                  onDismissed: (_) async {
                    await widget.presenter.dismiss(index);
                    setState(() {});
                  },
                );
              },
            ),
          ),
        ],
      );
    }
  }

  Widget _buildOrienteerItem(OrienteerItem orienteer) {
    final int now = DateTime.now().millisecondsSinceEpoch;
    final durationInMillisecond = orienteer.hasFinished ? orienteer.totalTimeInMilliseconds : now - orienteer.startDateInMilliseconds;

    return GestureDetector(
      onTap: (orienteer.hasFinished) ? null : () async => widget.presenter.selectOrUnselectOrienteer(orienteer),
      child: Container(
        padding: const EdgeInsets.fromLTRB(2, 2, 0, 1),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5.0),
          color: Theme.of(context).colorScheme.surface,
          border: Border.all(color: kOrangeColorDisabled),
        ),
        margin: const EdgeInsets.fromLTRB(5, 3, 5, 3),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Expanded(
              child: Text(
                orienteer.nickname,
                style: const TextStyle(fontSize: 14),
              ),
            ),
            Text(
              timeToString(durationInMillisecond),
              style: const TextStyle(fontSize: 13),
            ),
            SizedBox(
              width: 35,
              child: (orienteer.score != null)
                  ? Text(
                      "(${orienteer.score})",
                      style: const TextStyle(fontSize: 13),
                      textAlign: TextAlign.center,
                    )
                  : const Text(''),
            ),
            if (orienteer.hasFinished)
              const Icon(
                Icons.sports_score_outlined,
              )
            else if (orienteer.isDisconnected)
              ImageIcon(
                AssetImage('assets/images/not_listed_location.png'),
                color: orienteer.color,
              )
            else
              ClipRRect(
                borderRadius: BorderRadius.circular(3.0),
                child: Container(
                  height: 20.0,
                  width: 16.0,
                  color: orienteer.color,
                ),
              ),
          ],
        ),
      ),
    );
  }

  void update() {
    _errorMessage = null;
    if (mounted) {
      setState(() {});
    }
  }

  void displayError(String errorMessage) {
    setState(() {
      _errorMessage = errorMessage;
    });
  }
}
