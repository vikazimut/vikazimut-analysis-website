import 'package:flutter/material.dart';

class ColorPicker {
  static const Color unselectedColor = Color(0x00000000);
  static const List<Color> _basicColors = [
    Color(0xFFE6194B),
    Color(0xFF4363D8),
    Color(0xFF3B9700),
    Color(0xFFF032E6),
    Color(0xFF46F0F0),
    Color(0xFF0D98BA),
    Color(0xFFFFEF00),
    Color(0xFF145A32),
    Color(0xFFF58231),
    Color(0xFF911EB4),
    Color(0xFF9A6324),
    Color(0xFFFABEBE),
    Color(0xFF0000FF),
    Color(0xFF777777),
    Color(0xFFBCF60C),
    Color(0xFF800000),
    Color(0xFFCE961C),
    Color(0xFF7900D7),
    Color(0xFF000075),
    Color(0xFFB0B0B0),
  ];

  Color pickColor(int index) {
    return _basicColors[index % _basicColors.length];
  }

  @visibleForTesting
  int size_() {
    return _basicColors.length;
  }
}
