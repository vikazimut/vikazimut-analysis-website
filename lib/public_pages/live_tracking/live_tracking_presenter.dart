import 'dart:async';
import 'dart:ui' as ui;

// ignore: depend_on_referenced_packages
import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/public_pages/model/course_georeference.dart';

import 'animation_presenter.dart';
import 'color_picker.dart';
import 'orienteer_item.dart';
import 'orienteer_list_view.dart';
import 'server_gateway.dart';
import 'settings_view.dart';

abstract class LiveTrackingPresenter {
  late OrienteerListViewState _sidebar;
  late CourseGeoreference? _courseData;
  AnimationPresenter? _animationPresenter;
  final LiveTrackingPreferences liveTrackingPreferences = LiveTrackingPreferences();
  final ServerGateway _server = ServerGateway();
  final int _courseId;
  final List<OrienteerItem> _orienteers = [];
  final List<OrienteerItem> _selectedOrienteers = [];
  final ColorPicker _colorPicker = ColorPicker();
  Timer? _timer;
  bool _liveStarted = false;
  int _currentColorIndex = 0;

  LiveTrackingPresenter(this._courseId);

  bool get isLiveStarted => _liveStarted;

  List<OrienteerItem> get orienteers => _orienteers;

  set orienteerListView(OrienteerListViewState sidebar) => _sidebar = sidebar;

  set animationPresenter(AnimationPresenter animationPresenter) => _animationPresenter = animationPresenter;

  CourseGeoreference? get courseData => _courseData;

  void dispose() {
    _timer?.cancel();
    _timer = null;
  }

  String getStartButtonText(bool isCurrentLiveTracking);

  String getQuitButtonText();

  Future<void> startLiveTracking(int courseId);

  Future<bool> isRunningLiveTrackingFor(int courseId) {
    return _server.isRunningLiveTrackingFor(courseId);
  }

  void stopLiveTracking();

  bool get isLiveTrackingCreator => _server.isLiveTrackingCreator;

  void loadPreferences() {
    liveTrackingPreferences.loadPreferences();
  }

  Future<CourseGeoreference> loadCourseData() async {
    _courseData = await CourseGeoreference.loadCourseMap(_courseId);
    return _courseData!;
  }

  Future<ui.Image> loadImage() => _server.loadImage(_courseData?.imageId);

  NetworkImage loadNetworkImage(String imageId) => _server.loadNetworkImage(imageId);

  void selectOrUnselectOrienteer(OrienteerItem orienteer) {
    if (_selectedOrienteers.contains(orienteer)) {
      _unselectOrienteer(orienteer);
    } else {
      _selectOrienteer(orienteer);
    }
    _sidebar.update();
    _animationPresenter?.displayOrienteerPositions(_selectedOrienteers);
  }

  Future<void> dismiss(int index);

  void _selectOrienteer(OrienteerItem orienteer) {
    orienteer.color = _colorPicker.pickColor(_currentColorIndex++);
    _selectedOrienteers.add(orienteer);
  }

  void _unselectOrienteer(OrienteerItem orienteer) {
    orienteer.color = ColorPicker.unselectedColor;
    _selectedOrienteers.remove(orienteer);
  }

  void _updateOrienteerList(List<OrienteerItem> orienteers) {
    List<OrienteerItem> beginners = getNewOrUpdateOrienteers_(orienteers, _orienteers);
    List<OrienteerItem> finishers = getFinishers_(orienteers, _orienteers);
    removeFinishers_(finishers);
    addNewOrienteers_(beginners);
    sortOrienteers(_orienteers);
    sortOrienteers(finishers);
    _orienteers.addAll(finishers);
    _sidebar.update();
  }

  @visibleForTesting
  void sortOrienteers(List<OrienteerItem> orienteers) {
    if (liveTrackingPreferences.sortCriteria == SortCriteria.time) {
      orienteers.sort((a, b) => b.totalTimeInMilliseconds.compareTo(a.totalTimeInMilliseconds));
    } else {
      orienteers.sort((a, b) {
        final scoreA = a.score ?? -1;
        final scoreB = b.score ?? -1;
        return scoreB.compareTo(scoreA);
      });
    }
  }

  @visibleForTesting
  void addNewOrienteers_(List<OrienteerItem> beginners) {
    for (int i = 0; i < beginners.length; i++) {
      var orienteer = beginners[i];
      _selectOrienteer(orienteer);
      _orienteers.add(orienteer);
    }
  }

  @visibleForTesting
  void removeFinishers_(List<OrienteerItem> finishers) {
    for (int i = 0; i < finishers.length; i++) {
      final finisher = finishers[i];
      _finishOrienteer(finisher);
      _orienteers.remove(finisher);
    }
  }

  void _finishOrienteer(OrienteerItem orienteer) {
    if (_isSelected(orienteer)) {
      _unselectOrienteer(orienteer);
    }
    orienteer.finish();
  }

  @visibleForTesting
  List<OrienteerItem> getNewOrUpdateOrienteers_(
    List<OrienteerItem> newOrienteerList,
    List<OrienteerItem> orienteerList,
  ) {
    List<OrienteerItem> beginners = [];
    for (int i = 0; i < newOrienteerList.length; i++) {
      var orienteer = newOrienteerList[i];
      OrienteerItem? o = orienteerList.firstWhereOrNull((OrienteerItem o) => o.id == orienteer.id);
      if (o == null) {
        beginners.add(orienteer);
      } else {
        if (o.isNotSameLocations(orienteer)) {
          o.isDisconnected = false;
          o.updatePosition(orienteer);
        } else {
          o.isDisconnected = true;
        }
      }
    }
    return beginners;
  }

  @visibleForTesting
  List<OrienteerItem> getFinishers_(
    List<OrienteerItem> newOrienteerList,
    List<OrienteerItem> orienteerList,
  ) {
    List<OrienteerItem> finishers = [];
    for (int i = 0; i < orienteerList.length; i++) {
      final orienteer = orienteerList[i];
      if (!newOrienteerList.contains(orienteer)) {
        finishers.add(orienteer);
      }
    }
    return finishers;
  }

  static bool _isSelected(OrienteerItem orienteer) {
    return orienteer.color != ColorPicker.unselectedColor;
  }

  Timer _setTimer(period) {
    _getOrienteerList();
    _refreshOrienteersPositionOnCanvas();
    return Timer.periodic(Duration(seconds: period), (Timer timer) {
      if (timer.tick % 3 == 0) {
        _getOrienteerList();
      }
      _refreshOrienteersPositionOnCanvas();
    });
  }

  void _refreshOrienteersPositionOnCanvas() {
    _animationPresenter?.displayOrienteerPositions(_selectedOrienteers);
    _sidebar.update();
    _updateOrienteersPositions(_selectedOrienteers);
  }

  void _getOrienteerList() async {
    List<OrienteerItem>? orienteers = await _server.getOrienteerList();
    if (orienteers != null) {
      _updateOrienteerList(orienteers);
    }
  }

  void _updateOrienteersPositions(List<OrienteerItem> orienteers) {
    for (final orienteer in orienteers) {
      orienteer.updateTail();
    }
  }
}

class LiveTrackingCreatorPresenter extends LiveTrackingPresenter {
  LiveTrackingCreatorPresenter(super.courseId);

  @override
  String getStartButtonText(bool isCurrentLiveTracking) {
    if (isCurrentLiveTracking) {
      return "live_tracking_auditor_start_button_label";
    } else {
      return "live_tracking_creator_start_button_label";
    }
  }

  @override
  String getQuitButtonText() {
    if (isLiveTrackingCreator) {
      return "live_tracking_creator_stop_button_label";
    } else {
      return "live_tracking_auditor_stop_button_label";
    }
  }

  @override
  Future<void> startLiveTracking(int courseId) async {
    try {
      await _server.startLiveTracking(courseId);
      _liveStarted = true;
      _timer = _setTimer(LiveTrackingPreferences.periodicityInSecond);
      _updateOrienteerList([]);
    } catch (e) {
      _sidebar.displayError("${L10n.getString("live_tracking_start_error1")}\n($e)");
    }
  }

  @override
  void stopLiveTracking() {
    _liveStarted = false;
    _animationPresenter?.displayOrienteerPositions([]);
    _server.stopLiveTracking();
    dispose();
  }

  @override
  Future<void> dismiss(int index) async {
    OrienteerItem orienteer = dismissOrienteer_(index);
    _server.dismissOrienteer(orienteer);
  }

  @visibleForTesting
  OrienteerItem dismissOrienteer_(int index) {
    var orienteer = _orienteers[index];
    _finishOrienteer(orienteer);
    _orienteers.removeAt(index);
    _orienteers.add(orienteer);
    return orienteer;
  }
}

class LiveTrackingAuditorPresenter extends LiveTrackingPresenter {
  LiveTrackingAuditorPresenter(super.courseId);

  @override
  String getStartButtonText(bool isCurrentLiveTracking) {
    return "live_tracking_auditor_start_button_label";
  }

  @override
  String getQuitButtonText() {
    return "live_tracking_auditor_stop_button_label";
  }

  @override
  Future<void> startLiveTracking(int courseId) async {
    try {
      await _server.joinLiveTracking(courseId);
      _liveStarted = true;
      _timer = _setTimer(LiveTrackingPreferences.periodicityInSecond);
      _updateOrienteerList([]);
    } on EmptyLiveException catch (_) {
      _sidebar.displayError(L10n.getString("live_tracking_start_error2"));
      _liveStarted = false;
    } catch (e) {
      _liveStarted = false;
      _sidebar.displayError("${L10n.getString("live_tracking_start_error1")}\n($e)");
    }
  }

  @override
  void stopLiveTracking() {
    _liveStarted = false;
    _animationPresenter?.displayOrienteerPositions([]);
    dispose();
  }

  @override
  Future<void> dismiss(int index) async {}
}
