// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:vikazimut_website/common/discipline.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/menu/responsive_scaffold_widget.dart';
import 'package:vikazimut_website/public_pages/model/course_georeference.dart';
import 'package:vikazimut_website/public_pages/routes/route_list_page_view.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/alert_dialog.dart';
import 'package:vikazimut_website/utils/global_error_widget.dart';
import 'package:vikazimut_website/utils/screen_helper.dart';
import 'package:vikazimut_website/utils/sidebar_widget/collapsible_or_rigid_sidebar.dart';

import 'live_tracking_presenter.dart';
import 'map_view.dart';
import 'orienteer_list_view.dart';

class LiveTrackingPage extends StatefulWidget {
  static const String routePathPublic = '/course/live';
  static const String routePathPlanner = '/planner/course/live';
  final int _courseId;
  final bool _isPlanner;

  LiveTrackingPage(args, this._isPlanner) : _courseId = int.parse(args);

  @override
  State<LiveTrackingPage> createState() => LiveTrackingPageState();
}

class LiveTrackingPageState extends State<LiveTrackingPage> {
  late final Future<CourseGeoreference> _courseData;
  late final LiveTrackingPresenter _presenter;
  late final Future<bool> _isRunningLiveTracking;

  @override
  void initState() {
    if (widget._isPlanner) {
      _presenter = LiveTrackingCreatorPresenter(widget._courseId);
    } else {
      _presenter = LiveTrackingAuditorPresenter(widget._courseId);
    }
    _courseData = _presenter.loadCourseData();
    _isRunningLiveTracking = _presenter.isRunningLiveTrackingFor(widget._courseId);
    _presenter.loadPreferences();
    super.initState();
  }

  @override
  Future<void> dispose() async {
    _presenter.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ResponsiveScaffoldWidget(
      body: FutureBuilder<List<dynamic>>(
        future: Future.wait([_courseData, _isRunningLiveTracking]),
        builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
          if (snapshot.hasError) {
            final errorText = L10n.translateIfPossible(snapshot.error.toString());
            return GlobalErrorWidget(errorText);
          } else if (snapshot.hasData) {
            final courseData = snapshot.data![0];
            final bool isCurrentLiveTracking = snapshot.data![1];
            return SizedBox(
              height: ScreenHelper.isDesktop(context) ? MediaQuery.of(context).size.height - 70 : 1000,
              child: CollapsibleOrRigidSidebar(
                isCollapsible: !ScreenHelper.isDesktop(context),
                sidebarTitle: Column(
                  children: [
                    Row(
                      children: [
                        Icon(
                          Discipline.getIcon(courseData.discipline),
                          color: kOrangeColor,
                          size: 32,
                        ),
                        const SizedBox(width: 2),
                        Expanded(
                          child: Text(
                            courseData.name,
                            style: Theme.of(context).textTheme.headlineMedium?.copyWith(color: kOrangeColor),
                          ),
                        ),
                      ],
                    ),
                    if (_presenter.isLiveStarted)
                      Center(
                        child: ElevatedButton.icon(
                          style: ButtonStyle(backgroundColor: WidgetStateProperty.all(kRed)),
                          onPressed: () {
                            if (_presenter.isLiveTrackingCreator) {
                              showBinaryQuestionAlertDialog(
                                context: context,
                                title: L10n.getString("live_tracking_quit_title"),
                                message: L10n.getString("live_tracking_quit_message"),
                                okMessage: L10n.getString("live_tracking_quit_ok_label"),
                                noMessage: L10n.getString("live_tracking_quit_no_label"),
                                action: () {
                                  _presenter.stopLiveTracking();
                                  GoRouter.of(context).go("${RouteListPageView.routePath}/${widget._courseId}");
                                },
                              );
                            } else {
                              GoRouter.of(context).go("${RouteListPageView.routePath}/${widget._courseId}");
                            }
                          },
                          icon: const Icon(
                            Icons.not_started_outlined,
                            size: 24.0,
                            color: Colors.white,
                          ),
                          label: Text(L10n.getString(_presenter.getQuitButtonText())),
                        ),
                      )
                    else
                      Center(
                        child: ElevatedButton.icon(
                          style: ButtonStyle(backgroundColor: WidgetStateProperty.all(kGreenColor)),
                          onPressed: () async {
                            await _presenter.startLiveTracking(widget._courseId);
                            setState(() {});
                          },
                          icon: const Icon(
                            Icons.not_started_outlined,
                            size: 24.0,
                            color: Colors.white,
                          ),
                          label: Text(L10n.getString(_presenter.getStartButtonText(isCurrentLiveTracking))),
                        ),
                      ),
                    Center(
                      child: Padding(
                        padding: const EdgeInsets.only(top: 8),
                        child: Text(
                          L10n.getString("live_tracking_time_warning_message"),
                          style: const TextStyle(
                            fontSize: 12,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                sidebarContents: [
                  OrienteerListView(presenter: _presenter),
                ],
                contents: MapView(courseData: courseData, presenter: _presenter),
              ),
            );
          } else {
            return const Center(child: CircularProgressIndicator());
          }
        },
      ),
    );
  }
}
