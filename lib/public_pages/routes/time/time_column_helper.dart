import 'package:flutter/material.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/time.dart';

import 'time_sheet_presenter.dart';

class TimeColumnHelper {
  TimeColumnHelper._();

  static String distanceDataToString(
    double actualDistance,
    double straightDistance,
    double actualElevation,
  ) {
    var overcost = TimeSheetPresenter.computeOvercostPercentage(actualDistance, straightDistance);
    String distanceText;
    if (actualElevation >= 0) {
      distanceText = '${L10n.formatNumber(actualDistance.toInt(), 0)} (+${actualElevation.toInt()})\n+${overcost.toInt()}%';
    } else {
      distanceText = '${L10n.formatNumber(actualDistance.toInt(), 0)} (-)\n+${overcost.toInt()}%';
    }
    return distanceText;
  }

  static double calculateSpeedInKmh(int timeInMilliseconds, double distanceInMeter) {
    if (timeInMilliseconds == 0) {
      return 0;
    } else {
      return 3600.0 * distanceInMeter / timeInMilliseconds;
    }
  }

  static int calculatePaceInMillisecondPerKm(int timeInMilliseconds, double distanceInMeter) {
    if (distanceInMeter == 0) {
      return 0;
    } else {
      return 1000 * timeInMilliseconds ~/ distanceInMeter;
    }
  }

  static String formatSpeed(double speed) {
    if (speed <= 0) {
      return '0';
    } else {
      return L10n.formatNumber(speed, 1);
    }
  }

  static String formatTime(int time) {
    if (time <= 0) {
      return '';
    } else {
      return timeToString(time, withHour: false);
    }
  }
}

class TextCentered extends Text {
  const TextCentered(super.text) : super(textAlign: TextAlign.center);
}

class TextCenteredBold extends Text {
  const TextCenteredBold(super.text) : super(textAlign: TextAlign.center, style: const TextStyle(fontWeight: FontWeight.bold));
}

class TotalCell extends StatelessWidget {
  final Widget child;

  const TotalCell({required this.child});

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: const BoxDecoration(
          border: Border(
            bottom: BorderSide(width: 3.0, color: kOrangeColor),
          ),
        ),
        child: Center(child: child));
  }
}

class ColumnHeaderText extends StatelessWidget {
  final String label;

  const ColumnHeaderText({
    required this.label,
  });

  @override
  Widget build(BuildContext context) {
    return Expanded(child: Container(alignment: Alignment.topCenter, padding: const EdgeInsets.only(top: 10), child: TextCentered(label)));
  }
}
