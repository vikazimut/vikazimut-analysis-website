// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:vikazimut_website/constants.dart' as constants;
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/public_pages/routes/route_list_page_presenter.dart';
import 'package:vikazimut_website/utils/html_utils.dart';
import 'package:vikazimut_website/utils/sidebar_widget/collapsible_sidebar_widget.dart';

import 'csv_converter.dart';

class SettingsSideBar extends StatelessWidget {
  final RouteListPagePresenter routePresenter;

  const SettingsSideBar(this.routePresenter);

  @override
  Widget build(BuildContext context) {
    return CollapsibleSideBar(
      position: Position.RIGHT,
      isOpen: false,
      title: const SizedBox.shrink(),
      maxWidth: constants.COLLAPSIBLE_MAX_WIDTH,
      minWidth: constants.COLLAPSIBLE_MIN_WIDTH,
      padding: const EdgeInsets.all(10.0),
      children: [
        SizedBox(
          width: 380,
          child: Padding(
            padding: const EdgeInsets.only(top: 15),
            child: OutlinedButton(
              onPressed: () {
                downloadStringInFile("${routePresenter.courseId}.csv", CsvConverter.convertResultToCsv(routePresenter.selectedTracks, routePresenter.courseData!));
              },
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Expanded(
                    child: Text(
                      L10n.getString("time_sheet_split_export_button"),
                    ),
                  ),
                  const SizedBox(width: 5),
                  const Icon(Icons.file_download),
                ],
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 30, bottom: 10),
          child: Text(
            L10n.getString("time_sheet_split_caption"),
            style: Theme.of(context).textTheme.headlineMedium,
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(L10n.getString("time_sheet_split_caption_forced")),
        ),
      ],
    );
  }
}
