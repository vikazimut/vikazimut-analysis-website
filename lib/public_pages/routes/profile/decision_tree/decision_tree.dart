import 'dart:math' as math;

import 'package:community_charts_flutter/community_charts_flutter.dart' as charts;

import '../orienteer_grades.dart';
import '../speed_in_kmh.dart';
import 'profile_constants.dart';

abstract class DecisionTree {
  final ProfileConstants profileConstants;

  const DecisionTree(this.profileConstants);

  OrienteerProfile calculateOrienteeringLevel(List<SpeedInKmH> data);

  int get maxSpeed => profileConstants.maxSpeed;

  int get speedPerBin => profileConstants.speedPerBin;

  List<charts.TickSpec<String>> getTicksXAxis() => profileConstants.getTicksXAxis();
}

class OrienteerProfile {
  OrienteerGrade orienteerGrade;
  String? characteristics;
  String? advices;

  OrienteerProfile(this.orienteerGrade);

  void addCharacteristic(String s) {
    if (characteristics == null) {
      characteristics = s;
    } else {
      characteristics = "$s\n${characteristics!}";
    }
  }

  void addAdvice(String s) {
    if (advices == null) {
      advices = s;
    } else {
      advices = "$s\n${advices!}";
    }
  }
}

bool isNotEnoughData(List<SpeedInKmH> data) {
  for (var i = 0; i < data.length; i++) {
    if (data[i].percent != 0) {
      return false;
    }
  }
  return true;
}

double computePercentOfStoppingPeriod(List<SpeedInKmH> data) {
  return data[0].percent + data[1].percent + data[2].percent;
}

bool isThereRunningPeriod(List<SpeedInKmH> data) {
  final int speedInKmh = lastBinGreaterThan(data, 5);
  return speedInKmh >= 8;
}

int lastBinGreaterThan(List<SpeedInKmH> data, int percent) {
  int maxSpeed = 0;
  for (var i = 3; i < data.length; i++) {
    if (data[i].percent > percent) {
      maxSpeed = i;
    }
  }
  return maxSpeed;
}

int computeMaximumPeak(List<SpeedInKmH> data) {
  var maximumPercent = 0.0;
  var maximumSpeed = -1;
  for (var i = 2; i < data.length; i++) {
    if (data[i].percent >= maximumPercent) {
      maximumPercent = data[i].percent;
      maximumSpeed = data[i].speed;
    }
  }
  return maximumSpeed;
}

int computeMinimumCumulatedSpeed(List<SpeedInKmH> data, double percent) {
  double sum = 0;
  for (var i = 0; i < data.length; ++i) {
    sum += data[i].percent;
    if (sum >= percent) {
      return i;
    }
  }
  return 0;
}

int computeMaximumCumulatedSpeed(List<SpeedInKmH> data, double percent) {
  double sum = 0;
  for (var i = data.length - 1; i >= 0; --i) {
    sum += data[i].percent;
    if (sum >= percent) {
      return i;
    }
  }
  return data.length - 1;
}

int computeHistogramWidth(List<SpeedInKmH> data, int thresholdPercent) {
  int numberOfSpeedsWithHighPercent = 0;
  for (var i = 2; i < data.length; ++i) {
    if (data[i].percent >= thresholdPercent) {
      numberOfSpeedsWithHighPercent++;
    }
  }
  return numberOfSpeedsWithHighPercent;
}

double computeMeanSpeed(List<SpeedInKmH> data) {
  double sum = 0;
  for (int i = 0; i < data.length; i++) {
    sum += data[i].percent * data[i].speed;
  }
  return sum / 100;
}

double computeStandardDeviation(List<SpeedInKmH> data, double meanSpeed) {
  double sum = 0;
  for (int i = 0; i < data.length; i++) {
    sum += data[i].percent * (data[i].speed - meanSpeed) * (data[i].speed - meanSpeed);
  }
  return math.sqrt(sum / 100.0);
}

int computeMinSpeedGreaterThan(List<SpeedInKmH> data, int percent) {
  for (var i = 2; i < data.length; ++i) {
    if (data[i].percent >= percent) {
      return i;
    }
  }
  return 0;
}
