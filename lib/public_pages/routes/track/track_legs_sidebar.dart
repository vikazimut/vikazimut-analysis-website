// coverage:ignore-file
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:sprintf/sprintf.dart';
import 'package:vikazimut_website/constants.dart' as constants;
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/public_pages/routes/gps_route.dart';
import 'package:vikazimut_website/public_pages/routes/route_list_page_presenter.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/sidebar_widget/collapsible_sidebar_widget.dart';
import 'package:vikazimut_website/utils/time.dart';

import 'track_tab_presenter.dart';

class LegStatsSidebar extends StatefulWidget {
  final TrackTabPresenter trackTabPresenter;
  final RouteListPagePresenter presenter;

  const LegStatsSidebar(this.trackTabPresenter, this.presenter);

  @override
  State<LegStatsSidebar> createState() => LegStatsSidebarState();
}

class LegStatsSidebarState extends State<LegStatsSidebar> {
  int _legIndex = 0;

  @override
  void initState() {
    widget.trackTabPresenter.setSideBarView(this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return CollapsibleSideBar(
      position: Position.RIGHT,
      isOpen: false,
      title: Text(
        (_legIndex == 0) ? "" : sprintf(L10n.getString("track_leg_sidebar_data_title"), [_legIndex]),
        style: Theme.of(context).textTheme.headlineMedium,
      ),
      maxWidth: constants.COLLAPSIBLE_MAX_WIDTH + 55,
      minWidth: constants.COLLAPSIBLE_MIN_WIDTH,
      padding: const EdgeInsets.all(1.0),
      children: [
        if (_legIndex > 0)
          DataTable(
            border: TableBorder.all(
              style: BorderStyle.solid,
              width: 1,
              color: kOrangeColorDisabled,
            ),
            dataTextStyle: const TextStyle(fontSize: 13),
            showBottomBorder: true,
            horizontalMargin: 5,
            columnSpacing: 5,
            headingRowHeight: 32,
            dataRowMinHeight: 20,
            dataRowMaxHeight: 20,
            columns: <DataColumn>[
              DataColumn(label: Expanded(child: Text(L10n.getString("track_leg_sidebar_data_nickname"), textAlign: TextAlign.center))),
              DataColumn(label: Expanded(child: Text(L10n.getString("track_leg_sidebar_data_time"), textAlign: TextAlign.center))),
              DataColumn(label: Expanded(child: Text(L10n.getString("track_leg_sidebar_data_distance"), textAlign: TextAlign.center))),
              DataColumn(label: Expanded(child: Text(L10n.getString("track_leg_sidebar_data_elevation"), textAlign: TextAlign.center))),
              DataColumn(label: Expanded(child: Text(L10n.getString("track_leg_sidebar_data_speed"), textAlign: TextAlign.center))),
              DataColumn(label: Expanded(child: Text(L10n.getString("track_leg_sidebar_data_pace"), textAlign: TextAlign.center))),
            ],
            rows: _buildTrackSplits(),
          )
        else
          Text(L10n.getString("track_leg_sidebar_no_track_selected")),
      ],
    );
  }

  List<DataRow> _buildTrackSplits() {
    List<GpsRoute> tracks = List.from(widget.presenter.selectedTracks);
    tracks.sort(((GpsRoute a, GpsRoute b) => a.model.calculateLegDuration(_legIndex).compareTo(b.model.calculateLegDuration(_legIndex))));
    final rows = <DataRow>[];
    for (var track in tracks) {
      final calculateLegDuration = track.model.calculateLegDuration(_legIndex);
      if (calculateLegDuration > 0) {
        final elevation = track.model.calculateLegElevationGain(_legIndex);
        String legElevationGain = elevation < -1000 ? "-" : "${elevation.toInt()}";
        rows.add(DataRow(
          cells: <DataCell>[
            DataCell(Text(track.model.nickname.substring(0, min(10, track.model.nickname.length)))),
            DataCell(Center(child: Text(timeToString(calculateLegDuration, withHour: false)))),
            DataCell(Center(child: Text("${track.model.calculateLegActualDistance(_legIndex).toInt()}"))),
            DataCell(Center(child: Text(legElevationGain))),
            DataCell(Center(child: Text(track.model.legSpeedToString(_legIndex)))),
            DataCell(Center(child: Text(track.model.legPaceToString(_legIndex, widget.presenter.courseData!.checkpoints)))),
          ],
        ));
      } else {
        rows.add(DataRow(
          cells: <DataCell>[
            DataCell(Text(track.model.nickname)),
            const DataCell(Center(child: Text("-"))),
            const DataCell(Center(child: Text("-"))),
            const DataCell(Center(child: Text("-"))),
            const DataCell(Center(child: Text("-"))),
            const DataCell(Center(child: Text("-"))),
          ],
        ));
      }
    }
    return rows;
  }

  void update(int legIndex) {
    setState(() {
      _legIndex = legIndex;
    });
  }
}
