// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/public_pages/routes/import/import_container.dart';
import 'package:vikazimut_website/public_pages/routes/route_list_page_presenter.dart';
import 'package:vikazimut_website/utils/alert_dialog.dart';

import 'import_presenter.dart';

class ImportTabView extends StatefulWidget {
  final RouteListPagePresenter presenter;

  const ImportTabView({
    required this.presenter,
  });

  @override
  ImportTabViewState createState() => ImportTabViewState();
}

class ImportTabViewState extends State<ImportTabView> {
  @override
  Widget build(BuildContext context) {
    return ImportContainer(
      presenter: ImportPresenter(
        presenter: widget.presenter,
        view: this,
      ),
    );
  }

  void displaySuccess() {
    showSuccessDialog(
      context,
      message: L10n.getString("import_gpx_success_message"),
    );
  }

  void displayError(String error) {
    showErrorDialog(
      context,
      title: L10n.getString("page_error_server_title"),
      message: error,
    );
  }
}
