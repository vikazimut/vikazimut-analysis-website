import 'authentication_response.dart';
import 'authentication_scopes.dart';
import 'strava_client.dart';

class Authentication {
  final StravaClient stravaClient;

  Authentication(this.stravaClient);

  Future<TokenResponse> authorize(
    List<AuthenticationScope> scopes,
    String redirectUrl, {
    required StravaClient client,
  }) {
    return stravaClient.authentication.authenticate(
      client: client,
      scopes: scopes,
      redirectUrl: redirectUrl,
      forceShowingApproval: false,
      callbackUrlScheme: "localhost",
    );
  }

  Future<void> deAuthorize() {
    return stravaClient.authentication.deAuthorize(client: stravaClient);
  }
}
