class Fault {
  final List<Error>? errors;
  final String? message;

  Fault({
    this.errors,
    this.message,
  });

  factory Fault.fromJson(Map<String, dynamic> json) => Fault(
        errors: List<Error>.from(json["errors"].map((x) => Error.fromJson(x))),
        message: json["message"],
      );
}

class Error {
  final String? code;
  final String? field;
  final String? resource;

  Error({
    this.code,
    this.field,
    this.resource,
  });

  factory Error.fromJson(Map<String, dynamic> json) => Error(
        code: json["code"],
        field: json["field"],
        resource: json["resource"],
      );
}
