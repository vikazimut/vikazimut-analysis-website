import '../connection/strava_client.dart';
import 'client.dart';
import 'model_detailed_athlete.dart';

/// Repository managing all API calls related to `athletes`.
class RepositoryAthlete {
  Future<DetailedAthlete> getAuthenticatedAthlete(StravaClient client,) {
    return ApiClient.getRequest<DetailedAthlete>(
      endPoint: "/v3/athlete",
      dataConstructor: (data) {
        return DetailedAthlete.fromJson(data);
      }, client: client,
    );
  }
}
