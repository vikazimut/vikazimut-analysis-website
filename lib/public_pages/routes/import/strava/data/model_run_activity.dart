class RunActivity {
  List<List<double>>? positions;
  List<int>? times;
  List<double>? altitudes;

  RunActivity({
    this.positions,
    this.times,
    this.altitudes,
  });

  factory RunActivity.fromJson(dynamic json) {
    final data = json['latlng']['data'];
    final List<List<double>> positions = [];
    if (data != null) {
      for (int i = 0; i < data.length; i++) {
        positions.add([data[i][0], data[i][1]]);
      }
    }
    final List<int> times = json['time']['data'] != null ? json['time']['data'].cast<int>() : [];
    final List<double>altitudes = json['altitude']['data'] != null ? json['altitude']['data'].cast<double>() : [];
    return RunActivity(positions: positions, times: times, altitudes: altitudes);
  }
}
