import '../connection/strava_client.dart';
import 'client.dart';
import 'model_summary_activity.dart';

class RepositoryActivity {
  Future<List<SummaryActivity>> listLoggedInAthleteActivities({
    required int page,
    required int perPage,
    required StravaClient client,
  }) {
    var queryParams = {
      "page": page,
      "per_page": perPage,
    };
    return ApiClient.getRequest(
        endPoint: "/v3/athlete/activities",
        queryParameters: queryParams,
        dataConstructor: (data) {
          if (data is List) {
            return data.map((d) => SummaryActivity.fromJson(Map<String, dynamic>.from(d))).toList();
          }
          return [];
        },
        client: client);
  }
}
