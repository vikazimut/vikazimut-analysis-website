import 'dart:async';
import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

import '../connection/strava_client.dart';

class ApiClient {
  static const _baseUrl = "https://www.strava.com/api";

  static Future<NetworkingHelper> _getClient({
    required StravaClient client,
  }) async {
    var networkingHelper = NetworkingHelper();
    var token = await client.sessionManager.getToken();
    var headers = <String, String>{};
    if (token != null) {
      headers.putIfAbsent("Authorization", () => "Bearer ${token.accessToken}");
    }
    networkingHelper.headers = headers;
    return Future.value(networkingHelper);
  }

  static Future<T> getRequest<T>({
    required String endPoint,
    Map<String, dynamic>? queryParameters,
    required T Function(dynamic) dataConstructor,
    required StravaClient client,
  }) async {
    var completer = Completer<T>();
    _getClient(client: client).then((NetworkingHelper networkingHelper) {
      networkingHelper
          .get("$_baseUrl$endPoint", queryParameters: queryParameters)
          .then(
            (response) => completer.complete(dataConstructor(jsonDecode(response.body))),
          )
          .catchError(
            (error, stackTrace) => handleError(completer, error, stackTrace),
          );
    });
    return completer.future;
  }

  static Future<T> postRequest<T>({
    required String endPoint,
    String? baseUrl,
    Map<String, dynamic>? queryParameters,
    dynamic postBody,
    required T Function(dynamic) dataConstructor,
    required StravaClient client,
  }) async {
    var completer = Completer<T>();
    _getClient(client: client).then((networkingHelper) {
      networkingHelper
          .post("${baseUrl ?? _baseUrl}$endPoint", queryParameters: queryParameters, data: postBody)
          .then(
            (response) => completer.complete(dataConstructor(jsonDecode(response.body))),
          )
          .catchError(
            (error, stackTrace) => handleError(completer, error, stackTrace),
          );
    });
    return completer.future;
  }

  static void handleError<T>(Completer<T> completer, dynamic error, StackTrace stackTrace) {
    completer.completeError(error, stackTrace);
  }
}

class NetworkingHelper {
  Map<String, String>? headers;

  @visibleForTesting
  static String buildUri(String url, Map<String, dynamic>? queryParameters) {
    var finalUrl = url;
    if (queryParameters != null) {
      String? parameters;
      queryParameters.forEach((key, value) {
        if (parameters == null) {
          parameters = "$key=$value";
        } else {
          parameters = "${parameters!}&$key=$value";
        }
      });
      finalUrl += "?$parameters";
    }
    return finalUrl;
  }

  Future<http.Response> get(String url, {Map<String, dynamic>? queryParameters}) {
    return http.get(
      Uri.parse(buildUri(url, queryParameters)),
      headers: headers,
    );
  }

  Future<http.Response> post(String url, {Map<String, dynamic>? queryParameters, required data}) {
    return http.post(
      Uri.parse(buildUri(url, queryParameters)),
      headers: headers,
      body: data,
    );
  }
}
