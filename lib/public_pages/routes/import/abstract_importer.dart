import 'package:flutter/material.dart';
import 'package:vikazimut_website/public_pages/model/geodesic_point.dart';

import 'import_presenter.dart';

abstract class AbstractImporter {
  Future<List<GeodesicPoint>?> getRouteWaypoints(BuildContext context, ImportPresenter presenter);
}
