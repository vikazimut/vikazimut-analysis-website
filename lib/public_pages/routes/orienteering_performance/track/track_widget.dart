// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_map_cancellable_tile_provider/flutter_map_cancellable_tile_provider.dart';
import 'package:vikazimut_website/common/flutter_map_layers/custom_map_tappable_polyline.dart';
import 'package:vikazimut_website/common/flutter_map_layers/custom_overlay_image_layer.dart';
import 'package:vikazimut_website/common/flutter_map_layers/scale_layer_widget.dart';
import 'package:vikazimut_website/constants.dart' as constants;
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/public_pages/courses/map_zoom.dart';
import 'package:vikazimut_website/public_pages/model/course_georeference.dart';
import 'package:vikazimut_website/public_pages/model/route_helper.dart';
import 'package:vikazimut_website/public_pages/routes/gps_route.dart';
import 'package:vikazimut_website/public_pages/routes/route_list_page_presenter.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/openstreetmap_contribution_widget.dart';
import 'package:vikazimut_website/utils/screen_helper.dart';

import '../my_tab_controller.dart';
import 'track_presenter.dart';

class TrackView extends StatefulWidget {
  final RouteListPagePresenter presenter;
  final Future<CourseData> courseInfo;
  final GpsRoute track;
  final MyTabController tabController;

  const TrackView({
    required this.presenter,
    required this.courseInfo,
    required this.track,
    required this.tabController,
  });

  @override
  State<TrackView> createState() => _TrackViewState();
}

class _TrackViewState extends State<TrackView> with TickerProviderStateMixin {
  late final TrackPresenter _trackPresenter;
  late final ScrollController _scrollController;
  bool _showedMap = true;
  MapZoomController? _mapZoomController;
  late Future<List<List<List<Object>>>> _routeLoader;

  @override
  void initState() {
    _trackPresenter = TrackPresenter();
    _mapZoomController = MapZoomController(this);
    _scrollController = ScrollController();
    _routeLoader = _trackPresenter.splitRouteIntoLegsAndAreas(
      widget.track,
      widget.track.model.punchTimes,
      widget.presenter.courseData!.checkpoints,
      widget.track.model.courseFormat,
    );
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    _mapZoomController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<dynamic>>(
      future: Future.wait([widget.courseInfo, _routeLoader]),
      builder: (context, snapshot) {
        if (snapshot.hasData && snapshot.data != null) {
          final CourseGeoreference courseGeoreference = snapshot.data![0].courseGeoreference;
          final List<RouteSegmentType> legRoutes = snapshot.data![1][0];
          final List<List<RouteSegmentType>> legAreaTracks = snapshot.data![1][1];
          final List<TaggedColoredPolyline> polylines = _trackPresenter.buildPolylineFromSelectedLeg(
            legRoutes,
            legAreaTracks,
            widget.tabController.currentLeg,
            widget.track.route,
            widget.track.color,
            widget.presenter.courseData!.courseGeoreference.discipline,
          );
          final LatLngBounds bounds = _trackPresenter.buildBounds(courseGeoreference);
          final overlayImages = _trackPresenter.loadOverlayImage(courseGeoreference, bounds, widget.presenter.user);
          _mapZoomController!.bounds = bounds;
          final renderBox = context.findRenderObject() as RenderBox;
          return SizedBox(
            height: double.infinity,
            child: Stack(
              alignment: Alignment.topRight,
              children: [
                _MapWithTracks(
                  mapController: _mapZoomController!.mapController,
                  bounds: bounds,
                  polylines: polylines,
                  startLatitude: courseGeoreference.startLatitude,
                  startLongitude: courseGeoreference.startLongitude,
                  showedMap: _showedMap,
                  overlayImages: overlayImages,
                ),
                Positioned(
                  left: constants.COLLAPSIBLE_MIN_WIDTH,
                  top: 0,
                  child: Container(
                    color: Theme.of(context).scaffoldBackgroundColor,
                    width: renderBox.size.width - constants.COLLAPSIBLE_MIN_WIDTH,
                    padding: const EdgeInsets.all(3),
                    child: Row(
                      children: [
                        Text(
                          L10n.getString("track_leg_toolbar_title"),
                          style: const TextStyle(
                            fontSize: 11,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        const SizedBox(width: 5),
                        Expanded(
                          child: Scrollbar(
                            interactive: true,
                            thumbVisibility: true,
                            thickness: 4,
                            scrollbarOrientation: ScrollbarOrientation.bottom,
                            controller: _scrollController,
                            child: SingleChildScrollView(
                              scrollDirection: Axis.horizontal,
                              controller: _scrollController,
                              padding: const EdgeInsets.only(bottom: 10),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  _RadioButton(
                                    value: 0,
                                    groupValue: widget.tabController.currentLeg,
                                    title: "*",
                                    onChanged: (value) {
                                      setState(() => widget.tabController.currentLeg = value!);
                                    },
                                  ),
                                  for (int i = 1; i < widget.presenter.courseData!.checkpoints.length; i++)
                                    _RadioButton(
                                      value: i,
                                      groupValue: widget.tabController.currentLeg,
                                      title: "$i",
                                      onChanged: (value) {
                                        setState(() => widget.tabController.currentLeg = value!);
                                      },
                                    ),
                                ],
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                Positioned(
                  right: 5.0,
                  top: 55.0,
                  child: Column(
                    children: [
                      if (ScreenHelper.isDesktop(context))
                        _MapButton(
                          iconData: Icons.add,
                          onPressed: () => _mapZoomController?.zoomIn(),
                        ),
                      if (ScreenHelper.isDesktop(context))
                        _MapButton(
                          iconData: Icons.remove,
                          onPressed: () => _mapZoomController?.zoomOut(),
                        ),
                      if (!ScreenHelper.isMobile(context))
                        _MapButton(
                          iconData: Icons.my_location,
                          onPressed: () => _mapZoomController?.reset(),
                        ),
                      _MapButton(
                        onPressed: () => setState(() => _showedMap = !_showedMap),
                        iconData: (_showedMap) ? Icons.layers_clear : Icons.layers,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        } else {
          return Container();
        }
      },
    );
  }
}

class _MapButton extends StatelessWidget {
  final VoidCallback? onPressed;
  final IconData iconData;

  const _MapButton({
    required this.onPressed,
    required this.iconData,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: kOrangeColorUltraLight,
        borderRadius: BorderRadius.circular(4),
        border: Border.all(width: 1, color: kCaptionColor),
      ),
      child: IconButton(
        icon: Icon(iconData, size: 24, color: Colors.black),
        onPressed: onPressed,
      ),
    );
  }
}

class _MapWithTracks extends StatefulWidget {
  final MapController mapController;
  final LatLngBounds bounds;
  final List<TaggedColoredPolyline> polylines;
  final double startLatitude;
  final double startLongitude;
  final bool showedMap;
  final List<CustomBaseOverlayImage> overlayImages;

  const _MapWithTracks({
    required this.mapController,
    required this.bounds,
    required this.polylines,
    required this.startLatitude,
    required this.startLongitude,
    required this.showedMap,
    required this.overlayImages,
  });

  @override
  State<_MapWithTracks> createState() => _MapWithTracksState();
}

class _MapWithTracksState extends State<_MapWithTracks> {
  @override
  Widget build(BuildContext context) {
    return FlutterMap(
      mapController: widget.mapController,
      options: MapOptions(
        maxZoom: 19,
        minZoom: 10,
        interactionOptions: InteractionOptions(
          cursorKeyboardRotationOptions: CursorKeyboardRotationOptions.disabled(),
          flags: InteractiveFlag.all & ~InteractiveFlag.rotate,
          scrollWheelVelocity: 0.002,
        ),
        initialCameraFit: CameraFit.bounds(bounds: widget.bounds, padding: const EdgeInsets.all(20)),
      ),
      children: [
        TileLayer(
          urlTemplate: 'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
          userAgentPackageName: 'fr.vikazim.vikazimut',
          tileProvider: CancellableNetworkTileProvider(),
        ),
        if (widget.showedMap) CustomOverlayImageLayer(overlayImages: widget.overlayImages),
        ScaleLayerWidget(
          options: ScaleLayerPluginOption(
            lineColor: Colors.blue,
            lineWidth: 2,
            textStyle: const TextStyle(color: Colors.blue, fontSize: 12),
          ),
        ),
        CustomTappablePolylineLayer(
          pointerDistanceTolerance: 15,
          polylines: widget.polylines,
          onTap: (selectedPolylines, selectedSegments, tapPosition) {},
        ),
        OpenStreetMapAttributionWidget(),
      ],
    );
  }
}

class _RadioButton extends StatelessWidget {
  final String title;
  final int value;
  final int groupValue;
  final ValueChanged<int?> onChanged;

  const _RadioButton({required this.title, required this.value, required this.groupValue, required this.onChanged});

  @override
  Widget build(BuildContext context) {
    final isSelected = value == groupValue;
    return SizedBox(
      width: 20,
      height: 30,
      child: InkWell(
        onTap: () {
          onChanged(value);
        },
        child: Container(
          alignment: Alignment.center,
          decoration: BoxDecoration(
            color: isSelected ? kOrangeColor : null,
            border: Border.all(
              color: kOrangeColorDisabled,
            ),
          ),
          child: Text(
            title,
            style: const TextStyle(
              fontSize: 10,
            ),
          ),
        ),
      ),
    );
  }
}
