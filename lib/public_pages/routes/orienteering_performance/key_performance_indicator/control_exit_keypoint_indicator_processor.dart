part of 'keypoint_indicator_processor.dart';

class ControlExitKeypointIndicatorProcessor extends ControlKeypointIndicatorProcessor {
  static const _distanceRemovedAfterValidationInM = 10;
  static const _timeRemovedAfterValidationInMs = 2 * 1000;

  @override
  String getRelatedControl(int control1, int control2) => "$control1";

  @override
  List<GeodesicPoint> cropRouteFromDistance(List<GeodesicPoint> points) {
    return cropRouteFromDistance_(points, _distanceRemovedAfterValidationInM);
  }

  @override
  List<GeodesicPoint> cropRouteFromTime(List<GeodesicPoint> points) {
    return cropRouteFromTime_(points, _timeRemovedAfterValidationInMs);
  }

  @visibleForTesting
  List<GeodesicPoint> cropRouteFromDistance_(List<GeodesicPoint> points, int maximumValidationDistanceInM) {
    var distance = 0.0;
    for (int i = 1; i < points.length; i++) {
      final point = points[i];
      final previousPoint = points[i - 1];
      final length = GeodesicPoint.distanceBetweenInMeter(previousPoint.latitude, previousPoint.longitude, point.latitude, point.longitude);
      distance += length;
      if (distance > maximumValidationDistanceInM) {
        var ratio = (maximumValidationDistanceInM - (distance - length)) / length;
        var time = point.timestampInMillisecond - previousPoint.timestampInMillisecond;
        final newPointTimestamp = (time * ratio).toInt() + previousPoint.timestampInMillisecond;
        final newPoint = RouteHelper.createPointAtGivenTime(previousPoint, point, newPointTimestamp);
        return points.sublist(i)..insert(0, newPoint);
      } else if (distance == maximumValidationDistanceInM) {
        return points.sublist(i);
      }
    }
    return points;
  }

  @visibleForTesting
  List<GeodesicPoint> cropRouteFromTime_(List<GeodesicPoint> points, int timeRemovedAfterValidationInS) {
    final firstPoint = points[0];
    for (int i = 1; i < points.length; i++) {
      final point = points[i];
      if (point.timestampInMillisecond - firstPoint.timestampInMillisecond > timeRemovedAfterValidationInS) {
        final previousPoint = points[i - 1];
        final newPointTimestamp = firstPoint.timestampInMillisecond + timeRemovedAfterValidationInS;
        final newPoint = RouteHelper.createPointAtGivenTime(previousPoint, point, newPointTimestamp);
        return points.sublist(i)..insert(0, newPoint);
      } else if (point.timestampInMillisecond - firstPoint.timestampInMillisecond == timeRemovedAfterValidationInS) {
        return points.sublist(i);
      }
    }
    return points;
  }

  @override
  double getWeightingSegmentLengthInM(List<GeodesicPoint> segments, int i) {
    return segments[i].distanceToInMeter(segments[i + 1]);
  }
}
