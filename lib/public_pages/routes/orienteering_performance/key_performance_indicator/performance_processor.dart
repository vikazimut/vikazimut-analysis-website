import 'package:flutter/foundation.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/public_pages/model/checkpoint.dart';
import 'package:vikazimut_website/public_pages/model/geodesic_point.dart';
import 'package:vikazimut_website/public_pages/model/punch_time.dart';
import 'package:vikazimut_website/public_pages/model/route_helper.dart';
import 'package:vikazimut_website/public_pages/model/track_model.dart';
import 'package:vikazimut_website/public_pages/routes/gps_route.dart';

import 'analysis_report.dart';
import 'area_score.dart';
import 'keypoint_indicator_processor.dart';

class PerformanceProcessor {
  final KeypointIndicatorProcessor _controlExitProcessor = ControlExitKeypointIndicatorProcessor();
  final KeypointIndicatorProcessor _controlAttackProcessor = ControlAttackKeypointIndicatorProcessor();
  final KeypointIndicatorProcessor _handrailProcessor = HandrailKeypointIndicatorProcessor();
  final PerformanceIndicators _orienteeringPerformances = PerformanceIndicators();

  PerformanceIndicators computeKeypointIndicators(
    GpsRoute track,
    List<Checkpoint> checkpoints,
  ) {
    final legs = track.model.legs;
    final List<List<GeodesicPoint>> legRoutes = [];
    for (int i = 1; i < legs.length; i++) {
      if (legs[i].punchTime >= 0) {
        legRoutes.add(legs[i].waypoints ?? []);
      }
    }
    List<List<List<GeodesicPoint>>> legAreaTracks = RouteHelper.splitLegIntoAreas(legRoutes);
    final (averageSpeedInHandrailAreasInKmh, averageSpeedInControlAttachAndExitAreasInKmh) = RouteHelper.computeAverageSpeedInAreas(legRoutes, legAreaTracks);
    _computeKPIForEachAreaOfEachLeg(checkpoints, track, legRoutes, legAreaTracks, averageSpeedInHandrailAreasInKmh, averageSpeedInControlAttachAndExitAreasInKmh);
    _orienteeringPerformances.totalScore = calculateGlobalScores_(_orienteeringPerformances.legPerformanceScores);
    _orienteeringPerformances.globalComments = _createGlobalComments();
    return _orienteeringPerformances;
  }

  void _computeKPIForEachAreaOfEachLeg(
    List<Checkpoint> checkpoints,
    GpsRoute track,
    List<List<GeodesicPoint>> legRoutes,
    List<List<List<GeodesicPoint>>> legAreaTracks,
    double averageSpeedInHandrailAreasInKmh,
    double averageSpeedInControlAttachAndExitAreasInKmh,
  ) {
    final List<Checkpoint> sortedCheckpoints = sortCheckpointsByPunchTimes_(checkpoints, track.model.punchTimes);
    for (int leg = 0; leg < legRoutes.length; leg++) {
      final controlExitTrack = legAreaTracks[leg][0];
      final log1 = <String, dynamic>{
        "leg": leg + 1,
        "control1": sortedCheckpoints[leg].id,
        "control2": sortedCheckpoints[leg + 1].id,
      };
      final AnalysisReport report1 = controlExitTrack.isEmpty
          ? AnalysisReport()
          : _controlExitProcessor.computeScoreAndReport(
              controlExitTrack,
              controlExitTrack.first,
              leg,
              sortedCheckpoints[leg].index,
              sortedCheckpoints[leg + 1].index,
              averageSpeedInHandrailAreasInKmh,
              log1,
            );
      final handrailTrack = legAreaTracks[leg][1];
      final log2 = <String, dynamic>{"leg": leg + 1};
      final AnalysisReport report2 = handrailTrack.isEmpty
          ? AnalysisReport()
          : _handrailProcessor.computeScoreAndReport(
              handrailTrack,
              handrailTrack.last,
              leg,
              sortedCheckpoints[leg].index,
              sortedCheckpoints[leg + 1].index,
              averageSpeedInControlAttachAndExitAreasInKmh,
              log2,
            );
      final controlAttackTrack = legAreaTracks[leg][2];
      final log3 = <String, dynamic>{"leg": leg + 1};
      final AnalysisReport report3 = controlAttackTrack.isEmpty
          ? AnalysisReport()
          : _controlAttackProcessor.computeScoreAndReport(
              controlAttackTrack,
              controlAttackTrack.last,
              leg,
              sortedCheckpoints[leg].index,
              sortedCheckpoints[leg + 1].index,
              averageSpeedInHandrailAreasInKmh,
              log3,
            );
      AreaScore legScore = AreaScore(
        controlExitReport: report1,
        handrailReport: report2,
        controlAttackReport: report3,
        control1: sortedCheckpoints[leg],
        control2: sortedCheckpoints[leg + 1],
        controlExitLog: log1,
        handrailLog: log2,
        controlAttackLog: log3,
      );
      _orienteeringPerformances.legPerformanceScores.add(legScore);
    }
  }

  @visibleForTesting
  static AreaScore calculateGlobalScores_(
    List<AreaScore> legPerformanceScores,
  ) {
    double score1 = 0;
    int count1 = 0;
    Comment comments1 = Comment();
    double score2 = 0;
    int count2 = 0;
    Comment comments2 = Comment();
    double score3 = 0;
    int count3 = 0;
    Comment comments3 = Comment();
    for (int i = 0; i < legPerformanceScores.length; i++) {
      final legPerformanceScore = legPerformanceScores[i];
      if (legPerformanceScore.controlExitReport.score >= 0) {
        count1 += 1;
        score1 += legPerformanceScore.controlExitReport.score;
      }
      if (legPerformanceScore.controlExitReport.comments != null) {
        comments1.add(legPerformanceScore.controlExitReport.comments!);
      }

      if (legPerformanceScore.handrailReport.score >= 0) {
        count2 += 1;
        score2 += legPerformanceScore.handrailReport.score;
      }
      if (legPerformanceScore.handrailReport.comments != null) {
        comments2.add(legPerformanceScore.handrailReport.comments!);
      }

      if (legPerformanceScore.controlAttackReport.score >= 0) {
        count3 += 1;
        score3 += legPerformanceScore.controlAttackReport.score;
      }
      if (legPerformanceScore.controlAttackReport.comments != null) {
        comments3.add(legPerformanceScore.controlAttackReport.comments!);
      }
    }
    return AreaScore(
      controlExitReport: AnalysisReport(
        score: count1 > 0 ? score1 / count1 : 0,
        comments: comments1,
      ),
      handrailReport: AnalysisReport(
        score: count2 > 0 ? score2 / count2 : 0,
        comments: comments2,
      ),
      controlAttackReport: AnalysisReport(
        score: count3 > 0 ? score3 / count3 : 0,
        comments: comments3,
      ),
    );
  }

  @visibleForTesting
  static List<Checkpoint> sortCheckpointsByPunchTimes_(
    List<Checkpoint> checkpoints,
    List<PunchTime> punchTimes,
  ) {
    final List<Checkpoint> sortedCheckpoints = [];
    final List<int> sortCheckpointsByPunchTimes = TrackModel.sortCheckpointsByPunchTimes(punchTimes);
    for (int i = 0; i < punchTimes.length; i++) {
      if (punchTimes[sortCheckpointsByPunchTimes[i]].timestampInMillisecond >= 0) {
        sortedCheckpoints.add(checkpoints[sortCheckpointsByPunchTimes[i]]);
      }
    }
    return sortedCheckpoints;
  }

  List<String> _createGlobalComments() {
    final globalComments = <String>[];
    if (_orienteeringPerformances.totalScore.controlExitReport.score <= 3) {
      globalComments.add(L10n.getString("global_performance_general_commentary_1"));
    }

    if (_orienteeringPerformances.totalScore.handrailReport.score <= 3) {
      globalComments.add(L10n.getString("global_performance_general_commentary_2"));
    }

    if (_orienteeringPerformances.totalScore.controlAttackReport.score <= 3) {
      globalComments.add(L10n.getString("global_performance_general_commentary_3"));
    }

    if (((_orienteeringPerformances.totalScore.controlExitReport.score - _orienteeringPerformances.totalScore.handrailReport.score).abs() <= 1) //
        &&
        ((_orienteeringPerformances.totalScore.handrailReport.score - _orienteeringPerformances.totalScore.controlAttackReport.score).abs() <= 1) //
        &&
        ((_orienteeringPerformances.totalScore.controlExitReport.score - _orienteeringPerformances.totalScore.controlAttackReport.score).abs() <= 1)) {
      globalComments.add(L10n.getString("global_performance_general_commentary_4"));
    }
    return globalComments;
  }
}

class PerformanceIndicators {
  final List<AreaScore> legPerformanceScores = [];
  late final List<String> globalComments;
  late final AreaScore totalScore;
}
