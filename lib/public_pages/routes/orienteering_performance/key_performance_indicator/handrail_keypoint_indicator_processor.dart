part of 'keypoint_indicator_processor.dart';

class HandrailKeypointIndicatorProcessor extends KeypointIndicatorProcessor {
  static const double _minSpeedForMovementInKmh = 2;
  static const int _stopDurationInS = 5;
  static const int _durationBetweenStopInS = 5;

  @override
  AnalysisReport computeScoreAndReport(
    List<GeodesicPoint> trackZone,
    GeodesicPoint areaCenterLocation,
    int legIndex,
    int control1,
    int control2,
    double averageSpeedInKmh,
    Map<String, dynamic> log,
  ) {
    if (trackZone.length < 2) {
      return AnalysisReport(score: 10);
    }

    final Comment comments = Comment();

    // 1. Detect U-turns and loops
    final hasUTurn = KeypointIndicatorProcessor.detectUTurn(trackZone);
    if (hasUTurn) {
      comments.uTurn.add(LegComment(legIndex, "$control1-$control2"));
    }
    log["Présence demi-tours"] = hasUTurn;
    List<double> speedsEffortInKmh = RouteHelper.computeInstantaneousEffortSpeedsInKmh(trackZone);

    // 2. Count the number of stops
    final stopCount = countStops_(speedsEffortInKmh, trackZone, _minSpeedForMovementInKmh, _stopDurationInS, _durationBetweenStopInS);
    if (stopCount > 2) {
      comments.stop.add(LegComment(legIndex, "$control1-$control2"));
    }
    // TODO L10n
    log["Nombre d'arrêts"] = stopCount;

    // 3. Area average speed versus control exit and attack average speed
    final zoneAverageSpeedInKmh = RouteHelper.computeAverageSpeedEffortInKmh(trackZone);
    final averageSpeedScore = calculateAverageSpeedScore_(zoneAverageSpeedInKmh, averageSpeedInKmh);
    if (averageSpeedScore < 5) {
      comments.speed.add(LegComment(legIndex, "$control1-$control2"));
    }
    log["Vitesse moyenne en zone"] = zoneAverageSpeedInKmh.toStringAsFixed(2);
    log["Vitesse moyenne hors zone"] = averageSpeedInKmh.toStringAsFixed(2);
    log["Score vitesse-effort moyenne"] = averageSpeedScore.toStringAsFixed(2);

    // 4. Compute distance speed curve to adjustment curve
    final speedStandardDeviation = calculateSpeedStandardDeviation_(speedsEffortInKmh);
    log["Écart-type de la vitesse-effort"] = speedStandardDeviation.toStringAsFixed(2);
    double speedVariationScore = calculateEffortDiscrepancyScore_(speedStandardDeviation);
    log["Score variation de vitesse-effort"] = speedVariationScore.toStringAsFixed(2);

    // 5. Final score
    final double score;
    if (hasUTurn) {
      score = 0;
    } else {
      final int quotient;
      if (stopCount > 2) {
        score = 0;
      } else {
        if (stopCount == 0) {
          quotient = 1;
        } else if (stopCount == 1) {
          quotient = 2;
        } else {
          quotient = 4;
        }
        double totalScore = (averageSpeedScore + speedVariationScore) / (2 * quotient);
        score = totalScore.clamp(0.0, 10.0);
      }
    }
    return AnalysisReport(score: score, comments: comments);
  }

  @visibleForTesting
  static double calculateAverageSpeedScore_(
    double zoneAverageSpeed,
    double routeAverageSpeed,
  ) {
    final double speedScore;
    if (zoneAverageSpeed >= routeAverageSpeed) {
      speedScore = 10;
    } else {
      speedScore = 10 * (1 - (routeAverageSpeed - zoneAverageSpeed) / routeAverageSpeed);
    }
    return speedScore.clamp(0.0, 10.0);
  }

  @visibleForTesting
  static int countStops_(
    List<double> speedsInKmh,
    List<GeodesicPoint> points,
    double minSpeed,
    int stopDurationInS,
    int durationBetweenStopInS,
  ) {
    if (points.isEmpty) {
      return 0;
    }
    int count = 0;
    int stopStart = -1;
    int moveStart = -1;
    for (int i = 0; i < speedsInKmh.length; i++) {
      if (speedsInKmh[i] <= minSpeed) {
        if (stopStart < 0) {
          bool isEnoughDurationBetweenStops;
          if (moveStart < 0) {
            isEnoughDurationBetweenStops = true;
          } else {
            final duration = points[i].timestampInMillisecond - moveStart;
            isEnoughDurationBetweenStops = duration > durationBetweenStopInS * 1000;
          }
          if (isEnoughDurationBetweenStops) {
            stopStart = points[i].timestampInMillisecond;
            moveStart = -1;
          }
        }
      } else {
        if (stopStart >= 0) {
          final duration = points[i].timestampInMillisecond - stopStart;
          if (duration > stopDurationInS * 1000) {
            count++;
          }
        }
        stopStart = -1;
        if (moveStart < 0) {
          moveStart = points[i].timestampInMillisecond;
        }
      }
    }
    if (stopStart >= 0) {
      final duration = points.last.timestampInMillisecond - stopStart;
      if (duration > stopDurationInS * 1000) {
        count++;
      }
    }
    return count;
  }

  @visibleForTesting
  static double calculateSpeedStandardDeviation_(List<double> speedsEffortInKmh) {
    var n = 0;
    var sum = 0.0;
    var sumSquare = 0.0;
    for (int i = 0; i < speedsEffortInKmh.length; i++) {
      final x = speedsEffortInKmh[i] - 10.0; // Variance is invariant by translation
      n = n + 1;
      sum = sum + x;
      sumSquare = sumSquare + x * x;
    }
    if (n == 0) {
      return 0;
    }
    double variance = (sumSquare - (sum * sum) / n) / n;
    return math.sqrt(variance);
  }

  @visibleForTesting
  static double calculateEffortDiscrepancyScore_(double standardDeviation) {
    const int minSpeedStandardDeviationInKmh = 2;
    const int maxAcceptableSpeedStandardDeviation = 10;

    final d = standardDeviation - minSpeedStandardDeviationInKmh;
    final x = maxAcceptableSpeedStandardDeviation - d;
    return x.clamp(0, 10);
  }
}
