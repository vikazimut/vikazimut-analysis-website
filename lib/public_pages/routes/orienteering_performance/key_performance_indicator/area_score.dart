import 'package:vikazimut_website/public_pages/model/checkpoint.dart';

import 'analysis_report.dart';

class AreaScore {
  final AnalysisReport controlExitReport;
  final AnalysisReport handrailReport;
  final AnalysisReport controlAttackReport;
  final Checkpoint? control1;
  final Checkpoint? control2;
  final Map<String, dynamic>? controlExitLog;
  final Map<String, dynamic>? handrailLog;
  final Map<String, dynamic>? controlAttackLog;

  AreaScore({
    required this.controlExitReport,
    required this.handrailReport,
    required this.controlAttackReport,
    this.control1,
    this.control2,
    this.controlExitLog,
    this.handrailLog,
    this.controlAttackLog,
  });

  String legName() {
    return "${control1!.index} - ${control2!.index}\n(${control1!.id}) - (${control2!.id})";
  }

  double computeAverage() {
    return (controlExitReport.score + handrailReport.score + controlAttackReport.score) / 3;
  }
}
