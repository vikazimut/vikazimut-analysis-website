library;

import 'dart:math' as math;

import 'package:flutter/foundation.dart';
import 'package:vikazimut_website/public_pages/model/geodesic_point.dart';
import 'package:vikazimut_website/public_pages/model/route_helper.dart';

import 'analysis_report.dart';
import 'douglas_peucker_algorithm.dart';
import 'vector.dart';

part 'control_attack_keypoint_indicator_processor.dart';
part 'control_exit_keypoint_indicator_processor.dart';
part 'control_keypoint_indicator_processor.dart';
part 'handrail_keypoint_indicator_processor.dart';

abstract class KeypointIndicatorProcessor {
  static const double _toleranceMeters = 10.0;
  static const int _toleranceTimeInS = 15;

  AnalysisReport computeScoreAndReport(
    List<GeodesicPoint> trackZone,
    GeodesicPoint areaCenterLocation,
    int legIndex,
    int control1,
    int control2,
    double averageSpeedInKmh,
    Map<String, dynamic> log,
  );

  @visibleForTesting
  static List<double> calculateAccelerationsInKmh2FromFiniteDifferences_(List<double> speedsInKmh) {
    List<double> accelerationInKmh2 = [];
    for (int i = 0; i < speedsInKmh.length - 1; i++) {
      final finiteDifferenceInKmh = speedsInKmh[i + 1] - speedsInKmh[i];
      accelerationInKmh2.add(finiteDifferenceInKmh);
    }
    return accelerationInKmh2;
  }

  @visibleForTesting
  static double calculateSpeedVariationScore_(
    List<double> accelerationInKmh2,
    double averageSpeedInHandrailAreasInKmh,
    Map<String, dynamic> log,
  ) {
    if (accelerationInKmh2.isEmpty) {
      return 10;
    }
    double totalDecelerationInKmh2 = 0;
    for (int i = 0; i < accelerationInKmh2.length; i++) {
      final acceleration = accelerationInKmh2[i];
      if (acceleration < 0) {
        totalDecelerationInKmh2 += acceleration * acceleration;
      }
    }
    log["Accélération"] = math.sqrt(totalDecelerationInKmh2).toStringAsFixed(2);
    log["Vitesse moyenne"] = averageSpeedInHandrailAreasInKmh.toStringAsFixed(2);
    final double criteria;
    if (averageSpeedInHandrailAreasInKmh > 0) {
      criteria = math.sqrt(totalDecelerationInKmh2 / (averageSpeedInHandrailAreasInKmh * averageSpeedInHandrailAreasInKmh));
    } else {
      criteria = 0.0;
    }
    double score = 10 * (1 - criteria);
    return score.clamp(0.0, 10.0);
  }

  @visibleForTesting
  static int calculateStopDurationInMs_(
    List<double> speedsInKmh,
    List<GeodesicPoint> points,
    double minSpeed,
  ) {
    int totalTimeInMs = 0;
    for (int i = 0; i < speedsInKmh.length; i++) {
      if (speedsInKmh[i] <= minSpeed) {
        totalTimeInMs += points[i + 1].timestampInMillisecond - points[i].timestampInMillisecond;
      }
    }
    return totalTimeInMs;
  }

  static bool detectUTurn(List<GeodesicPoint> route) => detectUTurn_(route, _toleranceMeters, _toleranceTimeInS);

  @visibleForTesting
  static bool detectUTurn_(
    List<GeodesicPoint> route,
    double distanceToleranceInM,
    int timeToleranceInS,
  ) {
    for (int i = 0; i < route.length; i++) {
      var coveredDistance = 0.0;
      for (int j = i + 1; j < route.length; j++) {
        final point1 = route[j - 1];
        final point2 = route[j];
        final distance1 = GeodesicPoint.distanceBetweenInMeter(
          point1.latitude,
          point1.longitude,
          point2.latitude,
          point2.longitude,
        );
        coveredDistance += distance1;
        if (coveredDistance > distanceToleranceInM * 2) {
          final point0 = route[i];
          final distance = GeodesicPoint.distanceBetweenInMeter(
            point0.latitude,
            point0.longitude,
            point2.latitude,
            point2.longitude,
          );
          final time = point2.timestampInMillisecond - point0.timestampInMillisecond;
          if (distance < distanceToleranceInM && //
              time > timeToleranceInS * 1000) {
            return true;
          }
        }
      }
    }
    return false;
  }
}
