import 'package:vikazimut_website/public_pages/routes/gps_route.dart';
import 'package:vikazimut_website/public_pages/routes/orienteering_performance/key_performance_indicator/performance_processor.dart';

class CsvWriter {
  static String convert(
    List<GpsRoute> selectedTracks,
    List<PerformanceIndicators> performanceData,
  ) {
    if (performanceData.isEmpty) {
      return "";
    }
    String csvContents = "";
    for (int track = 0; track < performanceData.length; track++) {
      csvContents += "${selectedTracks[track].nickname}\n";
      csvContents += "Sortie de poste;;;;;;;;;;Main courante;;;;;;;;Attaque de poste\n";
      {
        final legPerformanceScores = performanceData[track].legPerformanceScores;
        for (var item in legPerformanceScores[track].controlExitLog!.keys) {
          csvContents += "$item;";
        }
        for (var item in legPerformanceScores[track].handrailLog!.keys) {
          csvContents += "$item;";
        }
        for (var item in legPerformanceScores[track].controlAttackLog!.keys) {
          csvContents += "$item;";
        }
      }
      csvContents += "\n";
      {
        final legPerformanceScores = performanceData[track].legPerformanceScores;
        for (int leg = 0; leg < legPerformanceScores.length; leg++) {
          if (legPerformanceScores[leg].controlExitLog!.values.length < 4) {
            csvContents += "${legPerformanceScores[leg].controlExitLog!.values.elementAt(0)};";
            csvContents += "${legPerformanceScores[leg].controlExitLog!.values.elementAt(1)};";
            csvContents += "${legPerformanceScores[leg].controlExitLog!.values.elementAt(2)};-;-;-;-;-;-;-;";
          } else {
            for (var item in legPerformanceScores[leg].controlExitLog!.values) {
              csvContents += "$item;";
            }
          }
          if (legPerformanceScores[leg].handrailLog!.values.length < 4) {
            csvContents += "${legPerformanceScores[leg].controlExitLog!.values.first};-;-;-;-;-;-;-;";
          } else {
            for (var item in legPerformanceScores[leg].handrailLog!.values) {
              csvContents += "$item;";
            }
          }
          if (legPerformanceScores[leg].controlAttackLog!.values.length < 4) {
            csvContents += "${legPerformanceScores[leg].controlAttackLog!.values.first};-;-;-;-;-;-;-;";
          } else {
            for (var item in legPerformanceScores[leg].controlAttackLog!.values) {
              csvContents += "$item;";
            }
          }
          csvContents += "\n";
        }
      }
    }
    return csvContents;
  }
}
