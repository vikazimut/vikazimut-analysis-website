// coverage:ignore-file
// ignore: implementation_imports
import 'package:community_charts_common/src/chart/common/behavior/legend/legend.dart';
import 'package:community_charts_flutter/community_charts_flutter.dart' as charts;
import 'package:flutter/material.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/custom_button.dart';

import 'key_performance_indicator/area_score.dart';
import 'my_tab_controller.dart';

class DetailedPerformanceView extends StatelessWidget {
  final List<AreaScore> performances;
  final ScrollController _scrollController = ScrollController();
  final _ButtonsState _buttonsState = _ButtonsState();
  final MyTabController tabController;

  DetailedPerformanceView({
    required this.performances,
    required this.tabController,
  });

  @override
  Widget build(BuildContext context) {
    final bool isDarkMode = AppTheme.isDarkMode(context);
    return Column(
      children: [
        Scrollbar(
          controller: _scrollController,
          thumbVisibility: true,
          trackVisibility: true,
          child: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            controller: _scrollController,
            child: ListenableBuilder(
              listenable: _buttonsState,
              builder: (context, child) {
                return Container(
                  width: performances.length * 70.0,
                  height: 400,
                  constraints: const BoxConstraints(minWidth: 700),
                  padding: const EdgeInsets.only(top: 16, bottom: 10),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Expanded(
                        child: charts.BarChart(
                          _createData(),
                          animate: false,
                          rtlSpec: const charts.RTLSpec(axisDirection: charts.AxisDirection.reversed),
                          defaultInteractions: false,
                          domainAxis: charts.OrdinalAxisSpec(
                            renderSpec: charts.SmallTickRendererSpec(
                              labelStyle: charts.TextStyleSpec(
                                fontSize: 10,
                                color: isDarkMode ? charts.MaterialPalette.white.darker : null,
                              ),
                            ),
                          ),
                          behaviors: [
                            charts.SeriesLegend.customLayout(
                              _CustomTopLegendBuilder(_buttonsState),
                              position: charts.BehaviorPosition.top,
                              outsideJustification: charts.OutsideJustification.start,
                            ),
                            charts.SelectNearest(eventTrigger: charts.SelectionTrigger.tapAndDrag),
                          ],
                          selectionModels: [
                            charts.SelectionModelConfig(
                              type: charts.SelectionModelType.info,
                              changedListener: (charts.SelectionModel model) {
                                if (model.hasDatumSelection) {
                                  final charts.SeriesDatum first = model.selectedDatum.first;
                                  final int legIndex = first.datum.legIndex + 1;
                                  tabController.openTabWithLeg(legIndex);
                                }
                              },
                            ),
                          ],
                          primaryMeasureAxis: charts.NumericAxisSpec(
                            tickProviderSpec: charts.BasicNumericTickProviderSpec(desiredTickCount: 20),
                            renderSpec: charts.GridlineRendererSpec(
                              labelStyle: charts.TextStyleSpec(
                                fontSize: 18,
                                color: isDarkMode ? charts.MaterialPalette.white.darker : null,
                              ),
                              lineStyle: charts.LineStyleSpec(
                                color: isDarkMode ? charts.MaterialPalette.white.darker : null,
                              ),
                            ),
                            viewport: charts.NumericExtents(0, 10.0),
                            showAxisLine: true,
                          ),
                        ),
                      ),
                      Text(L10n.getString("global_performance_split_legend")),
                    ],
                  ),
                );
              },
            ),
          ),
        ),
      ],
    );
  }

  List<charts.Series<_PerformanceGrade, String>> _createData() {
    int maxSize = performances.length;
    final List<_PerformanceGrade> controlExitGrades;
    if (_buttonsState.get(0)) {
      controlExitGrades = List.generate(maxSize, (index) => _PerformanceGrade(index, performances[index].legName(), performances[index].controlExitReport.score), growable: false);
    } else {
      controlExitGrades = List.generate(maxSize, (index) => _PerformanceGrade(index, performances[index].legName(), 0), growable: false);
    }
    final List<_PerformanceGrade> handrailGrades;
    if (_buttonsState.get(1)) {
      handrailGrades = List.generate(maxSize, (index) => _PerformanceGrade(index, performances[index].legName(), performances[index].handrailReport.score), growable: false);
    } else {
      handrailGrades = List.generate(maxSize, (index) => _PerformanceGrade(index, performances[index].legName(), 0), growable: false);
    }
    final List<_PerformanceGrade> listControlAttackGrades;
    if (_buttonsState.get(2)) {
      listControlAttackGrades = List.generate(maxSize, (index) => _PerformanceGrade(index, performances[index].legName(), performances[index].controlAttackReport.score), growable: false);
    } else {
      listControlAttackGrades = List.generate(maxSize, (index) => _PerformanceGrade(index, performances[index].legName(), 0), growable: false);
    }
    return [
      charts.Series<_PerformanceGrade, String>(
        id: L10n.getString("global_performance_tab1_title"),
        domainFn: (_PerformanceGrade performance, _) => performance.grade < 0 ? "[${performance.leg}]" : performance.leg,
        measureFn: (_PerformanceGrade performance, _) => performance.grade < 0 ? null : performance.gradeForDisplay,
        data: controlExitGrades,
        fillColorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
      ),
      charts.Series<_PerformanceGrade, String>(
        id: L10n.getString("global_performance_tab2_title"),
        domainFn: (_PerformanceGrade performance, _) => performance.grade < 0 ? "[${performance.leg}]" : performance.leg,
        measureFn: (_PerformanceGrade performance, _) => performance.grade < 0 ? null : performance.gradeForDisplay,
        data: handrailGrades,
        fillColorFn: (_, __) => charts.MaterialPalette.lime.shadeDefault,
      ),
      charts.Series<_PerformanceGrade, String>(
        id: L10n.getString("global_performance_tab3_title"),
        domainFn: (_PerformanceGrade performance, _) => performance.grade < 0 ? "[${performance.leg}]" : performance.leg,
        measureFn: (_PerformanceGrade performance, _) => performance.grade < 0 ? null : performance.gradeForDisplay,
        data: listControlAttackGrades,
        fillColorFn: (_, __) => charts.Color(r: 0xFF, g: 0x00, b: 0xFF),
      ),
    ];
  }
}

class _CustomTopLegendBuilder extends charts.LegendContentBuilder {
  _ButtonsState buttonsState;

  _CustomTopLegendBuilder(this.buttonsState);

  @override
  Widget build(
    BuildContext context,
    LegendState<dynamic> legendState,
    Legend<dynamic> legend, {
    bool? showMeasures,
  }) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: legend.legendState.legendEntries
          .asMap()
          .map<int, Widget>(
            (index, legendEntry) {
              var color = legendEntry.series.fillColorFn!(0)!;
              return MapEntry(
                index,
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                  child: CustomPlainButton(
                    label: legendEntry.label,
                    backgroundColor: buttonsState.get(index) ? Color.fromARGB(color.a, color.r, color.g, color.b) : Theme.of(context).disabledColor,
                    onPressed: () {
                      buttonsState.change(index);
                    },
                  ),
                ),
              );
            },
          )
          .values
          .toList(),
    );
  }
}

class _PerformanceGrade {
  final int legIndex;
  final String leg;
  final double grade;

  _PerformanceGrade(this.legIndex, this.leg, this.grade);

  double get gradeForDisplay => (grade == 0) ? grade + 0.1 : grade;
}

class _ButtonsState extends ChangeNotifier {
  final List<bool> _buttonsEnabled = [true, true, true];

  bool get(int index) => _buttonsEnabled[index];

  void change(int index) {
    _buttonsEnabled[index] = !_buttonsEnabled[index];
    notifyListeners();
  }
}
