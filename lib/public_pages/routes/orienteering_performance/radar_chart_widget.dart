// coverage:ignore-file
import 'dart:math' as math;
import 'dart:math' show pi, cos, sin;

import 'package:flutter/material.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/screen_helper.dart';

import 'key_performance_indicator/area_score.dart';

class RadarChartWidget extends StatelessWidget {
  final AreaScore featureValues;

  const RadarChartWidget({
    super.key,
    required this.featureValues,
  });

  @override
  Widget build(BuildContext context) {
    final bool isDarkMode = AppTheme.isDarkMode(context);
    return _RadarChart(
      featuresTextStyle: TextStyle(
        color: isDarkMode ? Colors.white : Colors.black,
        fontSize: (ScreenHelper.isMobile(context)) ? 10 : 20,
      ),
      outlineColor: isDarkMode ? Colors.white : Colors.black,
      axisColor: Colors.grey,
      sides: 3,
      ticks: const [2, 4, 6, 8, 10],
      graphColors: const [
        kOrangeColor,
      ],
      features: [
        L10n.getString("global_performance_tab2_title"),
        L10n.getString("global_performance_tab3_title"),
        L10n.getString("global_performance_tab1_title"),
      ],
      data: [
        [
          featureValues.handrailReport.score,
          featureValues.controlAttackReport.score,
          featureValues.controlExitReport.score,
        ]
      ],
    );
  }
}

const _defaultGraphColors = [
  Colors.green,
  Colors.blue,
  Colors.red,
  Colors.orange,
];

class _RadarChart extends StatefulWidget {
  static const TextStyle ticksTextStyle = TextStyle(color: Colors.grey, fontSize: 12);
  final List<int> ticks;
  final List<String> features;
  final List<List<num>> data;
  final TextStyle featuresTextStyle;
  final Color outlineColor;
  final Color axisColor;
  final List<Color> graphColors;
  final int sides;

  const _RadarChart({
    required this.ticks,
    required this.features,
    required this.data,
    this.featuresTextStyle = const TextStyle(color: Colors.black, fontSize: 16),
    this.outlineColor = Colors.black,
    this.axisColor = Colors.grey,
    this.graphColors = _defaultGraphColors,
    this.sides = 0,
  });

  @override
  _RadarChartState createState() => _RadarChartState();
}

class _RadarChartState extends State<_RadarChart> with SingleTickerProviderStateMixin {
  double fraction = 0;
  late Animation<double> animation;
  late AnimationController animationController;

  @override
  void initState() {
    super.initState();
    animationController = AnimationController(duration: Duration(milliseconds: 1000), vsync: this);

    animation = Tween(begin: 0.0, end: 1.0).animate(CurvedAnimation(
      curve: Curves.fastOutSlowIn,
      parent: animationController,
    ))
      ..addListener(() {
        setState(() {
          fraction = animation.value;
        });
      });

    animationController.forward();
  }

  @override
  void didUpdateWidget(_RadarChart oldWidget) {
    super.didUpdateWidget(oldWidget);

    animationController.reset();
    animationController.forward();
  }

  @override
  Widget build(BuildContext context) {
    return CustomPaint(
      size: Size(double.infinity, double.infinity),
      painter: RadarChartPainter(
        widget.ticks,
        widget.features,
        widget.data,
        widget.featuresTextStyle,
        widget.outlineColor,
        widget.axisColor,
        widget.graphColors,
        widget.sides,
        fraction,
      ),
    );
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }
}

class RadarChartPainter extends CustomPainter {
  final List<int> ticks;
  final List<String> features;
  final List<List<num>> data;
  final TextStyle featuresTextStyle;
  final Color outlineColor;
  final Color axisColor;
  final List<Color> graphColors;
  final int sides;
  final double fraction;

  RadarChartPainter(
    this.ticks,
    this.features,
    this.data,
    this.featuresTextStyle,
    this.outlineColor,
    this.axisColor,
    this.graphColors,
    this.sides,
    this.fraction,
  );

  Path variablePath(Size size, double radius, int sides) {
    final path = Path();
    final angle = (math.pi * 2) / sides;

    Offset center = Offset(size.width / 2, size.height / 2);

    if (sides < 3) {
      // Draw a circle
      path.addOval(Rect.fromCircle(
        center: Offset(size.width / 2, size.height / 2),
        radius: radius,
      ));
    } else {
      // Draw a polygon
      Offset startPoint = Offset(radius * cos(-pi / 2), radius * sin(-pi / 2));

      path.moveTo(startPoint.dx + center.dx, startPoint.dy + center.dy);

      for (int i = 1; i <= sides; i++) {
        double x = radius * cos(angle * i - pi / 2) + center.dx;
        double y = radius * sin(angle * i - pi / 2) + center.dy;
        path.lineTo(x, y);
      }
      path.close();
    }
    return path;
  }

  @override
  void paint(Canvas canvas, Size size) {
    // Tips to occupy the maximum widget content
    Size size1 = Size(size.width, size.height + 50);
    final centerX = size1.width / 2.0;
    final centerY = size1.height / 2.0;
    final centerOffset = Offset(centerX, centerY);
    final radius = math.min(centerX, centerY) * 0.8;
    final scale = radius / ticks.last;

    // Painting the chart outline
    final outlinePaint = Paint()
      ..color = outlineColor
      ..style = PaintingStyle.stroke
      ..strokeWidth = 2.0
      ..isAntiAlias = true;

    final ticksPaint = Paint()
      ..color = axisColor
      ..style = PaintingStyle.stroke
      ..strokeWidth = 1.0
      ..isAntiAlias = true;

    canvas.drawPath(variablePath(size1, radius, sides), outlinePaint);
    // Painting the circles and labels for the given ticks (could be auto-generated)
    // The last tick is ignored, since it overlaps with the feature label
    final tickDistance = radius / (ticks.length);
    final tickLabels = ticks;

    tickLabels.sublist(0, ticks.length - 1).asMap().forEach((index, tick) {
      final tickRadius = tickDistance * (index + 1);
      canvas.drawPath(variablePath(size1, tickRadius, sides), ticksPaint);
      final TextStyle ticksLabelTextStyle = TextStyle(color: featuresTextStyle.color, fontSize: 14);
      TextPainter(
        text: TextSpan(
          text: tick.toString(),
          style: ticksLabelTextStyle,
        ),
        textDirection: TextDirection.ltr,
      )
        ..layout(minWidth: 0, maxWidth: size1.width)
        ..paint(canvas, Offset(centerX, centerY - tickRadius - _RadarChart.ticksTextStyle.fontSize!));
    });

    // Painting the axis for each given feature
    final angle = (2 * pi) / features.length;
    features.asMap().forEach((index, feature) {
      final angleRadians = angle * index - pi / 2;
      final xAngle = cos(angleRadians);
      final yAngle = sin(angleRadians);

      final featureOffset = Offset(centerX + radius * xAngle, centerY + radius * yAngle);

      canvas.drawLine(centerOffset, featureOffset, ticksPaint);

      final featureLabelFontHeight = featuresTextStyle.fontSize;
      var labelYOffset = yAngle < 0 ? -featureLabelFontHeight! : 5;
      var labelXOffset = xAngle > 0 ? featureOffset.dx : 0.0;

      TextAlign textAlign = switch (xAngle) {
        < 0.1 && > -0.1 => TextAlign.center,
        < 0 => TextAlign.right,
        _ => TextAlign.left,
      };

      final textPainter = TextPainter(
        text: TextSpan(text: feature, style: featuresTextStyle),
        textAlign: textAlign,
        textDirection: TextDirection.ltr,
      )..layout(minWidth: featureOffset.dx);
      if (xAngle.abs() < 0.1) {
        labelXOffset -= textPainter.width / 2;
        labelYOffset -= 5;
      } else if (xAngle < 0) {
        labelXOffset += feature.length * 2;
      } else {
        labelXOffset -= 30;
      }
      textPainter.paint(canvas, Offset(labelXOffset, featureOffset.dy + labelYOffset));
    });

    // Painting each graph
    data.asMap().forEach((index, graph) {
      final graphPaint = Paint()
        ..color = graphColors[index % graphColors.length].withValues(alpha: 0.3)
        ..style = PaintingStyle.fill;

      final graphOutlinePaint = Paint()
        ..color = graphColors[index % graphColors.length]
        ..style = PaintingStyle.stroke
        ..strokeWidth = 2.0
        ..isAntiAlias = true;

      // Calculates the point coordinates
      final List<Offset> offsets = [];
      graph.asMap().forEach((index, point) {
        if (index == 0) {
          final scaledPoint = scale * graph[0] * fraction;
          offsets.add(Offset(centerX, centerY - scaledPoint));
        } else {
          final xAngle = cos(angle * index - pi / 2);
          final yAngle = sin(angle * index - pi / 2);
          final scaledPoint = scale * point * fraction;
          offsets.add(Offset(centerX + scaledPoint * xAngle, centerY + scaledPoint * yAngle));
        }
      });

      // Display areas
      final path = Path();
      path.moveTo(offsets[0].dx, offsets[0].dy);
      for (int i = 1; i < offsets.length; i++) {
        path.lineTo(offsets[i].dx, offsets[i].dy);
      }
      path.close();
      canvas.drawPath(path, graphPaint);
      canvas.drawPath(path, graphOutlinePaint);

      // Display scores
      Paint paint = Paint()
        ..color = kOrangeColor
        ..style = PaintingStyle.fill;
      graphColors[0];
      for (int i = 0; i < offsets.length; i++) {
        displayScore(canvas, offsets[i], graph[i].toStringAsFixed(1), paint);
      }
    });
  }

  void displayScore(Canvas canvas, Offset center, String score, Paint paint) {
    canvas.drawCircle(center, 15, paint);
    final textPainter = TextPainter(
      text: TextSpan(text: score, style: featuresTextStyle.copyWith(fontSize: 12)),
      textAlign: TextAlign.left,
      textDirection: TextDirection.ltr,
    )..layout(minWidth: center.dx);
    final offset = Offset(center.dx - (score.length * 2.4), center.dy - textPainter.height / 2.3);
    textPainter.paint(canvas, offset);
  }

  @override
  bool shouldRepaint(RadarChartPainter oldDelegate) {
    return oldDelegate.fraction != fraction;
  }
}
