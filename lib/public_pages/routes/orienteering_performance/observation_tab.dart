// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:vikazimut_website/l10n/l10n.dart';

import 'key_performance_indicator/analysis_report.dart';
import 'key_performance_indicator/area_score.dart';
import 'my_tab_controller.dart';

class ObservationTab extends StatelessWidget {
  final AreaScore featureValues;
  final MyTabController tabController;
  final List<String> globalComments;

  const ObservationTab({
    required this.featureValues,
    required this.globalComments,
    required this.tabController,
  });

  @override
  Widget build(BuildContext context) {
    return ObservationWidget(
      globalComments: globalComments,
      controlExitComments: featureValues.controlExitReport.comments!,
      handrailComments: featureValues.handrailReport.comments!,
      controlAttackComments: featureValues.controlAttackReport.comments!,
      tabController: tabController,
    );
  }
}

class ObservationWidget extends StatefulWidget {
  final List<String> globalComments;
  final Comment controlExitComments;
  final Comment handrailComments;
  final Comment controlAttackComments;
  final MyTabController tabController;

  const ObservationWidget({
    super.key,
    required this.globalComments,
    required this.controlExitComments,
    required this.handrailComments,
    required this.controlAttackComments,
    required this.tabController,
  });

  @override
  State<ObservationWidget> createState() => _ObservationWidgetState();
}

class _ObservationWidgetState extends State<ObservationWidget> with AutomaticKeepAliveClientMixin<ObservationWidget> {
  late final ScrollController _scrollController;

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    _scrollController = ScrollController();
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return SingleChildScrollView(
      controller: _scrollController,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if (widget.globalComments.isNotEmpty)
            Column(
              children: [
                Padding(
                  padding: _headerPadding,
                  child: Text(
                    L10n.getString("global_performance_comment_title"),
                    style: _headerTextStyle,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    widget.globalComments.join("\n"),
                    style: Theme.of(context).textTheme.bodyMedium,
                  ),
                ),
                const Divider(),
              ],
            ),
          Padding(
            padding: _headerPadding,
            child: Row(
              children: [
                Icon(Icons.explore, color: Color(0xFF1186FF )),
                SizedBox(width: 8),
                Text(
                  L10n.getString("global_performance_tab1_title"),
                  style: _headerTextStyle,
                ),
              ],
            ),
          ),
          _textLine(
            context,
            "global_performance_exit_comment1",
            widget.controlExitComments.stop,
            widget.tabController,
          ),
          _textLine(
            context,
            "global_performance_exit_comment2",
            widget.controlExitComments.speed,
            widget.tabController,
          ),
          _textLine(
            context,
            "global_performance_exit_comment3",
            widget.controlExitComments.uTurn,
            widget.tabController,
          ),
          _textLine(
            context,
            "global_performance_exit_comment4",
            widget.controlExitComments.direction,
            widget.tabController,
          ),
          const Divider(),
          Padding(
            padding: _headerPadding,
            child: Row(
              children: [
                Icon(Icons.explore, color: Color(0xFFCDDC39)),
                SizedBox(width: 8),
                Text(
                  L10n.getString("global_performance_tab2_title"),
                  style: _headerTextStyle,
                ),
              ],
            ),
          ),
          _textLine(
            context,
            "global_performance_handrail_comment1",
            widget.handrailComments.uTurn,
            widget.tabController,
          ),
          _textLine(
            context,
            "global_performance_handrail_comment2",
            widget.handrailComments.stop,
            widget.tabController,
          ),
          _textLine(
            context,
            "global_performance_handrail_comment3",
            widget.handrailComments.speed,
            widget.tabController,
          ),
          const Divider(),
          Padding(
            padding: _headerPadding,
            child: Row(
              children: [
                Icon(Icons.explore, color: Color(0xFFFF00FF )),
                SizedBox(width: 8),
                Text(
                  L10n.getString("global_performance_tab3_title"),
                  style: _headerTextStyle,
                ),
              ],
            ),
          ),
          _textLine(
            context,
            "global_performance_attack_comment1",
            widget.controlAttackComments.stop,
            widget.tabController,
          ),
          _textLine(
            context,
            "global_performance_attack_comment2",
            widget.controlAttackComments.speed,
            widget.tabController,
          ),
          _textLine(
            context,
            "global_performance_attack_comment3",
            widget.controlAttackComments.uTurn,
            widget.tabController,
          ),
          _textLine(
            context,
            "global_performance_attack_comment4",
            widget.controlAttackComments.direction,
            widget.tabController,
          ),
        ],
      ),
    );
  }
}

const TextStyle _headerTextStyle = TextStyle(fontSize: 22.0, fontWeight: FontWeight.bold);
const EdgeInsets _headerPadding = EdgeInsets.only(top: 10.0);

Widget _textLine(
  BuildContext context,
  String text,
  List<LegComment> legs,
  final MyTabController tabController,
) {
  if (legs.isEmpty) {
    return const SizedBox.shrink();
  }
  String label = (legs.length == 1) ? L10n.getString("${text}_singular") : L10n.getString("${text}_plural");
  return Padding(
    padding: const EdgeInsets.all(8.0),
    child: Text.rich(
      TextSpan(
        children: [
          TextSpan(
            text: label,
            style: Theme.of(context).textTheme.bodyMedium,
          ),
          ...legs.map(
            (LegComment comment) => WidgetSpan(
              alignment: PlaceholderAlignment.middle,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                child: OutlinedButton(
                  onPressed: () => tabController.openTabWithLeg(comment.legIndex + 1),
                  child: Text(comment.legName),
                ),
              ),
            ),
          ),
        ],
      ),
    ),
  );
}
