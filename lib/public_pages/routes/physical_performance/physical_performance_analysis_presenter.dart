import 'package:vikazimut_website/public_pages/model/checkpoint.dart';
import 'package:vikazimut_website/public_pages/routes/gps_route.dart';
import 'package:vikazimut_website/public_pages/routes/physical_performance/physical_performance_processor.dart';

import 'effort_bin.dart';

class PerformanceAnalysisPresenter {
  static Future<List<List<EffortBin>>?> computePhysicalPerformances(
    List<GpsRoute> selectedTracks, List<Checkpoint> checkpoints,
  ) async {
    return selectedTracks.map((track) => PhysicalPerformanceProcessor.computeSpeedEffortDifferenceWithAverageSpeedPerGradient(track, checkpoints)).toList(growable: false);
  }
}
