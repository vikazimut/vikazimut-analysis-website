// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:vikazimut_website/constants.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/theme/theme.dart';

import 'line_chart_widget.dart';
import 'effort_bin.dart';

class PhysicalPerformanceWidget extends StatelessWidget {
  final String nickname;
  final Color trackColor;
  final List<EffortBin> data;

  const PhysicalPerformanceWidget({
    required this.nickname,
    required this.trackColor,
    required this.data,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(left: COLLAPSIBLE_MIN_WIDTH),
          child: Row(
            children: [
              Icon(Icons.person, color: trackColor),
              Text(
                nickname,
                style: const TextStyle(fontWeight: FontWeight.bold),
              ),
            ],
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Icon(Icons.query_stats, color: kOrangeColor),
            const SizedBox(width: 8),
            Text(
              L10n.getString("physical_performance_widget_title"),
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold, color: kOrangeColor),
            )
          ],
        ),
        Container(
          padding: const EdgeInsets.all(8.0),
          child: Card(
            margin: const EdgeInsets.only(left: 0, top: 10),
            child: CustomPaint(
              painter: LineChartWidget(context, data: data),
              child: Container(
                constraints: BoxConstraints(
                  maxWidth: 500,
                  maxHeight: 400,
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
