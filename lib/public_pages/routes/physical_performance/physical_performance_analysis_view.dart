// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:vikazimut_website/common/discipline.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/public_pages/routes/route_list_page_presenter.dart';
import 'package:vikazimut_website/utils/global_error_widget.dart';

import 'physical_performance_analysis_presenter.dart';
import 'physical_performance_widget.dart';
import 'side_bar.dart';
import 'effort_bin.dart';

class PhysicalPerformanceAnalysisView extends StatefulWidget {
  final RouteListPagePresenter routePresenter;
  final Future<CourseData> courseInfo;

  const PhysicalPerformanceAnalysisView({
    required this.routePresenter,
    required this.courseInfo,
  });

  @override
  State<PhysicalPerformanceAnalysisView> createState() => _PhysicalPerformanceAnalysisViewState();
}

class _PhysicalPerformanceAnalysisViewState extends State<PhysicalPerformanceAnalysisView> with SingleTickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    if (widget.routePresenter.courseData!.courseGeoreference.discipline == Discipline.MTBO) {
      return Center(
        child: Text(
          L10n.getString("physical_performance_bad_discipline_error"),
        ),
      );
    } else if (widget.routePresenter.selectedTracks.isEmpty) {
      return Center(
        child: Text(
          L10n.getString("orienteer_profile_no_orienteer"),
        ),
      );
    } else {
      return FutureBuilder(
        future: PerformanceAnalysisPresenter.computePhysicalPerformances(
          widget.routePresenter.selectedTracks,
          widget.routePresenter.courseData!.checkpoints,
        ),
        builder: (BuildContext context, AsyncSnapshot<List<List<EffortBin>>?> snapshot) {
          if (snapshot.hasData && snapshot.connectionState == ConnectionState.done) {
            List<List<EffortBin>> data = snapshot.data!;
            return Stack(
              alignment: Alignment.topRight,
              children: [
                ListView.builder(
                  itemCount: data.length,
                  itemBuilder: (BuildContext context, int index) {
                    return PhysicalPerformanceWidget(
                      nickname: widget.routePresenter.selectedTracks[index].nickname,
                      trackColor: widget.routePresenter.selectedTracks[index].color,
                      data: data[index],
                    );
                  },
                ),
                const SideBar(),
              ],
            );
          } else if (snapshot.hasError) {
            return GlobalErrorWidget(snapshot.error.toString());
          } else {
            return Container();
          }
        },
      );
    }
  }
}
