import 'dart:math' as math;
import 'dart:ui';

import 'package:flutter/foundation.dart';
import 'package:image/image.dart' as img;
import 'package:vikazimut_website/public_pages/model/geodesic_point.dart';
import 'package:vikazimut_website/public_pages/routes/gps_route.dart';
import 'package:vikazimut_website/utils/utils.dart';

import 'image_size.dart';

class GpsTrackPainter {
  static Future<void> addTrackDataToTextureImage(
    List<GpsRoute> selectedTracks,
    List<double> mapGeodesicCenter,
    List<double> mapGeodesicSize,
    double rotationAngleInDegree,
    img.Image textureImage,
  ) async {
    final rotationAngleInRadian = deg2rad(-rotationAngleInDegree);
    final sinAngle = math.sin(rotationAngleInRadian);
    final cosAngle = math.cos(rotationAngleInRadian);

    GeodesicPoint rotation(GeodesicPoint point) => rotation_(point, cosAngle, sinAngle, mapGeodesicCenter[0], mapGeodesicCenter[1]);
    Point projection(GeodesicPoint point) => projection_(point, ImageSize(textureImage.width, textureImage.height), mapGeodesicCenter, mapGeodesicSize);

    for (int i = 0; i < selectedTracks.length; i++) {
      if (selectedTracks[i].model.route != null && selectedTracks[i].model.route!.isNotEmpty) {
        final Color trackColor = selectedTracks[i].color;
        final img.Color trackColorRgba = img.ColorRgb8(
          (trackColor.r * 255).toInt(),
          (trackColor.g * 255).toInt(),
          (trackColor.b * 255).toInt(),
        );
        final List<Point> points = [];
        for (GeodesicPoint point in selectedTracks[i].model.route!) {
          points.add(projection(rotation(point)));
        }
        await _drawPath(textureImage, points, trackColorRgba);
      }
    }
  }

  @visibleForTesting
  static GeodesicPoint rotation_(
    GeodesicPoint geodesicPoint,
    final double cosAngle,
    final double sinAngle,
    final double longitudeCenter,
    final double latitudeCenter,
  ) {
    final double xDelta = geodesicPoint.latitude - latitudeCenter;
    final double yDelta = geodesicPoint.longitude - longitudeCenter;
    final longitudeRotated = -xDelta * sinAngle + yDelta * cosAngle + longitudeCenter;
    final latitudeRotated = xDelta * cosAngle + yDelta * sinAngle + latitudeCenter;
    return GeodesicPoint(latitudeRotated, longitudeRotated, 0, geodesicPoint.altitude);
  }

  @visibleForTesting
  static Point projection_(
    GeodesicPoint geodesicPoint,
    ImageSize textureSize,
    List<double> mapGeodesicCenter,
    List<double> mapGeodesicSize,
  ) {
    return Point(
      (textureSize.width / 2 + textureSize.width * (geodesicPoint.longitude - mapGeodesicCenter[0]) / mapGeodesicSize[0]).toInt(),
      (textureSize.height / 2 - (textureSize.height * (geodesicPoint.latitude - mapGeodesicCenter[1]) / mapGeodesicSize[1])).toInt(),
    );
  }

  static Future<void> _drawPath(img.Image textureData, List<Point> points, img.Color color) async {
    var command = img.Command()..image(textureData);
    for (var i = 1; i < points.length; ++i) {
      command.drawLine(
        x1: points[i - 1].x,
        y1: points[i - 1].y,
        x2: points[i].x,
        y2: points[i].y,
        color: color,
        thickness: 3,
        antialias: true,
      );
      await command.execute();
    }
  }
}

class Point {
  final int x;
  final int y;

  Point(this.x, this.y);
}
