import 'dart:async';
import 'dart:io';
import 'dart:math' as math;

import 'package:http/http.dart' as http;
import 'package:vikazimut_website/common/lat_lon_box.dart';
import 'package:vikazimut_website/utils/utils.dart';

import 'course_3d_model.dart';
import 'image_size.dart';

class IgnAltimetryGateway {
  static const String GATEWAY_URL = "https://data.geopf.fr/altimetrie/1.0/calcul/alti/rest/elevation.json";

  static Future<Heightmap> fetchAltitudesForRegion(
    LatLonBox region,
    int orienteeringMapWidth,
    int orienteeringMapHeight,
  ) async {
    final double widthHeightRatio = orienteeringMapWidth / orienteeringMapHeight;
    final ImageSize altitudeMapSize = ImageSize(
      math.sqrt(Course3dModel.MAX_POINT_COUNT * Course3dModel.NUMBER_OF_PARTS * widthHeightRatio).floor(),
      math.sqrt(Course3dModel.MAX_POINT_COUNT * Course3dModel.NUMBER_OF_PARTS / widthHeightRatio).floor(),
    );
    final double latN = region.north;
    final double latS = region.south;
    final double lonE = region.east;
    final double lonW = region.west;
    final double deltaLat = (latN - latS) / altitudeMapSize.height;
    final double deltaLon = (lonE - lonW) / altitudeMapSize.width;

    // Warning: Points are upside down: from north to south
    final longitudeCenter = (lonE + lonW) / 2;
    final latitudeCenter = (latN + latS) / 2;
    final angleInRadian = deg2rad(region.rotation);
    final cosAngle = math.cos(angleInRadian);
    final sinAngle = math.sin(angleInRadian);
    List<_Position> points = [];
    for (var i = 0; i < altitudeMapSize.height; i++) {
      final latitude = latN - i * deltaLat;
      for (var j = 0; j < altitudeMapSize.width; j++) {
        final longitude = lonW + j * deltaLon;
        _Position point = _rotate(longitude, latitude, cosAngle, sinAngle, longitudeCenter, latitudeCenter);
        points.add(point);
      }
    }

    final int size = altitudeMapSize.height * altitudeMapSize.width;
    List<Future<List<double>?>> requests = [];
    for (int i = 0; i < Course3dModel.NUMBER_OF_PARTS; i++) {
      requests.add(
        _requestPointsFromGateway(
          points.sublist(
            i * size ~/ Course3dModel.NUMBER_OF_PARTS,
            (i + 1) * size ~/ Course3dModel.NUMBER_OF_PARTS,
          ),
        ),
      );
    }
    List<List<double>?> dataParts = await Future.wait(requests);
    for (int i = 0; i < Course3dModel.NUMBER_OF_PARTS; i++) {
      if (dataParts[i] == null) {
        return Heightmap([], const ImageSize(0, 0));
      }
    }
    List<double> data = [];
    for (int i = 0; i < Course3dModel.NUMBER_OF_PARTS; i++) {
      data += dataParts[i]!;
    }
    return Heightmap(data, altitudeMapSize);
  }

  static _Position _rotate(final double longitude, final double latitude, final double cosAngle, final double sinAngle, final double longitudeCenter, final double latitudeCenter) {
    final longitudeRotated = -(latitude - latitudeCenter) * sinAngle + (longitude - longitudeCenter) * cosAngle + longitudeCenter;
    final latitudeRotated = (latitude - latitudeCenter) * cosAngle + (longitude - longitudeCenter) * sinAngle + latitudeCenter;
    return _Position(longitude: longitudeRotated, latitude: latitudeRotated);
  }

  static Future<List<double>?> _requestPointsFromGateway(List<_Position> points) async {
    StringBuffer latitudes = StringBuffer();
    StringBuffer longitudes = StringBuffer();
    latitudes.write("${points[0].latitude}");
    longitudes.write("${points[0].longitude}");
    for (var i = 1; i < points.length; ++i) {
      latitudes.write("|${points[i].latitude}");
      longitudes.write("|${points[i].longitude}");
    }
    var jsonRequest = '{"lon": "${longitudes.toString()}","lat": "${latitudes.toString()}","resource": "ign_rge_alti_wld","delimiter": "|","indent": "false","measures": "false","zonly": "true"}';
    try {
      final response = await http.post(
        Uri.parse(GATEWAY_URL),
        headers: {
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.contentTypeHeader: "application/json",
        },
        body: jsonRequest,
      );
      if (response.statusCode == HttpStatus.ok) {
        return _extractAltitudesFromXML(response.body);
      }
    } catch (_) {}
    return null;
  }

  static List<double>? _extractAltitudesFromXML(String xmlString) {
    final RegExp regex = RegExp(r'<z>(.*)</z>');
    List<double> altitudes = [];
    Iterable<RegExpMatch> allMatches = regex.allMatches(xmlString);
    for (RegExpMatch match in allMatches) {
      double altitude = double.parse(match[1]!);
      if (altitude < -1000.0) {
        return null;
      }
      altitudes.add(altitude);
    }
    return altitudes;
  }
}

class _Position {
  final double longitude;
  final double latitude;

  _Position({required this.longitude, required this.latitude});
}
