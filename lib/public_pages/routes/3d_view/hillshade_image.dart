import 'dart:math' as math;

import 'package:flutter/foundation.dart';
import 'package:vikazimut_website/utils/utils.dart';

class HillshadeImage {
  static List<int> generateMultidirectionalHillshade(
    List<double> heightmap,
    int heightmapWidth,
    int heightmapHeight,
    double resolutionInMeterPerPixel,
  ) {
    const double illuminationAltitude = 45;
    const List<double> illuminationAzimuths = [225, 270, 315, 360];
    List<List<int>> hillshadeImages = _generateMultipleHillshade(
      illuminationAzimuths,
      illuminationAltitude,
      heightmap,
      heightmapWidth,
      heightmapHeight,
      resolutionInMeterPerPixel,
    );
    return brighten_(combineMultipleHillshades_(hillshadeImages));
  }

  static List<List<int>> _generateMultipleHillshade(
    List<double> illuminationAzimuths,
    double illuminationAltitude,
    List<double> heightmap,
    int heightmapWidth,
    int heightmapHeight,
    double resolutionInMeterPerPixel,
  ) {
    return illuminationAzimuths
        .map((azimuth) => generateHillshade_(
              illuminationAltitude,
              azimuth,
              heightmap,
              heightmapWidth,
              heightmapHeight,
              resolutionInMeterPerPixel,
            ))
        .toList();
  }

  @visibleForTesting
  static List<int> generateHillshade_(
    double altitude,
    double azimuth,
    List<double> heightmap,
    int heightmapWidth,
    int heightmapHeight,
    double resolutionInMeterPerPixel,
  ) {
    final zenithInDegree = 90.0 - altitude;
    final zenithInRadian = deg2rad(zenithInDegree);
    var azimuthMath = 360.0 - azimuth + 90.0;
    if (azimuthMath >= 360) {
      azimuthMath = azimuthMath - 360.0;
    }
    final azimuthInRadian = deg2rad(azimuthMath);

    List<int> hillshade = List.filled(heightmap.length, 0);
    for (int y = 0; y < heightmapHeight; y++) {
      for (int x = 0; x < heightmapWidth; x++) {
        // Border cases
        int y1 = y - 1;
        if (y1 < 0) y1 = 0;
        int y2 = y + 1;
        if (y2 > heightmapHeight - 1) y2 = heightmapHeight - 1;
        int x1 = x - 1;
        if (x1 < 0) x1 = 0;
        int x2 = x + 1;
        if (x2 > heightmapWidth - 1) x2 = heightmapWidth - 1;

        int a = heightmap[y1 * heightmapWidth + x1].floor();
        int b = heightmap[y1 * heightmapWidth + x].floor();
        int c = heightmap[y1 * heightmapWidth + x2].floor();
        int d = heightmap[y * heightmapWidth + x1].floor();
        int f = heightmap[y * heightmapWidth + x2].floor();
        int g = heightmap[y2 * heightmapWidth + x1].floor();
        int h = heightmap[y2 * heightmapWidth + x].floor();
        int i = heightmap[y2 * heightmapWidth + x2].floor();

        double dzdxGradient = ((c + 2 * f + i) - (a + 2 * d + g)) / (8 * resolutionInMeterPerPixel);
        double dzdyGradient = ((g + 2 * h + i) - (a + 2 * b + c)) / (8 * resolutionInMeterPerPixel);
        double slopeInRadian = math.atan(math.sqrt(dzdxGradient * dzdxGradient + dzdyGradient * dzdyGradient));

        double aspectInRadian;
        if (dzdxGradient != 0) {
          aspectInRadian = math.atan2(dzdyGradient, -dzdxGradient);
          if (aspectInRadian < 0) {
            aspectInRadian = 2 * math.pi + aspectInRadian;
          }
        } else {
          if (dzdyGradient > 0) {
            aspectInRadian = math.pi / 2;
          } else {
            if (dzdyGradient < 0) {
              aspectInRadian = 2 * math.pi - math.pi / 2;
            } else {
              aspectInRadian = 0;
            }
          }
        }

        double luminance = (math.cos(zenithInRadian) * math.cos(slopeInRadian)) + (math.sin(zenithInRadian) * math.sin(slopeInRadian) * math.cos(azimuthInRadian - aspectInRadian));
        // Normalize between 0 and 255
        if (luminance > 1.0) {
          hillshade[y * heightmapWidth + x] = 255;
        } else if (luminance < 0) {
          hillshade[y * heightmapWidth + x] = 0;
        } else {
          hillshade[y * heightmapWidth + x] = (luminance * 255.0).round();
        }
      }
    }
    return hillshade;
  }

  static List<int> combineMultipleHillshades_(final List<List<int>> hillshades) {
    final List<int> hillshade = List<int>.filled(hillshades[0].length, 0);
    for (int i = 0; i < hillshades[0].length; i++) {
      int sum = 0;
      for (int j = 0; j < hillshades.length; j++) {
        sum += hillshades[j][i];
      }
      hillshade[i] = sum ~/ hillshades.length;
    }
    return hillshade;
  }

  static List<int> brighten_(List<int> image) {
    var max = image.reduce(math.max);
    final offset = 255 - max;
    for (var i = 0; i < image.length; ++i) {
      image[i] += offset;
    }
    return image;
  }
}
