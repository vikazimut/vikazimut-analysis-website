import 'package:flutter/foundation.dart';
import 'package:vikazimut_website/common/discipline.dart';

abstract class SpeedColorHandlerFactory {
  static SpeedColorHandler build(Discipline? discipline) {
    if (Discipline.isFootOrienteering(discipline)) {
      return SpeedFootoColorHandler();
    } else {
      return SpeedMtboColorHandler();
    }
  }
}

abstract class SpeedColorHandler {
  static const List<int> _speedColors = [
    0xFFF44336, // stop
    0xFFFF9800, // walk
    0xFFFFEB3B, // low speed running
    0xFF2ECF81, // medium speed running
    0xFF1A9056, // high speed running
  ];

  int colorFromVelocity(double velocityInKmH) => _speedColors[colorIndexFromVelocity(velocityInKmH)];

  List<int> get speedThreshold;

  @visibleForTesting
  int colorIndexFromVelocity(double velocityInKmH) {
    for (int i = 0; i < speedThreshold.length; i++) {
      if (velocityInKmH < speedThreshold[i]) {
        return i;
      }
    }
    return _speedColors.length - 1;
  }
}

class SpeedFootoColorHandler extends SpeedColorHandler {
  static const List<int> speedThresholdsInKmh = [2, 6, 10, 15];

  @override
  List<int> get speedThreshold => speedThresholdsInKmh;
}

class SpeedMtboColorHandler extends SpeedColorHandler {
  static const List<int> speedThresholdsInKmh = [7, 14, 21, 28];

  @override
  List<int> get speedThreshold => speedThresholdsInKmh;
}
