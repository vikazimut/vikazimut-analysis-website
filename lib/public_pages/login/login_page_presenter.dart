import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/common/connection_exception.dart';
import 'package:vikazimut_website/public_pages/login/login_page_model.dart';
import 'package:vikazimut_website/authentication/identifier_user.dart';

import 'login_page_view.dart';

class LoginPagePresenter {
  final LoginPageViewState view;

  const LoginPagePresenter(this.view);

  Future<void> sendLoginRequest(String username, String password) async {
    try {
      IdentifiedUser? identifierUser = await LoginPageModel.sendCredentials(username, password);
      if (identifierUser != null) {
        view.redirect();
      } else {
        view.displayError(L10n.getString("login_page_invalid_credentials"));
      }
    } on ConnectionException catch (error) {
      view.displayError(error.message);
    } catch (e) {
      view.displayError("${L10n.getString("Server connection error")}\n($e)");
    }
  }
}
