import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong2/latlong.dart';
import 'package:vikazimut_website/public_pages/courses/worldmap/geolocation.dart';

class MapZoomController {
  final MapController mapController = MapController();
  final TickerProvider view;
  LatLngBounds? _bounds;

  MapZoomController(this.view);

  void dispose() {
    mapController.dispose();
  }

  set bounds(LatLngBounds bounds) {
    _bounds = bounds;
  }

  void zoomIn() => animatedMapMove(destination: mapController.camera.center, zoom: mapController.camera.zoom + 0.5);

  void zoomOut() => animatedMapMove(destination: mapController.camera.center, zoom: mapController.camera.zoom - 0.5);

  void reset() {
    if (_bounds != null) {
      mapController.fitCamera(CameraFit.bounds(bounds: _bounds!, padding: const EdgeInsets.all(20)));
    }
  }

  void animatedMapMove({required LatLng destination, required double zoom}) {
    // Create some tween. These serve to split up the transition from one location to another.
    // In our case, we want to split the transition be<tween> our current map center and the destination.
    final latTween = Tween<double>(begin: mapController.camera.center.latitude, end: destination.latitude);
    final lngTween = Tween<double>(begin: mapController.camera.center.longitude, end: destination.longitude);
    final zoomTween = Tween<double>(begin: mapController.camera.zoom, end: zoom);

    // Create a animation controller that has a duration and a TickerProvider.
    var controller = AnimationController(duration: const Duration(milliseconds: 800), vsync: view);
    // The animation determines what path the animation will take. You can try different Curves values, although I found
    // fastOutSlowIn to be my favorite.
    Animation<double> animation = CurvedAnimation(parent: controller, curve: Curves.fastOutSlowIn);

    controller.addListener(() {
      mapController.move(LatLng(latTween.evaluate(animation), lngTween.evaluate(animation)), zoomTween.evaluate(animation));
    });

    animation.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        controller.dispose();
      } else if (status == AnimationStatus.dismissed) {
        controller.dispose();
      }
    });

    controller.forward();
  }

  GeoLocation getCurrentLocation() {
    return GeoLocation(
      latitude: mapController.camera.center.latitude,
      longitude: mapController.camera.center.longitude,
      zoom: mapController.camera.zoom,
    );
  }
}
