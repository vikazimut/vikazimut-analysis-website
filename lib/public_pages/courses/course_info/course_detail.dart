import 'package:flutter/foundation.dart';
import 'package:vikazimut_website/common/discipline.dart';

@immutable
class CourseDetail {
  final String creator;
  final int length;
  final int checkpointCount;
  final Discipline? discipline;
  final String? club;
  final String? clubUrl;
  final bool touristic;
  final String creationDate;

  const CourseDetail({
    required this.creator,
    required this.length,
    required this.checkpointCount,
    required this.creationDate,
    this.discipline,
    this.club,
    this.clubUrl,
    this.touristic = false,
  });

  factory CourseDetail.fromJson(Map<String, dynamic> json) {
    return CourseDetail(
      creator: json["creator"] as String,
      length: json["length"] as int,
      checkpointCount: json["checkpointCount"] as int,
      discipline: Discipline.toEnum(json["discipline"] as int?),
      club: json["club_name"] as String?,
      clubUrl: json["club_url"] as String?,
      creationDate: json["date"] as String,
      touristic: (json["touristRoute"] as bool?) ?? false,
    );
  }
}
