// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_map_cancellable_tile_provider/flutter_map_cancellable_tile_provider.dart';
import 'package:latlong2/latlong.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:vikazimut_website/common/discipline.dart';
import 'package:vikazimut_website/common/flutter_map_layers/custom_overlay_image_layer.dart';
import 'package:vikazimut_website/common/flutter_map_layers/scale_layer_widget.dart';
import 'package:vikazimut_website/common/network_image.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/menu/responsive_scaffold_widget.dart';
import 'package:vikazimut_website/public_pages/courses/map_zoom.dart';
import 'package:vikazimut_website/public_pages/model/course_georeference.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/global_error_widget.dart';
import 'package:vikazimut_website/utils/openstreetmap_contribution_widget.dart';
import 'package:vikazimut_website/utils/screen_helper.dart';
import 'package:vikazimut_website/utils/sidebar_widget/collapsible_or_rigid_sidebar.dart';
import 'package:vikazimut_website/utils/time.dart';

import 'course_detail.dart';
import 'course_info_page_presenter.dart';

class CourseInfoViewPage extends StatefulWidget {
  static const String routePath = '/course/info';
  final int courseId;

  CourseInfoViewPage(args) : courseId = int.parse(args);

  @override
  CourseInfoViewPageState createState() => CourseInfoViewPageState();
}

class CourseInfoViewPageState extends State<CourseInfoViewPage> with TickerProviderStateMixin {
  final CourseInfoPagePresenter _courseInfoPresenter = CourseInfoPagePresenter();
  late final Future<CourseDetail?> _courseDetail;
  late final Future<CourseGeoreference?> _courseGeoreference;
  final ValueNotifier<String> _courseName = ValueNotifier<String>("");
  late final MapZoomController mapZoomController;
  bool _isMapShowed = true;

  @override
  void initState() {
    _courseDetail = _courseInfoPresenter.loadCourseDetail(widget.courseId);
    _courseGeoreference = CourseGeoreference.loadCourseMap(widget.courseId);
    _courseGeoreference.then((CourseGeoreference? value) {
      if (mounted) _courseName.value = value == null ? '' : value.name;
    }).catchError((_) {});
    mapZoomController = MapZoomController(this);
    super.initState();
  }

  @override
  void dispose() {
    _courseName.dispose();
    mapZoomController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var overlayImages = <CustomBaseOverlayImage>[];
    return ResponsiveScaffoldWidget(
      body: FutureBuilder<CourseDetail?>(
        future: _courseDetail,
        builder: (BuildContext context, AsyncSnapshot<CourseDetail?> snapshotCourse) {
          var titleText = ValueListenableBuilder<String>(
              valueListenable: _courseName,
              builder: (context, value, widget) {
                return Text(value, style: Theme.of(context).textTheme.headlineMedium?.copyWith(color: kOrangeColor));
              });
          if (snapshotCourse.hasError) {
            return GlobalErrorWidget(L10n.getString(snapshotCourse.error.toString()));
          } else if (snapshotCourse.hasData) {
            return CollapsibleOrRigidSidebar(
              isCollapsible: !ScreenHelper.isDesktop(context),
              sidebarTitle: titleText,
              sidebarContents: [
                _ItemWidget(
                  label: L10n.getString("information_discipline_tile"),
                  icon: Icon(
                    Discipline.getIcon(snapshotCourse.data!.discipline),
                    color: Theme.of(context).iconTheme.color,
                    size: 32,
                  ),
                ),
                _ItemWidget(
                  label: L10n.getString("information_author_tile"),
                  value: snapshotCourse.data!.creator,
                ),
                _ItemWidget(
                  label: L10n.getString("information_club_name_tile"),
                  value: (snapshotCourse.data!.club != null) ? snapshotCourse.data!.club! : "",
                ),
                if (snapshotCourse.data!.clubUrl != null)
                  _ItemWidget(
                    label: L10n.getString("information_club_url_tile"),
                    url: (snapshotCourse.data!.clubUrl != null) ? snapshotCourse.data!.clubUrl! : "",
                  ),
                _ItemWidget(
                  label: L10n.getString("information_date_tile"),
                  value: getLocalizedDate(convertISO8601DateToMilliseconds(snapshotCourse.data!.creationDate)),
                ),
                _ItemWidget(
                  label: L10n.getString("information_length_tile"),
                  value: '${L10n.formatNumber(snapshotCourse.data!.length, 2)} m',
                ),
                _ItemWidget(
                  label: L10n.getString("information_checkpoint_tile"),
                  value: '${snapshotCourse.data!.checkpointCount}',
                ),
                if (snapshotCourse.data!.touristic)
                  _ItemWidget(
                    label: L10n.getString("information_note_title"),
                    valueColor: kSuccessColor,
                    value: L10n.getString("information_note_text"),
                  ),
              ],
              contents: FutureBuilder<CourseGeoreference?>(
                future: _courseGeoreference,
                builder: (BuildContext context, AsyncSnapshot<CourseGeoreference?> snapshot) {
                  if (snapshot.hasError) {
                    return GlobalErrorWidget(snapshot.error.toString());
                  } else if (snapshot.hasData && snapshot.data != null) {
                    var latLngBounds = snapshot.data!.latLngBounds();
                    var rotation = -snapshot.data!.bounds.rotation;
                    LatLngBounds bounds = LatLngBounds(LatLng(latLngBounds[3][0], latLngBounds[3][1]), LatLng(latLngBounds[1][0], latLngBounds[1][1]));
                    mapZoomController.bounds = bounds;
                    overlayImages = <CustomRotatedOverlayImage>[
                      CustomRotatedOverlayImage(
                        bounds: bounds,
                        opacity: 0.8,
                        angleInDegree: rotation,
                        imageProvider: loadNetworkImage(_courseInfoPresenter.user, snapshot.data!.imageId),
                      ),
                    ];
                    return Stack(
                      alignment: Alignment.topRight,
                      children: [
                        _GeoreferencedMap(
                          mapController: mapZoomController.mapController,
                          bounds: bounds,
                          startLatitude: snapshot.data!.startLatitude,
                          startLongitude: snapshot.data!.startLongitude,
                          showedMap: _isMapShowed,
                          overlayImages: overlayImages,
                        ),
                        Positioned(
                          right: 5.0,
                          top: 15.0,
                          child: Column(
                            children: [
                              if (ScreenHelper.isDesktop(context))
                                _MapButton(
                                  iconData: Icons.add,
                                  onPressed: () => mapZoomController.zoomIn(),
                                ),
                              if (ScreenHelper.isDesktop(context))
                                _MapButton(
                                  iconData: Icons.remove,
                                  onPressed: () => mapZoomController.zoomOut(),
                                ),
                              if (!ScreenHelper.isMobile(context))
                                _MapButton(
                                  iconData: Icons.my_location,
                                  onPressed: () => mapZoomController.reset(),
                                ),
                              _MapButton(
                                iconData: (_isMapShowed) ? Icons.layers_clear : Icons.layers,
                                onPressed: () => setState(() => _isMapShowed = !_isMapShowed),
                              ),
                            ],
                          ),
                        ),
                      ],
                    );
                  } else {
                    return Container();
                  }
                },
              ),
            );
          }
          return Container();
        },
      ),
    );
  }
}

class _MapButton extends StatelessWidget {
  final VoidCallback onPressed;
  final IconData iconData;

  const _MapButton({
    required this.onPressed,
    required this.iconData,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: kOrangeColorUltraLight,
        borderRadius: BorderRadius.circular(4),
        border: Border.all(width: 1, color: kCaptionColor),
      ),
      child: IconButton(
        icon: Icon(iconData, size: 24, color: Colors.black),
        onPressed: onPressed,
      ),
    );
  }
}

class _ItemWidget extends StatelessWidget {
  final String label;
  final String? value;
  final Color? valueColor;
  final String? url;
  final Widget? icon;

  const _ItemWidget({
    required this.label,
    this.icon,
    this.value,
    this.valueColor,
    this.url,
  });

  @override
  Widget build(BuildContext context) {
    return ListTile(
      contentPadding: const EdgeInsets.symmetric(horizontal: 5.0),
      title: Text(
        label,
        style: const TextStyle(color: kOrangeColor),
      ),
      subtitle: Padding(
        padding: const EdgeInsets.only(left: 8.0),
        child: url != null
            ? InkWell(
                onTap: () => launchUrl(Uri.parse(url!)),
                child: Text(
                  _removeHttpFromUrl(url!),
                  textScaler: const TextScaler.linear(1.2),
                  style: const TextStyle(color: kInfoColor),
                ),
              )
            : (value != null)
                ? Text(
                    value!,
                    textScaler: const TextScaler.linear(1.2),
                    style: TextStyle(color: valueColor),
                  )
                : (icon != null)
                    ? Align(alignment: Alignment.topLeft, child: icon)
                    : null,
      ),
    );
  }

  _removeHttpFromUrl(String url) {
    if (url.contains("https://")) return url.substring(8);
    if (url.contains("http://")) return url.substring(7);
    return url;
  }
}

class _GeoreferencedMap extends StatefulWidget {
  final MapController mapController;
  final LatLngBounds bounds;
  final double startLatitude;
  final double startLongitude;
  final bool showedMap;
  final List<CustomBaseOverlayImage> overlayImages;

  @override
  State<_GeoreferencedMap> createState() => _GeoreferencedMapState();

  const _GeoreferencedMap({
    required this.mapController,
    required this.bounds,
    required this.startLatitude,
    required this.startLongitude,
    required this.showedMap,
    required this.overlayImages,
  });
}

class _GeoreferencedMapState extends State<_GeoreferencedMap> {
  @override
  Widget build(BuildContext context) {
    return FlutterMap(
      mapController: widget.mapController,
      options: MapOptions(
        maxZoom: 19,
        minZoom: 8,
        interactionOptions: InteractionOptions(
          cursorKeyboardRotationOptions: CursorKeyboardRotationOptions.disabled(),
          flags: InteractiveFlag.all & ~InteractiveFlag.rotate,
          scrollWheelVelocity: 0.002,
        ),
        initialCameraFit: CameraFit.bounds(bounds: widget.bounds, padding: const EdgeInsets.all(20)),
      ),
      children: [
        TileLayer(
          urlTemplate: 'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
          userAgentPackageName: 'fr.vikazim.vikazimut',
          tileProvider: CancellableNetworkTileProvider(),
        ),
        if (widget.showedMap) CustomOverlayImageLayer(overlayImages: widget.overlayImages),
        ScaleLayerWidget(
          options: ScaleLayerPluginOption(
            lineColor: Colors.blue,
            lineWidth: 2,
            textStyle: const TextStyle(color: Colors.blue, fontSize: 12),
          ),
        ),
        MarkerLayer(
          markers: [
            Marker(
              alignment: Alignment.topCenter,
              width: 48.0,
              height: 48.0,
              point: LatLng(widget.startLatitude, widget.startLongitude),
              child: const Icon(
                Icons.location_pin,
                color: kVioline,
                size: 48,
              ),
            ),
          ],
        ),
        OpenStreetMapAttributionWidget(),
      ],
    );
  }
}
