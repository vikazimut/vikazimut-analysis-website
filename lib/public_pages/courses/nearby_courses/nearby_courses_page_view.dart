// coverage:ignore-file
library;

import 'package:web/web.dart' as web;

import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:vikazimut_website/authentication/identifier_user.dart';
import 'package:vikazimut_website/common/discipline.dart';
import 'package:vikazimut_website/common/footer.dart';
import 'package:vikazimut_website/constants.dart' as constants;
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/menu/responsive_scaffold_widget.dart';
import 'package:vikazimut_website/public_pages/courses/course_info/course_info_page_view.dart';
import 'package:vikazimut_website/public_pages/live_tracking/live_tracking_page.dart';
import 'package:vikazimut_website/public_pages/routes/route_list_page_view.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/container_decoration.dart';
import 'package:vikazimut_website/utils/custom_button.dart';
import 'package:vikazimut_website/utils/dropdown_button.dart';
import 'package:vikazimut_website/utils/global_error_widget.dart';
import 'package:vikazimut_website/utils/screen_helper.dart';

import 'nearby_course_model.dart';
import 'nearby_courses_page_presenter.dart';

part 'list_widget.dart';

class NearbyCoursesPageView extends StatefulWidget {
  static const String routePath = '/routes/nearby';

  final String latitude;
  final String longitude;
  late final int? courseId;

  NearbyCoursesPageView({required this.latitude, required this.longitude, String? courseId}) {
    if (courseId != null) {
      this.courseId = int.tryParse(courseId);
    } else {
      this.courseId = null;
    }
  }

  @override
  State<NearbyCoursesPageView> createState() => _NearbyCoursesPageViewState();
}

class _NearbyCoursesPageViewState extends State<NearbyCoursesPageView> {
  final NearbyCoursesPagePresenter _presenter = NearbyCoursesPagePresenter();
  late final Future<List<NearbyCourseModel>> _coursesList;
  late final ScrollController? _scrollController;

  @override
  void initState() {
    _scrollController = ScrollController();
    _coursesList = _presenter.fetchMapList(latitude: widget.latitude, longitude: widget.longitude, courseId: widget.courseId);
    super.initState();
  }

  @override
  void dispose() {
    _scrollController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ResponsiveScaffoldWidget(
      body: Column(
        children: [
          Expanded(
            child: ContainerDecoration(
              constraints: const BoxConstraints(maxWidth: ScreenHelper.desktopMinWidth),
              child: FutureBuilder<List<NearbyCourseModel>>(
                future: _coursesList,
                builder: (context, snapshot) {
                  if (snapshot.hasError) {
                    return GlobalErrorWidget(snapshot.error.toString());
                  } else if (snapshot.hasData) {
                    return _CustomListWidget(
                      courses: snapshot.data!,
                      controller: _scrollController,
                      isFirstCourseSelected: widget.courseId != null,
                    );
                  } else {
                    return const Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                },
              ),
            ),
          ),
          Footer(),
        ],
      ),
    );
  }
}
