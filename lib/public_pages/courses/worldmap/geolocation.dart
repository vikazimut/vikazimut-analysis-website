import 'package:flutter/foundation.dart';

@immutable
class GeoLocation {
  static const double _initialZoom = 6.0;

  final double latitude;
  final double longitude;
  final double zoom;

  const GeoLocation({required this.latitude, required this.longitude, required this.zoom});

  factory GeoLocation.fromJson(Map<String, dynamic> json) {
    double latitude = json["latitude"];
    double longitude = json["longitude"];
    return GeoLocation(
      latitude: latitude,
      longitude: longitude,
      zoom: _initialZoom,
    );
  }
}
