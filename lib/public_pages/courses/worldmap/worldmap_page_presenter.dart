import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vikazimut_website/constants.dart' as constants;
import 'package:vikazimut_website/public_pages/courses/map_zoom.dart';
import 'package:vikazimut_website/public_pages/model/course.dart';

import 'geolocation.dart';
import 'location_as_dot.dart';

class WorldMapPagePresenter {
  static const String _SERVER_COURSE_LIST_URL = "${constants.SERVER}/data/courses";
  static GeoLocation? _userLocation;
  final List<LocationAsDot> _dots = [];

  Future<GeoLocation> getUserLocation() async {
    if (_userLocation != null) {
      return _userLocation!;
    }
    _userLocation = await _loadUserLocation();
    if (_userLocation != null) {
      return _userLocation!;
    }
    try {
      final response = await http.get(Uri.parse(constants.USER_LOCATION_SERVER_URL));
      if (response.statusCode == HttpStatus.ok) {
        _userLocation = GeoLocation.fromJson(jsonDecode(response.body));
        return _userLocation!;
      } else {
        return const GeoLocation(
          latitude: 0,
          longitude: 0,
          zoom: 2,
        );
      }
    } catch (_) {
      return const GeoLocation(
        latitude: 0,
        longitude: 0,
        zoom: 2,
      );
    }
  }

  set userLocation(GeoLocation geoLocation) => _userLocation = geoLocation;

  void saveUserLocation() async {
    if (_userLocation != null) {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String formattedString = "${_userLocation!.latitude}, ${_userLocation!.longitude}, ${_userLocation!.zoom}";
      prefs.setString(constants.KEY_LOCATION, formattedString);
    }
  }

  Future<List<Course>> fetchCourseList() async {
    try {
      final response = await http.get(Uri.parse(_SERVER_COURSE_LIST_URL));
      if (response.statusCode == HttpStatus.ok) {
        return _parseCourseList(response.body);
      } else {
        return [];
      }
    } catch (_) {
      return [];
    }
  }

  List<LocationAsDot> getDots(List<Course> courses) {
    if (_dots.isNotEmpty) {
      return _dots;
    }
    for (final course in courses) {
      int index = -1;
      for (var j = 0; j < _dots.length; j++) {
        if (_dots[j].isAtSamePlace(course.latitude, course.longitude)) {
          index = j;
          break;
        }
      }
      if (index >= 0) {
        _dots[index].addName(course.name);
      } else {
        _dots.add(LocationAsDot(course.latitude, course.longitude, [course.name]));
      }
    }
    return _dots;
  }

  int getSelectCourseIndex(String courseName) => getSelectCourseIndex_(courseName, _dots);

  String geLinkOfCurrentRegion(MapZoomController mapZoomController, String routePath) {
    final currentLocation = mapZoomController.getCurrentLocation();
    final latitude = currentLocation.latitude.toStringAsFixed(6);
    final longitude = currentLocation.longitude.toStringAsFixed(6);
    final zoom = currentLocation.zoom.toStringAsFixed(2);
    return "${constants.SERVER}/web/#$routePath?lat=$latitude&lon=$longitude&zoom=$zoom";
  }

  Future<GeoLocation?> _loadUserLocation() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final formattedString = prefs.getString(constants.KEY_LOCATION);
    if (formattedString != null) {
      List<String> elements = formattedString.split(",");
      double latitude = double.parse(elements[0]);
      double longitude = double.parse(elements[1]);
      double zoom = double.parse(elements[2]);
      return GeoLocation(latitude: latitude, longitude: longitude, zoom: zoom);
    }
    return null;
  }

  static List<Course> _parseCourseList(String responseBody) {
    if (responseBody.isEmpty) {
      return [];
    }
    final parsed = jsonDecode(responseBody).cast<Map<String, dynamic>>();
    List<Course> courseList = [];
    for (final json in parsed) {
      try {
        var mapProxy = Course.fromJson(json);
        courseList.add(mapProxy);
      } catch (_) {}
    }
    return courseList;
  }

  @visibleForTesting
  static int getSelectCourseIndex_(String courseName, List<LocationAsDot> dots) {
    if (courseName.isEmpty) {
      return -1;
    }
    return dots.indexWhere((dot) => dot.names.contains(courseName));
  }

  static Course? getCourseFromName(List<Course> courses, String currentSelectedCourse) {
    try {
      return courses.firstWhere((element) => (element.name == currentSelectedCourse));
    } catch (e) {
      return null;
    }
  }
}
