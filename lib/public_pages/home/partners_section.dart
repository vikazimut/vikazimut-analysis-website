// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/screen_helper.dart';

class PartnersSection extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: kOrangeColorDisabled,
      child: ScreenHelper(
        mobile: (BuildContext context) => _buildUi(context, ScreenHelper.getMobileMaxWidth(context), 0.8),
        tablet: (BuildContext context) => _buildUi(context, ScreenHelper.tabletMaxWidth, 1),
        desktop: (BuildContext context) => _buildUi(context, ScreenHelper.desktopMinWidth, 1),
      ),
    );
  }

  Widget _buildUi(BuildContext context, double width, double scale) {
    return Center(
      child: Container(
        width: width,
        padding: const EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              L10n.getString("homepage_partners_title"),
              style: Theme.of(context).textTheme.displayMedium!.apply(fontSizeFactor: scale),
            ),
            const SizedBox(
              height: 30.0,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Center(
                        child: Image.asset(
                          'assets/images/vikazim_logo.png',
                          fit: BoxFit.cover,
                          height: 130 * scale,
                        ),
                      ),
                      const SizedBox(height: 15),
                      Text(
                        L10n.getString("homepage_vikazim_title"),
                        style: Theme.of(context).textTheme.headlineMedium!.apply(fontSizeFactor: scale, color: Colors.black),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          L10n.getString("homepage_vikazim_description"),
                          style: Theme.of(context).textTheme.bodyMedium!.apply(fontSizeFactor: scale, color: Colors.black),
                        ),
                      ),
                      TextButton(
                        style: TextButton.styleFrom(
                          padding: const EdgeInsets.all(10),
                          backgroundColor: kOrangeColorDarkLight,
                        ),
                        onPressed: () {
                          launchUrl(Uri.parse("https://vikazim.fr/"));
                        },
                        child: Text(
                          '${L10n.getString("read_more")} \u00BB',
                          textAlign: TextAlign.center,
                          style: Theme.of(context).textTheme.bodyMedium!.apply(fontSizeFactor: scale, color: Colors.black),
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(width: 10),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Center(
                        child: Image.asset(
                          'assets/images/ensicaen_logo.png',
                          fit: BoxFit.cover,
                          height: 150 * scale,
                        ),
                      ),
                      Text(
                        L10n.getString("homepage_ensicaen_title"),
                        style: Theme.of(context).textTheme.headlineMedium!.apply(fontSizeFactor: scale, color: Colors.black),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          L10n.getString("homepage_ensicaen_description"),
                          style: Theme.of(context).textTheme.bodyMedium!.apply(fontSizeFactor: scale, color: Colors.black),
                        ),
                      ),
                      TextButton(
                        style: TextButton.styleFrom(
                          padding: const EdgeInsets.all(10),
                          backgroundColor: kOrangeColorDarkLight,
                        ),
                        onPressed: () {
                          launchUrl(Uri.parse(L10n.getString("homepage_ensicaen_url")));
                        },
                        child: Text(
                          '${L10n.getString("read_more")} \u00BB',
                          textAlign: TextAlign.center,
                          style: Theme.of(context).textTheme.bodyMedium!.apply(fontSizeFactor: scale, color: Colors.black),
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(width: 10),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Center(
                        child: Image.asset(
                          'assets/images/unicaen_logo.png',
                          fit: BoxFit.cover,
                          height: 130 * scale,
                        ),
                      ),
                      Text(
                        L10n.getString("homepage_unicaen_title"),
                        style: Theme.of(context).textTheme.headlineMedium!.apply(fontSizeFactor: scale, color: Colors.black),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          L10n.getString("homepage_unicaen_description"),
                          style: Theme.of(context).textTheme.bodyMedium!.apply(fontSizeFactor: scale, color: Colors.black),
                        ),
                      ),
                      TextButton(
                        style: TextButton.styleFrom(
                          padding: const EdgeInsets.all(10),
                          backgroundColor: kOrangeColorDarkLight,
                        ),
                        onPressed: () {
                          launchUrl(Uri.parse(L10n.getString("homepage_unicaen_url")));
                        },
                        child: Text(
                          '${L10n.getString("read_more")} \u00BB',
                          textAlign: TextAlign.center,
                          style: Theme.of(context).textTheme.bodyMedium!.apply(fontSizeFactor: scale, color: Colors.black),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
