class TeamItem {
  final String period;
  final List<String> students;

  TeamItem({
    required this.students,
    required this.period,
  });
}

final List<TeamItem> applicationTeamList = [
  TeamItem(
    period: "2015/2016",
    students: ["Florentin BLANC", "Denis CHEN", "Tristan FAUQUETTE", "Annick VOLCY"],
  ),
  TeamItem(
    period: "2016/2017",
    students: ["Vincent DUPLESSIS", "Benjamin HOUX", "Axel OLLIVIER", "Yann PELLEGRINI", "Elodie PROUX"],
  ),
  TeamItem(
    period: "2017/2018",
    students: ["Samuel ANSEL", "Sylvain GAUVREAU", "Antonio MANCUSO", "Loïc PETIT", "Clément PODEVIN"],
  ),
  TeamItem(
    period: "2018/2019",
    students: ["Aboubakrine NIANE", "Lauren SILVA ROLAN SAMPAIO", "Mathieu SERAPHIM"],
  ),
];

final List<TeamItem> websiteTeamList = [
  TeamItem(
    period: "2019/2020",
    students: ["Martin FÉAUX DE LA CROIX"],
  ),
  TeamItem(
    period: "2020/2021",
    students: ["Antoine BOITEAU", "Suliac LAVENANT", "Clémentine LEROY"],
  ),
  TeamItem(
    period: "2021/2022",
    students: ["Ilyass AZIRAR", "Amine KHALFAOUI", "Reda RGUIG", "Ilias LAHBABI"],
  ),
  TeamItem(
    period: "2021/2022",
    students: ["Quentin LIREUX", "Quentin LEGOT"],
  ),
  TeamItem(
    period: "2022/2023",
    students: ["Mohamed AKADDAR"],
  ),
  TeamItem(
    period: "2023-2024",
    students: ["Blaise HECQUET", "Moris Lorys KAMGANG", "Tom LE CAM", "Come LESCARMONTIER", "Robin SALMI"],
  ),
];
