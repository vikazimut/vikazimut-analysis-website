// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/screen_helper.dart';

class ShiftProjectSection extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScreenHelper(
      mobile: (BuildContext context) => _buildUi(context, ScreenHelper.getMobileMaxWidth(context), 0.8),
      tablet: (BuildContext context) => _buildUi(context, ScreenHelper.tabletMaxWidth, 1),
      desktop: (BuildContext context) => _buildUi(context, ScreenHelper.desktopMinWidth, 1),
    );
  }

  Widget _buildUi(BuildContext context, double width, double scale) {
    return Center(
      child: Container(
        width: width,
        padding: const EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              L10n.getString("homepage_shift_title"),
              style: Theme.of(context).textTheme.displayMedium!.apply(fontSizeFactor: scale),
            ),
            const SizedBox(
              height: 30.0,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  L10n.getString("homepage_shift_subtitle"),
                  style: Theme.of(context).textTheme.displaySmall!.apply(fontSizeFactor: scale),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    L10n.getString("homepage_shift_description"),
                  ),
                ),
                TextButton(
                  style: TextButton.styleFrom(
                    padding: const EdgeInsets.all(10),
                    backgroundColor: kOrangeColorDarkLight,
                  ),
                  onPressed: () {
                    launchUrl(Uri.parse("https://shift.uness.fr/"));
                  },
                  child: Text(
                    '${L10n.getString("read_more")} \u00BB',
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.bodyMedium!.apply(fontSizeFactor: scale, color: Colors.black),
                  ),
                ),
                const SizedBox(height: 15),
                Center(
                  child: Image.asset(
                    'assets/images/shift_logo.png',
                    fit: BoxFit.fill,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
